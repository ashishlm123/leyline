package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Prabin on 9/6/2017.
 */

public class FriendsModel extends UserModel implements Serializable {
    private boolean isFriends;
    public static final String SENT = "sent";
    public static final String ACCEPT = "accept";
    public static final String UNFRIEND = "unfriend";
    public static final String CANCEL_REQUEST = "cancelrequest";
    public static final String RECEIVE = "receive";
    public static final String SEND = "send";
    public static final String FRIEND = "isfriend";
    public static final String NONE = "none";

    private String threadId;
    private String requestStatus;

    public boolean isFriends() {
        return isFriends;
    }

    public void setFriends(boolean friends) {
        isFriends = friends;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    @Override
    public String toString() {
        return getUserName();
    }
}
