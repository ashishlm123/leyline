package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Srijana on 10/5/2017.
 */

public class FeedModel extends EventModel implements Serializable {
//    private boolean isFeedLiked;
    private String feedId, feedTitle;
    private UserModel relationBy;
//    private String eventFeedsRelation;
//    commentCount, likeCount, feedsLikeCount,

//    public boolean isFeedLiked() {
//        return isFeedLiked;
//    }
//
//    public void setFeedLiked(boolean feedLiked) {
//        isFeedLiked = feedLiked;
//    }

//    public String getCommentCount() {
//        return commentCount;
//    }
//
//    public void setCommentCount(String commentCount) {
//        this.commentCount = commentCount;
//    }
//
//    public String getLikeCount() {
//        return likeCount;
//    }
//
//    public void setLikeCount(String likeCount) {
//        this.likeCount = likeCount;
//    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public UserModel getRelationBy() {
        return relationBy;
    }

    public void setRelationBy(UserModel relationBy) {
        this.relationBy = relationBy;
    }

//    public String getEventFeedsRelation() {
//        return eventFeedsRelation;
//    }
//
//    public void setEventFeedsRelation(String eventFeedsRelation) {
//        this.eventFeedsRelation = eventFeedsRelation;
//    }
//
//    public String getFeedsLikeCount() {
//        return feedsLikeCount;
//    }
//
//    public void setFeedsLikeCount(String feedsLikeCount) {
//        this.feedsLikeCount = feedsLikeCount;
//    }

    public String getFeedTitle() {
        return feedTitle;
    }

    public void setFeedTitle(String feedTitle) {
        this.feedTitle = feedTitle;
    }
}
