package com.emts.leyline.models;

import java.io.Serializable;

public class EventCategory implements Serializable {

    private String eventCatIcon;
    private String eventCatName;
    private String eventCatId;
    private String eventCatColor;

    @Override
    public String toString() {
        return this.eventCatName;
    }

    public String getEventCatColor() {
        return eventCatColor;
    }

    public void setEventCatColor(String eventCatColor) {
        this.eventCatColor = eventCatColor;
    }

    public String getEventCatIcon() {
        return eventCatIcon;
    }

    public void setEventCatIcon(String eventCatIcon) {
        this.eventCatIcon = eventCatIcon;
    }

    public String getEventCatId() {
        return eventCatId;
    }

    public void setEventCatId(String eventCatId) {
        this.eventCatId = eventCatId;
    }

    public void setEventCatName(String eventCatName) {
        this.eventCatName = eventCatName;
    }

    public String getEventCatName() {
        return eventCatName;
    }
}
