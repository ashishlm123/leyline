package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Prabin on 9/13/2017.
 */

public class ChatModel extends ChatThread implements Serializable {

    public static final int MESSAGE_SENDING = 1;
    public static final int MESSAGE_FAILED = 2;
    public static final int MESSAGE_SENT = 3;

    private String messageTime;
    private String message;
    private String messageId = "0";
    private boolean fromMe;

    private int msgStatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public boolean isFromMe() {
        return fromMe;
    }

    public void setFromMe(boolean fromMe) {
        this.fromMe = fromMe;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public int getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(int msgStatus) {
        this.msgStatus = msgStatus;
    }
}
