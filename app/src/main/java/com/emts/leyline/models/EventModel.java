package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Prabin on 8/23/2017.
 */

public class EventModel extends EventCategory implements Serializable {
    public static final String OPEN_EVENT = "1";
    public static final String CLOSE_EVENT = "2";

    public static final String EVENT_ATTENDED = "attended";
    public static final String EVENT_ATTEND = "attend";
    public static final String EVENT_MAY_BE = "maybe";
    public static final String EVENT_ATTENDING = "attending";
    public static final String EEVENT_LEAVE = "leave";
    public static final String EVENT_CLOSED = "closed";
    public static final String EVENT_NONE = "none";
    public static final String MERGE_REQUEST = "merge_request";

    private boolean isLiked;
    private String eventName;
    private UserModel[] hostedBy;
    private UserModel createdBy;
    private float eventRating;
    private String eventDesc, eventLocation, eventImage, eventLikes,
            status, eventId, eventTime, eventDate;
    private String eventCommentCount;
    private String eventAttendStatus;

    private double latitude, longitude;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public UserModel[] getHostedBy() {
        return hostedBy;
    }

    public void setHostedBy(UserModel[] hostedBy) {
        this.hostedBy = hostedBy;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public float getEventRating() {
        return eventRating;
    }

    public void setEventRating(float eventRating) {
        this.eventRating = eventRating;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public String getEventLikes() {
        return eventLikes;
    }

    public void setEventLikes(String eventLikes) {
        this.eventLikes = eventLikes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventAttendStatus() {
        return eventAttendStatus;
    }

    public void setEventAttendStatus(String eventAttendStatus) {
        this.eventAttendStatus = eventAttendStatus;
    }

    public String getEventCommentCount() {
        return eventCommentCount;
    }

    public void setEventCommentCount(String eventCommentCount) {
        this.eventCommentCount = eventCommentCount;
    }

    public UserModel getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserModel createdBy) {
        this.createdBy = createdBy;
    }
}
