package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Prabin on 10/12/2017.
 */

public class NotificationModel extends EventModel implements Serializable {
    public static final String NOTI_RATED = "rated";
    public static final String NOTI_LIKED = "liked";
    public static final String NOTI_COMMENT = "commented";
    public static final String NOTI_MENTION = "mentioned";
    public static final String MERGE_ACCEPTED = "accepted";
    public static final String MERGE_REJECTED = "rejected";
    public static final String NOTI_MAYBE = "maybe";
    public static final String NOTI_INVITE = "invite";
    public static final String NOTI_STARTTODAY = "start_today";

    private String notId, notTitle, notType, mergeReqStat, notTime;
    private UserModel notiUser;

    public String getNotId() {
        return notId;
    }

    public void setNotId(String notId) {
        this.notId = notId;
    }

    public String getNotTitle() {
        return notTitle;
    }

    public void setNotTitle(String notTitle) {
        this.notTitle = notTitle;
    }

    public String getNotType() {
        return notType;
    }

    public void setNotType(String notType) {
        this.notType = notType;
    }

    public String getNotTime() {
        return notTime;
    }

    public void setNotTime(String notTime) {
        this.notTime = notTime;
    }

    public UserModel getNotiUser() {
        return notiUser;
    }

    public void setNotiUser(UserModel notiUser) {
        this.notiUser = notiUser;
    }

    public String getMergeReqStat() {
        return mergeReqStat;
    }

    public void setMergeReqStat(String mergeReqStat) {
        this.mergeReqStat = mergeReqStat;
    }
}
