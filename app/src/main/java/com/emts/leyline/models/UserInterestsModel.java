package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Prabin on 9/6/2017.
 */

public class UserInterestsModel implements Serializable {
    private String userInterests, userInterestId;

    public String getUserInterests() {
        return userInterests;
    }

    public void setUserInterests(String userInterests) {
        this.userInterests = userInterests;
    }

    public String getUserInterestId() {
        return userInterestId;
    }

    public void setUserInterestId(String userInterestId) {
        this.userInterestId = userInterestId;
    }
}
