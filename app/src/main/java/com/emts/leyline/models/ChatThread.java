package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by theone on 9/12/2017.
 */

public class ChatThread implements Serializable {
    private String latestMsgPreview,lastMsgId, chatTime;
    private UserModel sentBy;
    private String threadId;

    public String getLatestMsgPreview() {
        return latestMsgPreview;
    }

    public void setLatestMsgPreview(String latestMsgPreview) {
        this.latestMsgPreview = latestMsgPreview;
    }

    public String getChatTime() {
        return chatTime;
    }

    public void setChatTime(String chatTime) {
        this.chatTime = chatTime;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public UserModel getSentBy() {
        return sentBy;
    }

    public void setSentBy(UserModel sentBy) {
        this.sentBy = sentBy;
    }

    public String getLastMsgId() {
        return lastMsgId;
    }

    public void setLastMsgId(String lastMsgId) {
        this.lastMsgId = lastMsgId;
    }
}
