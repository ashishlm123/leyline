package com.emts.leyline.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Srijana on 10/4/2017.
 */

public class CalendarEventModel implements Serializable {
    private String day;
    private String month;
    private String year;

    public ArrayList<EventModel> getEventList() {
        return eventList;
    }

    public void setEventList(ArrayList<EventModel> eventList) {
        this.eventList = eventList;
    }

    private ArrayList<EventModel> eventList;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }


    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }



}
