package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Prabin on 8/24/2017.
 */

public class UserModel implements Serializable {
    public static final String GENDER_MALE = "1";
    public static final String GENDER_FEMALE = "2";
    private String userName, eMail;
    private String userId, imgUrl, coverImgUrl;
    private String userWebLink, gender, hometown, zipCode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getCoverImgUrl() {
        return coverImgUrl;
    }

    public void setCoverImgUrl(String coverImgUrl) {
        this.coverImgUrl = coverImgUrl;
    }

    public String getUserWebLink() {
        return userWebLink;
    }

    public void setUserWebLink(String userWebLink) {
        this.userWebLink = userWebLink;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
