package com.emts.leyline.models;

import java.io.Serializable;

/**
 * Created by Srijana on 10/4/ .
 */

public class CommentModel extends UserModel implements Serializable {
    private String cmntId;
    private String cmntTime;
    private String cmntBody;
    private int replyCount;
    private String mentionJsonData;

    public String getCmntId() {
        return cmntId;
    }

    public void setCmntId(String cmntId) {
        this.cmntId = cmntId;
    }

    public String getCmntTime() {
        return cmntTime;
    }

    public void setCmntTime(String cmntTime) {
        this.cmntTime = cmntTime;
    }

    public String getCmntBody() {
        return cmntBody;
    }

    public void setCmntBody(String cmntBody) {
        this.cmntBody = cmntBody;
    }


    public int getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(int replyCount) {
        this.replyCount = replyCount;
    }

    public String getMentionJsonData() {
        return mentionJsonData;
    }

    public void setMentionJsonData(String mentionJsonData) {
        this.mentionJsonData = mentionJsonData;
    }
}
