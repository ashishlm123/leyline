package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.activity.CommentActivity;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.FeedsActivity;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.MyTextUtils;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.FeedModel;
import com.emts.leyline.models.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prabin on 9/15/2017.
 */

public class FeedsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FeedModel> feedsList;
    private ArrayList<EventModel> eventSuggestionList;
    public static int EVENT_RATE = 123;
    PreferenceHelper helper = PreferenceHelper.getInstance(context);
    private static int ITEM_TYPE_FEEDS = 111;
    private static int ITEM_TYPE_SUGGESTIONS = 112;

    public void setEventSuggestionList(ArrayList<EventModel> eventSuggestionList) {
        this.eventSuggestionList = eventSuggestionList;
        notifyItemChanged(4);
    }

    public FeedsAdapter(ArrayList<FeedModel> feedsList, Context context) {
        this.feedsList = feedsList;
        this.context = context;

        eventSuggestionList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_FEEDS) {
            return new FeedsAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_feeds, parent, false));
        } else {
            return new FeedsAdapter.ViewHolderSuggestion(LayoutInflater.from(context).inflate(R.layout.item_suggestion_infeeds, parent,
                    false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_TYPE_SUGGESTIONS) {
            FeedsAdapter.ViewHolderSuggestion viewHolder = (FeedsAdapter.ViewHolderSuggestion) holder;
            try {
                viewHolder.eventSuggestionLists.getAdapter().notifyDataSetChanged();
                if (eventSuggestionList.size() < 1) {
                    viewHolder.itemView.setLayoutParams(new RelativeLayout.LayoutParams(0, 0));
                    viewHolder.itemView.setVisibility(View.GONE);
                } else {
                    viewHolder.itemView.setLayoutParams(
                            new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    viewHolder.itemView.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {

            }
        } else {
            FeedsAdapter.ViewHolder viewHolder = (FeedsAdapter.ViewHolder) holder;
            FeedModel feedsModel = feedsList.get(position);
            viewHolder.friendsName.setText(feedsModel.getRelationBy().getUserName());
            viewHolder.feedsTitle.setText(feedsModel.getFeedTitle().toLowerCase());
            viewHolder.eventName.setText(MyTextUtils.checkNull(feedsModel.getEventName()));
//        viewHolder.eventLoc.setText(feedsModel.getHostedBy().getUserName());
            viewHolder.eventDateTime.setText(DateUtils.convertDate(feedsModel.getEventDate()));
            viewHolder.eventLocation.setText(feedsModel.getEventLocation());
            if (feedsModel.isLiked()) {
                viewHolder.iconLike.setImageResource(R.drawable.ic_thumb_green_24dp);
            } else {
                viewHolder.iconLike.setImageResource(R.drawable.ic_thumb_white_24dp);
            }
            viewHolder.feedLikeCount.setText(feedsModel.getEventLikes());
            viewHolder.feedCommentCount.setText(feedsModel.getEventCommentCount());

            if (!TextUtils.isEmpty(feedsModel.getRelationBy().getImgUrl())) {
                Picasso.with(context).load(feedsModel.getRelationBy().getImgUrl()).into(viewHolder.profilePic);
            } else {
                Picasso.with(context).load(R.drawable.temp_profile_logo).into(viewHolder.profilePic);
            }

            if (!TextUtils.isEmpty(feedsModel.getEventImage())) {
                Picasso.with(context).load(feedsModel.getEventImage()).into(viewHolder.eventImage);
            } else {
                Picasso.with(context).load(R.drawable.temp_event_image).into(viewHolder.eventImage);
            }

            float rate = (feedsModel.getEventRating());
            viewHolder.eventRating.setRating((float) Math.ceil(rate));
//        viewHolder.eventComment
//        viewHolder.eventLikes.setText(feedsModel.getEventLikes());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 4) {
            return ITEM_TYPE_SUGGESTIONS;
        } else {
            return ITEM_TYPE_FEEDS;
        }
    }

    @Override
    public int getItemCount() {
        return feedsList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView friendsName, feedsTitle, eventName, eventDateTime, eventLocation, feedLikeCount, feedCommentCount;
        //eventLoc,
        RatingBar eventRating;
        ImageView eventDetails, profilePic, eventImage;
        final ImageView iconLike;
        RelativeLayout holderLike, holderComment;

        ViewHolder(View itemView) {
            super(itemView);

            friendsName = itemView.findViewById(R.id.friends_name);
            feedsTitle = itemView.findViewById(R.id.feeds_title);
            eventName = itemView.findViewById(R.id.event_name);
//            eventLoc = itemView.findViewById(R.id.host_name);
            eventDateTime = itemView.findViewById(R.id.event_date_time);
            eventLocation = itemView.findViewById(R.id.event_location);
            eventRating = itemView.findViewById(R.id.rating_bar);
            feedLikeCount = itemView.findViewById(R.id.like_no);
            feedCommentCount = itemView.findViewById(R.id.tv_comments);
            eventDetails = itemView.findViewById(R.id.event_details);
            profilePic = itemView.findViewById(R.id.profile_pic);
            eventImage = itemView.findViewById(R.id.event_image);
            iconLike = itemView.findViewById(R.id.icon_like);

            holderLike = itemView.findViewById(R.id.like_holder);
            holderComment = itemView.findViewById(R.id.comment_holder);

            friendsName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    FeedModel feedList = feedsList.get(getLayoutPosition());
                    UserModel userModel = new UserModel();
                    userModel.setUserId(feedList.getRelationBy().getUserId());
                    userModel.setUserName(feedList.getRelationBy().getUserName());
                    userModel.setImgUrl(feedList.getRelationBy().getImgUrl());
                    intent.putExtra("user", userModel);
                    context.startActivity(intent);
                }
            });

            profilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    FeedModel feedList = feedsList.get(getLayoutPosition());
                    UserModel userModel = new UserModel();
                    userModel.setUserId(feedList.getRelationBy().getUserId());
                    userModel.setUserName(feedList.getRelationBy().getUserName());
                    userModel.setImgUrl(feedList.getRelationBy().getImgUrl());
                    intent.putExtra("user", userModel);
                    context.startActivity(intent);
                }
            });

            eventImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getEventDetails(getLayoutPosition());
                }
            });

            eventName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getEventDetails(getLayoutPosition());
                }
            });

            eventDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getEventDetails(getLayoutPosition());
                }
            });

            holderComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.putExtra("eventid", feedsList.get(getLayoutPosition()).getEventId());
                    context.startActivity(intent);
                }
            });

            holderLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (feedsList.get(getLayoutPosition()).isLiked()) {
                        if (NetworkUtils.isInNetwork(context)) {
                            userFeedsLikeTask(getLayoutPosition(), false, feedLikeCount, iconLike);
                        } else {
                            AlertUtils.showToast(context, "No Internet Connection...");
                        }
                    } else {
                        if (NetworkUtils.isInNetwork(context)) {
                            userFeedsLikeTask(getLayoutPosition(), true, feedLikeCount, iconLike);
                        } else {
                            AlertUtils.showToast(context, "No Internet Connection...");
                        }
                    }
                }
            });
        }

        private void getEventDetails(int position) {
            Intent intent = new Intent(context, EventDetailsActivity.class);
            intent.putExtra("eventmodel", feedsList.get(position));
            intent.putExtra("position", position);
            ((FeedsActivity) context).startActivityForResult(intent, EVENT_RATE);
        }
    }

    private class ViewHolderSuggestion extends RecyclerView.ViewHolder {
        RecyclerView eventSuggestionLists;

        ViewHolderSuggestion(View itemView) {
            super(itemView);
            eventSuggestionLists = itemView.findViewById(R.id.event_suggestions_list);
            eventSuggestionLists.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            eventSuggestionLists.setNestedScrollingEnabled(false);

            EventSuggestionAdaper eventSuggestionAdaper = new EventSuggestionAdaper(eventSuggestionList, context);
            eventSuggestionLists.setAdapter(eventSuggestionAdaper);
        }
    }

    private void userFeedsLikeTask(final int pos, final boolean isEventLiked,
                                   final TextView feedLikeCount, final ImageView iconLike) {
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", helper.getUserId());
        if (isEventLiked) {
            postMap.put("status", String.valueOf(true));
        } else {
            postMap.put("status", String.valueOf(false));
        }
        postMap.put("feeds_id", feedsList.get(pos).getFeedId());
        postMap.put("event_id", feedsList.get(pos).getEventId());

        vHelper.addVolleyRequestListeners(Api.getInstance().likeEventApi, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        if (isEventLiked) {
                            String eventId = feedsList.get(pos).getEventId();
                            for (int i = 0; i < feedsList.size(); i++) {
                                String listEventId = feedsList.get(i).getEventId();
                                if (eventId.equals(listEventId)) {
                                    feedsList.get(i).setLiked(true);
                                    feedsList.get(i).setEventLikes(res.getString("count"));
                                }
                            }
                            feedsList.get(pos).setLiked(true);
                            notifyDataSetChanged();
                        } else {
                            String eventId = feedsList.get(pos).getEventId();
                            for (int i = 0; i < feedsList.size(); i++) {
                                String listEventId = feedsList.get(i).getEventId();
                                if (eventId.equals(listEventId)) {
                                    feedsList.get(i).setLiked(false);
                                    feedsList.get(i).setEventLikes(res.getString("count"));
                                }
                            }
                            feedsList.get(pos).setLiked(false);
                            notifyDataSetChanged();
//                            Drawable favoriteIcon = DrawableCompat.wrap();
//                            ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.white, getTheme());
//                            DrawableCompat.setTintList(favoriteIcon, colorSelector);
//                            item.setIcon(favoriteIcon);
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("userFeedsLikeTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {

            }
        }, "userFeedsLikeTask");

    }


}
