package com.emts.leyline.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.models.FriendsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by theone on 9/6/2017.
 */
public class InviteFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FriendsModel> friendsList;
    private ArrayList<String> selectedFriendsId;
    boolean searchFriends;

    public InviteFriendAdapter(Context context, ArrayList<FriendsModel> friendsList) {
        this.friendsList = friendsList;
        this.context = context;
        selectedFriendsId = new ArrayList<>(friendsList.size());
    }

    public void setSearchFriends(boolean searchFriends) {
        this.searchFriends = searchFriends;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_invite_frn, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        FriendsModel friendsModel = friendsList.get(position);
        viewHolder.friendsName.setText(friendsModel.getUserName());
        if (selectedFriendsId.contains(friendsModel.getUserId())) {
            viewHolder.cbInvite.setChecked(true);
        } else {
            viewHolder.cbInvite.setChecked(false);
        }
        if (!TextUtils.isEmpty(friendsModel.getImgUrl())) {
            Picasso.with(context).load(friendsModel.getImgUrl()).into(viewHolder.userImage);
        }
    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }

    public void selectAllFriends() {
        for (int i = 0; i < friendsList.size(); i++) {
            if (!selectedFriendsId.contains(friendsList.get(i).getUserId())) {
                selectedFriendsId.add(friendsList.get(i).getUserId());
            }
        }
        notifyDataSetChanged();
    }

    public void deSelectAllFriends() {
        selectedFriendsId.clear();
        notifyDataSetChanged();
    }

    public ArrayList<String> getSelectedUsers() {
        return this.selectedFriendsId;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView friendsName;
        CheckBox cbInvite;
        ImageView userImage;

        ViewHolder(View itemView) {
            super(itemView);

            friendsName = itemView.findViewById(R.id.friends_name);
            friendsName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, friendsList.get(getLayoutPosition()),friendsList.get(getLayoutPosition()).getThreadId());
                }
            });
            userImage = itemView.findViewById(R.id.user_image);
            userImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, friendsList.get(getLayoutPosition()),friendsList.get(getLayoutPosition()).getThreadId());
                }
            });
            cbInvite = itemView.findViewById(R.id.cb_invite);
            cbInvite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    String userId = friendsList.get(getLayoutPosition()).getUserId();
                    if (b) {
                        if (!selectedFriendsId.contains(userId)) {
                            selectedFriendsId.add(userId);
                        }
                    } else {
                        if (selectedFriendsId.contains(userId)) {
                            selectedFriendsId.remove(userId);
                        }
                    }
                }
            });

            if (searchFriends) {
                cbInvite.setVisibility(View.GONE);
            } else {
                cbInvite.setVisibility(View.VISIBLE);
            }
        }
    }
}
