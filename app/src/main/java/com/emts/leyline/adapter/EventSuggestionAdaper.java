package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.FeedsActivity;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.models.EventModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Prabin on 11/17/2017.
 */

public class EventSuggestionAdaper extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<EventModel> eventList;
    PreferenceHelper helper = PreferenceHelper.getInstance(context);
    public static int EVENT_RATE = 111;

    public EventSuggestionAdaper(ArrayList<EventModel> eventList, Context context) {
        this.eventList = eventList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventSuggestionAdaper.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_event_suggestions,
                parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventSuggestionAdaper.ViewHolder viewHolder = (EventSuggestionAdaper.ViewHolder) holder;
        EventModel eventModel = eventList.get(position);
        viewHolder.eventName.setText(eventModel.getEventName());
        viewHolder.eventLocation.setText(eventModel.getEventLocation());
        if (!TextUtils.isEmpty(eventModel.getEventImage())) {
            Picasso.with(context).load(eventModel.getEventImage()).into(viewHolder.eventImage);
        } else {
            Picasso.with(context).load(R.drawable.temp_event_image).into(viewHolder.eventImage);
        }
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView eventImage;
        TextView eventName, eventLocation;
        Button seeDetails;

        public ViewHolder(View itemView) {
            super(itemView);
            eventImage = itemView.findViewById(R.id.event_pic);
            eventName = itemView.findViewById(R.id.event_name);
            eventLocation = itemView.findViewById(R.id.event_location);

            seeDetails = itemView.findViewById(R.id.view_details);
            seeDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getEventDetails(getLayoutPosition());
                }
            });
        }
    }

    private void getEventDetails(int position) {
        Intent intent = new Intent(context, EventDetailsActivity.class);
        intent.putExtra("eventmodel", eventList.get(position));
        intent.putExtra("position", position);
        ((FeedsActivity) context).startActivityForResult(intent, FeedsAdapter.EVENT_RATE);
    }
}
