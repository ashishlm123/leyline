package com.emts.leyline.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.activity.CommentActivity;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.MergeRequestActivity;
import com.emts.leyline.activity.NotificationActivity;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.NotificationModel;
import com.emts.leyline.models.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prabin on 10/12/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<NotificationModel> notificationList;

    public NotificationAdapter(ArrayList<NotificationModel> notificationList, Context context) {
        this.notificationList = notificationList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        NotificationModel notificationModel = notificationList.get(position);
        if (!TextUtils.isEmpty(notificationModel.getNotiUser().getImgUrl())) {
            Picasso.with(context).load(notificationModel.getNotiUser().getImgUrl()).into(viewHolder.profilePic);
        }
        viewHolder.userName.setText(notificationModel.getNotiUser().getUserName());
        switch (notificationModel.getNotType()) {
            case NotificationModel.NOTI_LIKED:
                viewHolder.notTypeIcon.setImageResource(R.drawable.ic_thumb_white_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                        + "</b> " + context.getString(R.string.liked_event) + " <b> <font color=\"#8DBE18\">" +
                        notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.NOTI_COMMENT:
                viewHolder.notTypeIcon.setImageResource(R.drawable.ic_comment_black_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                        + "</b> " + context.getString(R.string.commented_on_event) + " <b> <font color=\"#8DBE18\">" +
                        notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.NOTI_RATED:
                viewHolder.notTypeIcon.setImageResource(R.drawable.starfull);
                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                        + "</b> " + context.getString(R.string.rated_on_event) + " <b> <font color=\"#8DBE18\">" +
                        notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.EVENT_ATTENDED:
                viewHolder.notTypeIcon.setImageResource(R.drawable.not_type_event_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                        + "</b> " + context.getString(R.string.attended_on_event) + " <b> <font color=\"#8DBE18\">" +
                        notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.EVENT_ATTENDING:
                viewHolder.notTypeIcon.setImageResource(R.drawable.not_type_event_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                        + "</b> " + context.getString(R.string.attending_an_event) + " <b> <font color=\"#8DBE18\">" +
                        notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.MERGE_REQUEST:
                PreferenceHelper prefsHelper =  PreferenceHelper.getInstance(context);
                if (notificationModel.getCreatedBy().getUserId().equals(prefsHelper.getUserId())){
                    viewHolder.notTypeIcon.setImageResource(R.drawable.friends_add);
                    viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                            + "</b> " + context.getString(R.string.requested_to_merge) + " <b> <font color=\"#8DBE18\">"
                            + notificationModel.getEventName() + "</font></b>"));
                }else {
                    switch (notificationModel.getMergeReqStat()) {
                        case NotificationModel.MERGE_ACCEPTED:
                            viewHolder.notTypeIcon.setImageResource(R.drawable.merge_accepted_24dp);
                            viewHolder.userName.setText(Html.fromHtml("<b> " + "Your "
                                    + "</b> " + context.getString(R.string.merge_request_accepted) +
                                    " <b> <font color=\"#8DBE18\">" + notificationModel.getEventName() + "<font/></b>"));
                            break;
                        case NotificationModel.MERGE_REJECTED:
                            viewHolder.notTypeIcon.setImageResource(R.drawable.merge_decline_24dp);
                            viewHolder.userName.setText(Html.fromHtml("<b> " + "Your"
                                    + "</b> " + context.getString(R.string.merge_request_denied) +
                                    " <b> <font color=\"#8DBE18\">" + notificationModel.getEventName() + "</font></b>"));
                            break;
                        default:
                            viewHolder.notTypeIcon.setImageResource(R.drawable.friends_add);
                            viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                                    + "</b> " + context.getString(R.string.requested_merge) + " <b> <font color=\"#8DBE18\">"
                                    + notificationModel.getEventName() + "</font></b>"));
                            break;
                    }
                }
                break;
            case NotificationModel.NOTI_MAYBE:
                viewHolder.notTypeIcon.setImageResource(R.drawable.may_be_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
                        + "</b> " + context.getString(R.string.may_be_attending) + " <b> <font color=\"#8DBE18\">"
                        + notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.NOTI_INVITE:
                viewHolder.notTypeIcon.setImageResource(R.drawable.invite_to_event_24dp);
//                viewHolder.userName.setText(Html.fromHtml("<b> " + notificationModel.getNotiUser().getUserName()
//                        + "</b>" + " invites you to an event " + "<b> <font color=\"#8DBE18\">"
//                        + notificationModel.getEventName() + "</font></b>"));
                viewHolder.userName.setText(Html.fromHtml(context.getString(R.string.invited_to_event) + " <b> <font color=\"#8DBE18\">"
                        + notificationModel.getEventName() + "</font></b>"));
                break;
            case NotificationModel.NOTI_STARTTODAY:
                viewHolder.notTypeIcon.setImageResource(R.drawable.invite_to_event_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> <font color = \"#8DBE18\">"
                        + notificationModel.getEventName()
                        + "</font></b>" + " is starting today. "));
                break;
            case NotificationModel.NOTI_MENTION:
                viewHolder.notTypeIcon.setImageResource(R.drawable.ic_comment_black_24dp);
                viewHolder.userName.setText(Html.fromHtml("<b> <font color = \"#8DBE18\">"
                        + notificationModel.getNotiUser().getUserName()
                        + "</font></b>" + " mentioned you in a comment."));
                break;
        }
        viewHolder.notificationTime.setText(DateUtils.getPostTimeAgoV2(notificationModel.getNotTime()));

    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profilePic, notTypeIcon;
        TextView userName, notificationTime;

        public ViewHolder(View itemView) {
            super(itemView);

            notTypeIcon = itemView.findViewById(R.id.not_icon_type);
            profilePic = itemView.findViewById(R.id.profile_pic);

            userName = itemView.findViewById(R.id.user_name);
            notificationTime = itemView.findViewById(R.id.time);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (notificationList.get(getLayoutPosition()).getNotType().equals(NotificationModel.NOTI_COMMENT)
                            || notificationList.get(getLayoutPosition()).getNotType().equals(NotificationModel.NOTI_MENTION)) {
                        Intent intent = new Intent(context, CommentActivity.class);
                        intent.putExtra("eventid", notificationList.get(getLayoutPosition()).getEventId());
                        context.startActivity(intent);
                    } else {
                        getEventDetails(getLayoutPosition(), notificationList.get(getLayoutPosition()).getNotType());
                    }
                }
            });

        }
    }

    private void getEventDetails(final int position, final String notType) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(context, "Please Wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("event_id", notificationList.get(position).getEventId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getEventViaDate, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);

                            if (res.getBoolean("status")) {
                                JSONArray infoArray = res.getJSONArray("info");
                                for (int i = 0; i < infoArray.length(); i++) {
                                    JSONArray eventArray = infoArray.getJSONArray(i);
                                    for (int j = 0; j < eventArray.length(); j++) {
                                        JSONObject eachObj = eventArray.getJSONObject(j);
                                        EventModel eventModel = new EventModel();

                                        JSONArray hostArray = eachObj.getJSONArray("host_name");
                                        UserModel[] hostUsers = new UserModel[hostArray.length()];
                                        for (int k = 0; k < hostArray.length(); k++) {
                                            JSONObject eachHostInfo = hostArray.getJSONObject(k);
                                            UserModel hostedBy = new UserModel();
                                            hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                            hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                            if (!eachHostInfo.getString("host_image").equals("")) {
                                                hostedBy.setImgUrl(res.getString("profileimage_default_url")
                                                        + eachHostInfo.getString("host_image"));
                                            } else {
                                                hostedBy.setImgUrl("");
                                            }
                                            hostUsers[k] = hostedBy;
                                        }
                                        eventModel.setHostedBy(hostUsers);

                                        eventModel.setEventName(eachObj.getString("event_name"));
                                        eventModel.setEventImage(res.getString("event_image_default_url")
                                                + eachObj.getString("event_image"));
                                        eventModel.setEventAttendStatus(eachObj.getString("attendance"));

                                        eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                        eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));

                                        eventModel.setEventDesc(eachObj.getString("description"));
                                        eventModel.setEventDate(eachObj.getString("event_date"));
                                        eventModel.setEventLocation(eachObj.getString("location"));
                                        eventModel.setEventId(eachObj.getString("event_id"));
                                        eventModel.setStatus(eachObj.getString("status"));

                                        try {
                                            eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                        } catch (Exception e) {
                                            eventModel.setEventRating(0);
                                        }
                                        eventModel.setEventCatId(eachObj.getString("event_category_id"));
                                        eventModel.setEventTime(eachObj.getString("event_time"));

                                        JSONObject likedBy = eachObj.getJSONObject("like");
                                        eventModel.setLiked(likedBy.getBoolean("your_like_status"));

                                        if (notType.equals(NotificationModel.MERGE_REQUEST)) {
                                            Intent intent = new Intent(context, MergeRequestActivity.class);
                                            intent.putExtra("notificationmodel", notificationList.get(position));
                                            intent.putExtra("position", position);
                                            intent.putExtra("eventmodel", eventModel);
                                            ((NotificationActivity)context).startActivityForResult(intent, NotificationActivity.MERGE_REQUEST_REQUEST);
                                        } else {
                                            Intent intent = new Intent(context, EventDetailsActivity.class);
                                            intent.putExtra("eventmodel", eventModel);
                                            context.startActivity(intent);
                                        }
                                        break;
                                    }
                                }
                            } else {
                                AlertUtils.showAlertMessage(context, "Error !!!", res.getString("message") + "");
                            }
                        } catch (JSONException e) {
                            Logger.e("getEventDetails json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please check internet access and try again latter.";
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {

                        }
                    }
                }, "getEventDetails");
    }

}
