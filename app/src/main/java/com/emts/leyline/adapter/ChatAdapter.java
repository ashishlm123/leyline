package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.ChatActivity;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.models.ChatModel;
import com.emts.leyline.models.UserModel;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

/**
 * Created by Prabin on 9/13/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ChatModel> chatList;
    private final int CHAT_FORM_ME = 1;
    private final int CHAT_FROM_FRIEND = 2;
    private PreferenceHelper prefHelper;

    public ChatAdapter(Context context, ArrayList<ChatModel> chatList) {
        this.context = context;
        this.chatList = chatList;
        prefHelper = PreferenceHelper.getInstance(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CHAT_FORM_ME) {
            return new ViewHolderMe(LayoutInflater.from(context).inflate(R.layout.item_your_message, parent, false));
        } else {
            return new ViewHolderFriends(LayoutInflater.from(context).inflate(R.layout.item_friends_message, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatModel chatModel = chatList.get(position);
        if (holder instanceof ViewHolderMe) {
            ViewHolderMe holderMe = (ViewHolderMe) holder;
            holderMe.yourSentMessage.setText(StringEscapeUtils.unescapeJava(chatModel.getMessage()));
            if (!TextUtils.isEmpty(prefHelper.getString(PreferenceHelper.APP_USER_PIC, ""))) {
                Picasso.with(context).load(prefHelper.getString(PreferenceHelper.APP_USER_PIC, ""))
                        .into(holderMe.yourProfilePic);
            }
            if (chatModel.getMsgStatus() == ChatModel.MESSAGE_SENDING) {
                holderMe.progress.setVisibility(View.VISIBLE);
                holderMe.errorImage.setVisibility(View.GONE);
                holderMe.yourMessageTime.setVisibility(View.GONE);
            } else if (chatModel.getMsgStatus() == ChatModel.MESSAGE_SENT) {
                holderMe.yourMessageTime.setText(DateUtils.getPostTimeAgoV2(chatModel.getMessageTime()));

                holderMe.progress.setVisibility(View.GONE);
                holderMe.errorImage.setVisibility(View.GONE);
                holderMe.yourMessageTime.setVisibility(View.VISIBLE);
            } else if (chatModel.getMsgStatus() == ChatModel.MESSAGE_FAILED) {
                holderMe.progress.setVisibility(View.GONE);
                holderMe.errorImage.setVisibility(View.VISIBLE);
                holderMe.yourMessageTime.setVisibility(View.GONE);
            }
        } else {
            ViewHolderFriends holderFriends = (ViewHolderFriends) holder;
            holderFriends.friendsMessageTime.setText(DateUtils.getPostTimeAgoV2(chatModel.getMessageTime()));
            holderFriends.friendsReceivedMessage.setText(StringEscapeUtils.unescapeJava(chatModel.getMessage()));
            if (!TextUtils.isEmpty(chatModel.getSentBy().getImgUrl())) {
                Picasso.with(context).load(chatModel.getSentBy().getImgUrl()).into(holderFriends.friendsProfilePic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chatList.get(position).isFromMe()) {
            return CHAT_FORM_ME;
        } else {
            return CHAT_FROM_FRIEND;
        }
    }

    private class ViewHolderMe extends RecyclerView.ViewHolder {
        TextView yourMessageTime, yourSentMessage;
        ImageView yourProfilePic, errorImage;
        ProgressBar progress;

        ViewHolderMe(View itemView) {
            super(itemView);

            yourMessageTime = itemView.findViewById(R.id.your_message_time);
            yourSentMessage = itemView.findViewById(R.id.your_sent_message);
            yourProfilePic = itemView.findViewById(R.id.profile_pic);

            progress = itemView.findViewById(R.id.progress);
            errorImage = itemView.findViewById(R.id.error);
        }
    }

    private class ViewHolderFriends extends RecyclerView.ViewHolder {
        TextView friendsMessageTime, friendsReceivedMessage;
        ImageView friendsProfilePic;

        ViewHolderFriends(View itemView) {
            super(itemView);

            friendsMessageTime = itemView.findViewById(R.id.friends_message_time);
            friendsReceivedMessage = itemView.findViewById(R.id.friends_received_message);
            friendsProfilePic = itemView.findViewById(R.id.friends_profile_pic);

            friendsProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserModel userModel = chatList.get(getLayoutPosition()).getSentBy();
                    String threadId=chatList.get(getLayoutPosition()).getThreadId();
                    ProfileActivity.openProfile(context, userModel,threadId);
                }
            });
        }
    }
}
