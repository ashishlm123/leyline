package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.AddEventActivity;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.MainActivity;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.models.EventCategory;
import com.emts.leyline.models.EventModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Prabin on 9/7/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static int CREATED_EVENTS = 1;
    public static int ATTENDING_EVENTS = 2;
    public static int ATTENDED_EVENTS = 3;
    public static int REQUEST_MERGE = 4;
    int eventAs = 0;
    private String calledFromMain="1";

    private Context context;
    private ArrayList<EventModel> eventList;

    public EventAdapter(ArrayList<EventModel> eventList, Context context) {
        this.eventList = eventList;
        this.context = context;
    }

    public EventAdapter(ArrayList<EventModel> eventList, Context context, String calledFrom) {
        this.eventList = eventList;
        this.context = context;
        this.calledFromMain = calledFrom;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_event_listing, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        EventModel eventModel = eventList.get(position);
        EventCategory eventCategory = Config.getEvent(context, eventModel.getEventCatId());
//        viewHolder.eventIcon.setImageResource(eventModel.getEventCatIcon());
        Picasso.with(context).load(eventCategory.getEventCatIcon()).into(viewHolder.eventIcon);
        viewHolder.eventName.setText(eventModel.getEventName());
        viewHolder.eventLoc.setText(eventModel.getEventLocation());
        viewHolder.createdDate.setText(DateUtils.convertDate(eventModel.getEventDate()));
        if (eventAs == CREATED_EVENTS) {
            viewHolder.eventIcon.setColorFilter(context.getResources().getColor(R.color.blue_created));
            viewHolder.eventIcon.setBackgroundResource(R.drawable.created_event_icon_background);
        } else if (eventAs == ATTENDING_EVENTS) {
            viewHolder.eventIcon.setColorFilter(context.getResources().getColor(R.color.green_attending));
            viewHolder.eventIcon.setBackgroundResource(R.drawable.attending_event_icon_background);
        } else if (eventAs == ATTENDED_EVENTS) {
            viewHolder.eventIcon.setColorFilter(context.getResources().getColor(R.color.pink_attended));
            viewHolder.eventIcon.setBackgroundResource(R.drawable.attended_events_icon_background);
        } else {
            viewHolder.eventIcon.setColorFilter(context.getResources().getColor(R.color.white));
            viewHolder.eventIcon.setBackgroundResource(R.drawable.request_merge_icon_background);
        }
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView eventIcon;
        TextView eventName, eventLoc, createdDate;

        public ViewHolder(View itemView) {
            super(itemView);
            eventIcon = itemView.findViewById(R.id.event_icon);
            eventName = itemView.findViewById(R.id.event_name);
            eventLoc = itemView.findViewById(R.id.event_location);
            createdDate = itemView.findViewById(R.id.created_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if ( context instanceof MainActivity ) {

                        Intent intent = new Intent(context, EventDetailsActivity.class);
                        intent.putExtra("eventmodel", eventList.get(getLayoutPosition()));
                        if (eventAs == CREATED_EVENTS) {
                            intent.putExtra("createdPosition", getLayoutPosition());
                            ((MainActivity) context).startActivityForResult(intent, CREATED_EVENTS);
                        } else if (eventAs == ATTENDING_EVENTS) {
                            intent.putExtra("attendingPosition", getLayoutPosition());
                            ((MainActivity) context).startActivityForResult(intent, ATTENDING_EVENTS);
                        } else if (eventAs == ATTENDED_EVENTS) {
                            intent.putExtra("attendedPosition", getLayoutPosition());
                            ((MainActivity) context).startActivityForResult(intent, ATTENDED_EVENTS);
                        } else {

                            ((MainActivity) context).startActivityForResult(intent, CREATED_EVENTS);
                        }

                    }else{//called from profile activity



                        Intent intent = new Intent(context, EventDetailsActivity.class);
                        intent.putExtra("eventmodel", eventList.get(getLayoutPosition()));
                        if (eventAs == CREATED_EVENTS) {
                            intent.putExtra("createdPosition", getLayoutPosition());
                            ((ProfileActivity) context).startActivityForResult(intent, CREATED_EVENTS);
                        } else if (eventAs == ATTENDING_EVENTS) {
                            intent.putExtra("attendingPosition", getLayoutPosition());
                            ((ProfileActivity) context).startActivityForResult(intent, ATTENDING_EVENTS);
                        } else if (eventAs == ATTENDED_EVENTS) {
                            intent.putExtra("attendedPosition", getLayoutPosition());
                            ((ProfileActivity) context).startActivityForResult(intent, ATTENDED_EVENTS);
                        } else {
                            if (context instanceof AddEventActivity) {
                                Logger.e("profile activity bata", "aipuge");
                                intent.putExtra("requestMerge", true);
                                intent.putExtra("requesterEvent", mergeRequesterEventId);
                                ((AddEventActivity) context).startActivityForResult(intent, REQUEST_MERGE);
                            }
                        }
                    }
                }

            });

        }
    }

    private String mergeRequesterEventId = "";

    public void mergeRequesterEvent(String mergeRequesterEventId) {
        this.mergeRequesterEventId = mergeRequesterEventId;
    }

    public void setEventAs(int eventAs) {
        this.eventAs = eventAs;
    }

}
