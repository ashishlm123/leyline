package com.emts.leyline.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.FriendsModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2017-11-17.
 */

public class FriendSuggestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FriendsModel> suggestionList;
    private LayoutInflater layoutInflater;

    public FriendSuggestionAdapter(Context context, ArrayList<FriendsModel> suggestionList) {
        this.context = context;
        this.suggestionList = suggestionList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_friend_suggestion, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        FriendsModel frnMdl = suggestionList.get(position);

        if (!TextUtils.isEmpty(frnMdl.getImgUrl())) {
            Picasso.with(context).load(frnMdl.getImgUrl()).into(viewHolder.userImg);
        }
        viewHolder.userName.setText(frnMdl.getUserName());
        if (frnMdl.getRequestStatus().equalsIgnoreCase(FriendsModel.SENT) ||
                frnMdl.getRequestStatus().equalsIgnoreCase(FriendsModel.SEND)) {
            viewHolder.sendRequest.setText("Cancel Request");
        } else {
            viewHolder.sendRequest.setText("Send Request");
        }
    }

    @Override
    public int getItemCount() {
        return suggestionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView userImg;
        TextView userName;
        Button sendRequest;

        public ViewHolder(View itemView) {
            super(itemView);

            userImg = itemView.findViewById(R.id.suggestion_user_image);
            userImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, suggestionList.get(getLayoutPosition()), "0");
                }
            });
            userName = itemView.findViewById(R.id.suggest_frn_name);
            userName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, suggestionList.get(getLayoutPosition()), "0");
                }
            });

            sendRequest = itemView.findViewById(R.id.send_request);
            sendRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FriendsModel frnMdl = suggestionList.get(getLayoutPosition());
                    if (frnMdl.getRequestStatus().equalsIgnoreCase(FriendsModel.SENT) ||
                            frnMdl.getRequestStatus().equalsIgnoreCase(FriendsModel.SEND)) {
                        getFriendStatusTask(FriendsModel.CANCEL_REQUEST, getLayoutPosition());
                    } else {

                        getFriendStatusTask(FriendsModel.SENT, getLayoutPosition());
                    }
                }
            });
        }
    }

    private void getFriendStatusTask(final String request, final int position) {
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("friend_id", suggestionList.get(position).getUserId());
        postMap.put("type", request);

        vHelper.addVolleyRequestListeners(Api.getInstance().friend_ManagementUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (request.equalsIgnoreCase(FriendsModel.CANCEL_REQUEST)) {
                                    suggestionList.get(position).setRequestStatus(FriendsModel.NONE);
                                } else {
                                    suggestionList.get(position).setRequestStatus(FriendsModel.SENT);
                                }
                                notifyItemChanged(position);
                            } else {
                                AlertUtils.showErrorAlert(context, res.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            Logger.e("getFriendStatusTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getFriendStatusTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(context, "Error !!!", errMsg);
                    }
                }, "getFriendStatusTask");
    }
}
