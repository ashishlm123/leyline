package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.CommentActivity;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.HashTagSearchActivity;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.MyTextUtils;
import com.emts.leyline.models.FeedModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Prabin on 9/15/2017.
 */

public class HashSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FeedModel> feedsList;
    public static int EVENT_RATE = 123;
    private LayoutInflater inflater;
    private String hashTag;

    public void setHastTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public HashSearchAdapter(Context context, ArrayList<FeedModel> feedsList) {
        this.feedsList = feedsList;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.item_hashtag_search_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        FeedModel feedsModel = feedsList.get(position);
        viewHolder.eventName.setText(MyTextUtils.checkNull(feedsModel.getEventName()));
        if (!TextUtils.isEmpty(feedsModel.getEventImage())) {
            Picasso.with(context).load(feedsModel.getEventImage()).into(viewHolder.eventImage);
        }
        viewHolder.keyHash.setText(feedsModel.getFeedTitle());
        viewHolder.commentorName.setText(feedsModel.getRelationBy().getUserName());

        if (!TextUtils.isEmpty(feedsModel.getRelationBy().getImgUrl())) {
            Picasso.with(context).load(feedsModel.getRelationBy().getImgUrl()).into(viewHolder.commentorPic);
        }

    }

    @Override
    public int getItemCount() {
        return feedsList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView keyHash, eventName, commentorName;
        ImageView eventImage, commentorPic;
        RelativeLayout eventDetails;
        LinearLayout hashView;


        ViewHolder(View itemView) {
            super(itemView);

            keyHash = itemView.findViewById(R.id.keyword_hashtag);
            eventName = itemView.findViewById(R.id.event_name);
            eventImage = itemView.findViewById(R.id.event_image);
            commentorName = itemView.findViewById(R.id.commentor_name);
            commentorPic = itemView.findViewById(R.id.commentor_profile_pic);
            eventDetails = itemView.findViewById(R.id.event_details);
            hashView = itemView.findViewById(R.id.hash_view);

            hashView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.putExtra("eventid", feedsList.get(getLayoutPosition()).getEventId());
                    context.startActivity(intent);
                }
            });

            commentorName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, feedsList.get(getLayoutPosition()).getRelationBy(), "0");

                }
            });

            commentorPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, feedsList.get(getLayoutPosition()).getRelationBy(), "0");
                }
            });

            eventDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getEventDetails(getLayoutPosition());
                }
            });
        }

        private void getEventDetails(int position) {
            Intent intent = new Intent(context, EventDetailsActivity.class);
            intent.putExtra("eventmodel", feedsList.get(position));
            intent.putExtra("position", position);
            ((HashTagSearchActivity) context).startActivityForResult(intent, EVENT_RATE);
        }
    }
}
