package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.ChatActivity;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.models.ChatThread;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;

/**
 * Created by Prabin on 9/12/2017.
 */

public class ChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ChatThread> chatList;

    public ChatListAdapter(ArrayList<ChatThread> chatList, Context context) {
        this.chatList = chatList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatListAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_chat_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        ChatThread chatListModel = chatList.get(position);
        viewHolder.friendsName.setText(chatListModel.getSentBy().getUserName());
        viewHolder.latestMsgPreview.setText(StringEscapeUtils.unescapeJava(chatListModel.getLatestMsgPreview()));
        viewHolder.chatTime.setText(DateUtils.getPostTimeAgoV2(chatListModel.getChatTime()));
        if (!TextUtils.isEmpty(chatListModel.getSentBy().getImgUrl())) {
            Picasso.with(context).load(chatListModel.getSentBy().getImgUrl()).into(viewHolder.profileIcon);
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profileIcon;
        TextView friendsName, latestMsgPreview, chatTime;

        ViewHolder(final View itemView) {
            super(itemView);

            profileIcon = itemView.findViewById(R.id.friends_profile_icon);
            friendsName = itemView.findViewById(R.id.friends_name);
            latestMsgPreview = itemView.findViewById(R.id.latest_msg_preview);
            chatTime = itemView.findViewById(R.id.chat_time);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chat_thread", chatList.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
