package com.emts.leyline.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.activity.ChatActivity;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.ChatThread;
import com.emts.leyline.models.FriendsModel;
import com.emts.leyline.models.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prabin on 9/6/2017.
 */

public class FriendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<FriendsModel> friendsList;
    private String friendId = " ";
    private boolean isMyProfile = false;

    public void setIsMyProfile(boolean isMyprofile) {
        this.isMyProfile = isMyprofile;
    }

    public FriendsAdapter(ArrayList<FriendsModel> friendsList, Context context) {
        this.friendsList = friendsList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendsAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_friends_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        FriendsModel friendsModel = friendsList.get(position);

        if (!TextUtils.isEmpty(friendsModel.getUserName())) {
            viewHolder.firstLetter.setText(friendsModel.getUserName().substring(0, 1).toUpperCase());
            viewHolder.friendsName.setText(friendsModel.getUserName());
        } else {
            viewHolder.friendsName.setText("N/A");
        }
        if (isMyProfile) {
            if (friendsModel.isFriends()) {
                viewHolder.message.setVisibility(View.VISIBLE);
                viewHolder.accept.setVisibility(View.GONE);
                viewHolder.delFrnReq.setVisibility(View.GONE);
            } else {
                if (friendsModel.getRequestStatus().equalsIgnoreCase(FriendsModel.SEND)) {
                    viewHolder.accept.setVisibility(View.GONE);
                    viewHolder.message.setVisibility(View.GONE);
                    viewHolder.delFrnReq.setVisibility(View.VISIBLE);
                    viewHolder.reqSent.setVisibility(View.VISIBLE);
                } else if (friendsModel.getRequestStatus().equalsIgnoreCase(FriendsModel.RECEIVE)) {
                    viewHolder.accept.setVisibility(View.VISIBLE);
                    viewHolder.message.setVisibility(View.GONE);
                    viewHolder.delFrnReq.setVisibility(View.VISIBLE);
                    viewHolder.reqSent.setVisibility(View.GONE);
                }
            }
        } else {
            viewHolder.accept.setVisibility(View.GONE);
            viewHolder.message.setVisibility(View.GONE);
//            if (!friendsModel.isFriends()) {
//                viewHolder.delFrnReq.setVisibility(View.VISIBLE);
//            } else {
            viewHolder.delFrnReq.setVisibility(View.GONE);
//            }
        }
        friendId = friendsModel.getUserId();
    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView firstLetter, friendsName;
        ImageView message, delFrnReq;
        LinearLayout accept, reqSent;
        RelativeLayout friendsBar;

        ViewHolder(View itemView) {
            super(itemView);
            friendsBar = itemView.findViewById(R.id.friends_bar);
            firstLetter = itemView.findViewById(R.id.first_letter);
            friendsName = itemView.findViewById(R.id.friends_name);
            message = itemView.findViewById(R.id.message);
            accept = itemView.findViewById(R.id.accept_request);
            delFrnReq = itemView.findViewById(R.id.del_frn_req);
            reqSent = itemView.findViewById(R.id.request_sent);

            friendsBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoUserProfile(getLayoutPosition());
                }
            });

            firstLetter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoUserProfile(getLayoutPosition());
                }
            });

            friendsName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoUserProfile(getLayoutPosition());
                }
            });

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFriendStatusTask(FriendsModel.ACCEPT, friendId, getLayoutPosition());
                }
            });
            delFrnReq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFriendStatusTask(FriendsModel.CANCEL_REQUEST, friendId, getLayoutPosition());
                }
            });
            message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FriendsModel frnMdl = friendsList.get(getLayoutPosition());
                    String threadId = frnMdl.getThreadId();

                    if (threadId.equals("0") || threadId.equals("null")) {
                        createThreadTask(frnMdl, getLayoutPosition());
                    } else {
                        Intent intent = new Intent(context, ChatActivity.class);
                        ChatThread thread = new ChatThread();
                        thread.setSentBy(frnMdl);
                        thread.setThreadId(threadId);
                        intent.putExtra("chat_thread", thread);
                        context.startActivity(intent);
                    }
                }
            });
        }
    }

    private void createThreadTask(final FriendsModel friendsModel, final int position) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(context, "Please wait...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("receiver_id", friendsModel.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().createThreadApi, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent(context, ChatActivity.class);
                                ChatThread thread = new ChatThread();
                                thread.setSentBy(friendsModel);
                                String threadId = res.getString("thread_id");
                                friendsList.get(position).setThreadId(threadId);
                                thread.setThreadId(threadId);
                                intent.putExtra("chat_thread", thread);
                                context.startActivity(intent);
                            } else {
                                AlertUtils.showErrorAlert(context, res.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            Logger.e("createThreadTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("createThreadTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(context, "Error !!!", errMsg);
                    }
                }, "createThreadTask");
    }

    private void getFriendStatusTask(final String request, final String friendId, final int layPosition) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(context, "Please wait...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", PreferenceHelper.getInstance(context).getUserId());
        postMap.put("friend_id", friendId);
        postMap.put("type", request);

        vHelper.addVolleyRequestListeners(Api.getInstance().friend_ManagementUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                friendsList.remove(layPosition);
                                notifyItemRemoved(layPosition);
                                if (updateListener != null) {
                                    updateListener.updateFriendList();
                                }
                            } else {
                                AlertUtils.showErrorAlert(context, res.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            Logger.e("getFriendStatusTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getFriendStatusTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(context, "Error !!!", errMsg);
                    }
                }, "getFriendStatusTask");
    }

    public void setUpdateFriendListListener(UpdateFriendListListener listListener) {
        updateListener = listListener;
    }

    UpdateFriendListListener updateListener;

    public interface UpdateFriendListListener {
        void updateFriendList();
    }

    private void gotoUserProfile(int position) {
        UserModel userModel = friendsList.get(position);
        String threadId = friendsList.get(position).getThreadId();
        ProfileActivity.openProfile(context, userModel, threadId);
    }
}
