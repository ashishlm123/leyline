package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.CommentActivity;
import com.emts.leyline.activity.HashTagSearchActivity;
import com.emts.leyline.activity.ProfileActivity;
import com.emts.leyline.activity.ReplyActivity;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.models.CommentModel;
import com.emts.leyline.models.UserModel;
import com.hendraanggrian.socialview.SocialView;
import com.hendraanggrian.widget.SocialTextView;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;

/**
 * Created by Srijana on 10/4/2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CommentModel> commentList;
    private boolean isReplied;
    private boolean isLoadMoreEnable = false;
    private static final int ROW_TYPE_LOAD_EARLIER_MESSAGES = 0;
    private static final int ROW_TYPE_COMMENT = 1;
    private LayoutInflater inflater;
    private LoadEarlierMessages loadListener;

    public void setIsLoadMoreEnable(boolean isLoadMoreEnable) {
        this.isLoadMoreEnable = isLoadMoreEnable;
    }

    public CommentAdapter(Context context, ArrayList<CommentModel> commentList) {
        this.context = context;
        this.commentList = commentList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ROW_TYPE_LOAD_EARLIER_MESSAGES) {
            View view = inflater.inflate(R.layout.item_load_more, parent, false);
            if (isLoadMoreEnable) {
                view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            } else {
                view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0));
            }
            return new LoadEarlierMsgViewHolder(view);
        } else {
            return new CommentAdapter.ViewHolder(inflater.inflate(R.layout.item_comment, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ROW_TYPE_LOAD_EARLIER_MESSAGES:
                LoadEarlierMsgViewHolder loadEarlierMsgsViewHolder =
                        (LoadEarlierMsgViewHolder) holder;
                loadEarlierMsgsViewHolder.progressLoadMore.setVisibility(View.GONE);
                loadEarlierMsgsViewHolder.loadMore.setVisibility(View.VISIBLE);
                if (isLoadMoreEnable) {
                    loadEarlierMsgsViewHolder.itemView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    loadEarlierMsgsViewHolder.itemView.setVisibility(View.VISIBLE);
                } else {
                    loadEarlierMsgsViewHolder.itemView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0));
                    loadEarlierMsgsViewHolder.itemView.setVisibility(View.GONE);
                }
                break;
            case ROW_TYPE_COMMENT:
                ViewHolder viewHolder = (ViewHolder) holder;
                CommentModel commentModel = commentList.get(position - 1);
                Picasso.with(context).load((commentModel.getImgUrl())).into(viewHolder.profilePic);
                viewHolder.userName.setText(commentModel.getUserName());
//                viewHolder.cmntBody.setText(commentModel.getCmntBody());
                viewHolder.cmntBody.setText(StringEscapeUtils.unescapeJava(commentModel.getCmntBody()));
                viewHolder.cmntTime.setText(DateUtils.getPostTimeAgoV2(commentModel.getCmntTime()));

                if (commentModel.getReplyCount() == 0) {
                    viewHolder.cmntReplies.setText("Reply");
                } else if (commentModel.getReplyCount() == 1) {
                    viewHolder.cmntReplies.setText(commentModel.getReplyCount() + " " + "Reply");
                } else {
                    viewHolder.cmntReplies.setText(commentModel.getReplyCount() + " " + "Replies");
                }

//              if (helper.getUserId().equals(commentList.get(position).getUserId())) {
//                  viewHolder.edtDelCommentHolder.setVisibility(View.VISIBLE);
//              } else {
//                  viewHolder.edtDelCommentHolder.setVisibility(View.GONE);
//              }

                break;
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView profilePic;
        TextView userName, cmntTime, cmntReplies;
        SocialTextView cmntBody;
        LinearLayout edtDelCommentHolder;

        ViewHolder(final View itemView) {
            super(itemView);
            edtDelCommentHolder = itemView.findViewById(R.id.edt_del_holder);
            profilePic = itemView.findViewById(R.id.friends_profile_pic);
            profilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProfileActivity.openProfile(context, commentList.get(getLayoutPosition() - 1), "0");
                }
            });
            userName = itemView.findViewById(R.id.friend_name);
            cmntBody = itemView.findViewById(R.id.comment_body);
            cmntBody.setOnHashtagClickListener(new Function2<SocialView, CharSequence, Unit>() {
                @Override
                public Unit invoke(SocialView socialView, CharSequence charSequence) {
                    Logger.e("hashTag Clicked", "ON->" + cmntBody.getText().toString() + "/nTAG->" + charSequence);
                    Intent intent = new Intent(context, HashTagSearchActivity.class);
                    intent.putExtra("hashtag", charSequence.toString());
                    context.startActivity(intent);
                    return null;
                }
            });
            cmntBody.setOnMentionClickListener(new Function2<SocialView, CharSequence, Unit>() {
                @Override
                public Unit invoke(SocialView socialView, CharSequence charSequence) {
                    try {
                        JSONArray mentionArray = new JSONArray(commentList.get(getLayoutPosition() - 1).getMentionJsonData());
                        for (int i = 0; i < mentionArray.length(); i++) {
                            JSONObject eachMention = mentionArray.getJSONObject(i);
                            String mentionUserSequence = "@" + eachMention.getString("username")
                                    .replace(" ", MentionPersonAdapter.MENTION_CONCAT_TEXT);

                            Logger.e("mention Clicked", "ON->" + cmntBody.getText().toString()
                                    + "\nMENTION->" + charSequence + "\nFROM DATA-> " + mentionUserSequence);
                            if (mentionUserSequence.trim().equals(charSequence.toString().trim())) {
                                UserModel userModel = new UserModel();
                                userModel.setUserId(eachMention.getString("user_id"));
                                userModel.setUserName(eachMention.getString("username"));
                                userModel.setImgUrl(eachMention.getString("image"));
                                ProfileActivity.openProfile(context, userModel, "0");
                            }
                        }
                    } catch (JSONException e) {
                        Logger.e("comment mention json ex", e.getMessage() + " ");
                    }
                    return null;
                }
            });
            cmntTime = itemView.findViewById(R.id.cmt_time);
            cmntReplies = itemView.findViewById(R.id.cmnt_replies);

            if (isReplied) {
                cmntReplies.setVisibility(View.GONE);
            } else {
                cmntReplies.setVisibility(View.VISIBLE);
            }

            cmntReplies.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ReplyActivity.class);
                    intent.putExtra("comment", commentList.get(getLayoutPosition() - 1));
                    intent.putExtra("eventid", ((CommentActivity) context).eventId);
                    context.startActivity(intent);
                }
            });
        }
    }

    private class LoadEarlierMsgViewHolder extends RecyclerView.ViewHolder {
        TextView loadMore;
        ProgressBar progressLoadMore;

        LoadEarlierMsgViewHolder(View itemView) {
            super(itemView);

            loadMore = itemView.findViewById(R.id.loadMore_text);
            progressLoadMore = itemView.findViewById(R.id.load_more_progress);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMore.setVisibility(View.GONE);
                    progressLoadMore.setVisibility(View.VISIBLE);
                    if (loadListener != null) {
                        loadListener.onLoadEarlierMessages();
                    }
                }
            });
        }
    }


    public interface LoadEarlierMessages {
        void onLoadEarlierMessages();
    }

    public void setLoadEarlierListener(LoadEarlierMessages loadListener) {
        this.loadListener = loadListener;
    }

    @Override
    public int getItemCount() {
        return commentList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ROW_TYPE_LOAD_EARLIER_MESSAGES;
        } else {
            return ROW_TYPE_COMMENT;
        }
    }

    public void isReplied(boolean isReplied) {
        this.isReplied = isReplied;
    }
}
