package com.emts.leyline.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.models.FriendsModel;
import com.emts.leyline.models.UserModel;
import com.hendraanggrian.widget.FilteredAdapter;
import com.squareup.picasso.Picasso;

/**
 * Created by User on 2017-10-31.
 */
public class MentionPersonAdapter extends FilteredAdapter<FriendsModel> {
    Context context;
    public static final String MENTION_CONCAT_TEXT = "_";

    final Filter filter = new FilteredAdapter.SocialFilter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((FriendsModel) resultValue).getUserName().replace(" ", MENTION_CONCAT_TEXT);
        }
    };

    public MentionPersonAdapter(Context context) {
        super(context, R.layout.item_mention, R.id.user_name);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mention, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        FriendsModel item = getItem(position);
        if (item != null) {
//            holder.textView.setText(item.getUserName().replace(" ", "&#160;"));
            holder.textView.setText(item.getUserName().replace(" ", MENTION_CONCAT_TEXT));
            holder.textView.setTag(item);
            if (!TextUtils.isEmpty(item.getImgUrl()) && item.getImgUrl() != null) {
                Picasso.with(context).load(item.getImgUrl()).into(holder.userImage);
            }
        }
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        try {
            return Long.parseLong(getItem(position).getUserId());
        } catch (Exception e) {
            return super.getItemId(position);
        }
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        final TextView textView;
        ImageView userImage;

        ViewHolder(@NonNull View view) {
            this.textView = view.findViewById(R.id.user_name);
            this.userImage = view.findViewById(R.id.user_img);
        }
    }
}
