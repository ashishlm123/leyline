package com.emts.leyline.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.leyline.R;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.MainActivity;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.models.CalendarEventModel;
import com.emts.leyline.models.EventCategory;
import com.emts.leyline.models.EventModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by User on 2017-06-20.
 */

public class CalEventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CalendarEventModel> events;
    private SimpleDateFormat sdfFrom, sdfTo;
    private Calendar cal;
    public static int CAL_EVENT = 135;

    public CalEventAdapter(Context context, ArrayList<CalendarEventModel> events) {
        this.context = context;
        this.events = events;
        sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
        cal = Calendar.getInstance();

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_event_cal, null, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final CalendarEventModel eventList = events.get(position);
        final ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.llEventHolder.removeAllViews();
        for (int j = 0; j < eventList.getEventList().size(); j++) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_event_date, null, false);
            view.setId(j);
            view.setTag(position);
            viewHolder.llEventHolder.addView(view);
            ImageView imageIcon = view.findViewById(R.id.event_icon);
//            imageIcon.setImageResource(eventList.getEventList().get(j).getEventCatIcon());
            EventCategory eventCategory = Config.getEvent(context, eventList.getEventList().get(j).getEventCatId());
            Picasso.with(context).load(eventCategory.getEventCatIcon()).into(imageIcon);
            TextView eventName = view.findViewById(R.id.event_name);
            eventName.setText(eventList.getEventList().get(j).getEventName());

            try {
                Date oldParsedDate = sdfFrom.parse(eventList.getEventList().get(j).getEventDate());
                cal.setTime(oldParsedDate);
                viewHolder.tvDate.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
                String day = "";
                switch (String.valueOf(cal.get(Calendar.DAY_OF_WEEK))) {
                    case Config.Sun:
                        day = "Sun";
                        break;
                    case Config.Mon:
                        day = "Mon";
                        break;
                    case Config.Tues:
                        day = "Tues";
                        break;
                    case Config.Wed:
                        day = "Wed";
                        break;
                    case Config.Thur:
                        day = "Thu";
                        break;
                    case Config.Fri:
                        day = "Fri";
                        break;
                    case Config.Sat:
                        day = "Sat";
                        break;
                }
                viewHolder.tvDay.setText(day);
            } catch (ParseException e) {
                Logger.e("calEventAdapter ex", e.getMessage() + " ");
            }

            imageIcon.setBackgroundColor(Color.parseColor(eventCategory.getEventCatColor()));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, EventDetailsActivity.class);
                    intent.putExtra("eventmodel", eventList.getEventList().get(view.getId()));
                    intent.putExtra("position", view.getId());
                    intent.putExtra("calendarPosition", (int) view.getTag());
                    ((MainActivity) context).startActivityForResult(intent, CAL_EVENT);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvDay;
        LinearLayout llEventHolder;

        ViewHolder(View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tvDate);
            tvDay = itemView.findViewById(R.id.tvBar);
            llEventHolder = itemView.findViewById(R.id.holder_tv_events);
        }
    }
}
