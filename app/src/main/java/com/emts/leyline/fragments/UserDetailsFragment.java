package com.emts.leyline.fragments;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.activity.SettingsActivity;
import com.emts.leyline.activity.WebsiteActivity;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.UserInterestsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import cn.lankton.flowlayout.FlowLayout;


public class UserDetailsFragment extends Fragment {
    PreferenceHelper helper;
    ArrayList<UserInterestsModel> interestList;
    LinearLayout holderAboutMe;
    TextView userBio, tvRate, errorMesg, userWebSite, userDob, userGender, userHometown;
    FlowLayout flowLayout;
    String userId = "";
    RatingBar ratingBar;
    Bundle bundle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bundle = this.getArguments();
        if (bundle != null) {
            userId = bundle.getString("prof_id");
        }

        return inflater.inflate(R.layout.fragment_user_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());
        tvRate = view.findViewById(R.id.tv_rate);
        userBio = view.findViewById(R.id.user_bio);
        userWebSite = view.findViewById(R.id.tv_website_link);
        interestList = new ArrayList<>();

        userDob = view.findViewById(R.id.userDob);
        userGender = view.findViewById(R.id.userGender);
        userHometown = view.findViewById(R.id.userHomeTown);
        holderAboutMe = view.findViewById(R.id.lay_about_me);
        errorMesg = view.findViewById(R.id.error_profile);
        ratingBar = view.findViewById(R.id.rating_bar);

        flowLayout = view.findViewById(R.id.flowlayout);

        tvRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRateAlert();
            }
        });

        if (!helper.getUserId().equals(userId)) {
            setUserData();
        }


        userWebSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userWebSite.getText().toString().equalsIgnoreCase("N/A")) {
                    AlertUtils.showToast(getActivity(), "Website not available..");
                    return;
                }
                Intent intent = new Intent(getActivity(), WebsiteActivity.class);
                intent.putExtra("website", userWebSite.getText().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (helper.getUserId().equals(userId)) {
            String[] interestList = helper.getString(PreferenceHelper.APP_USER_INTERESTS, "")
                    .split(SettingsActivity.INTEREST_CONCAT_TEXT);
            ArrayList<String> interests = new ArrayList<>();
            Collections.addAll(interests, interestList);
            bundle.putStringArrayList("interestList", interests);
            bundle.putString("bio", helper.getString(PreferenceHelper.APP_USER_BIO, ""));
            bundle.putString("website", helper.getString(PreferenceHelper.APP_USER_WEBSITE, ""));
            bundle.putString("hometown", helper.getString(PreferenceHelper.USER_HOME_TOWN, ""));
            bundle.putString("gender", helper.getString(PreferenceHelper.USER_GENDER, "M"));
            setUserData();
        }
    }

    private void setUserData() {
        if (bundle == null) {
            return;
        }
        String hometown = bundle.getString("hometown");
        if (hometown == null || hometown.equals("null") || hometown.trim().equalsIgnoreCase("")) {
            userHometown.setText("N/A");
        } else {
            userHometown.setText(hometown);
        }
        String gender = bundle.getString("gender");
        if (gender.equalsIgnoreCase("F")) {
            userGender.setText("Female");
        } else {
            userGender.setText("Male");
        }

        String dob = bundle.getString("dob");
        if (dob == null || dob.equals("null") || dob.trim().equalsIgnoreCase("")) {
            userDob.setText("N/A");
        } else {
            userDob.setText(dob);
        }

        String bio = bundle.getString("bio");
        if (bio == null || bio.equals("null") || bio.trim().equalsIgnoreCase("")) {
            userBio.setText("N/A");
        } else {
            userBio.setText(bio);
        }

        String website = bundle.getString("website");
        if (website == null || website.equals("null") || website.trim().equalsIgnoreCase("")) {
            userWebSite.setText("N/A");
        } else {
            userWebSite.setText(Html.fromHtml("<u>" + website + "</>"));
            userWebSite.setTextColor(getResources().getColor(R.color.colorAccent));
        }

        ArrayList<String> interestList = bundle.getStringArrayList("interestList");
        flowLayout.removeAllViews();
        if (interestList == null || interestList.size() < 1) {
            addNoInterestView();
        } else {
            for (int i = 0; i < interestList.size(); i++) {
                String interestTitle = interestList.get(i);
                if (TextUtils.isEmpty(interestTitle) || interestTitle.equalsIgnoreCase("null")) {
                    continue;
                }
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_user_interests,
                        null, false);
                TextView textView = view.findViewById(R.id.user_interests);
                textView.setText(interestList.get(i).trim());
                flowLayout.addView(view);
            }
            if (flowLayout.getChildCount() <= 0) {
                addNoInterestView();
            }
        }
        ratingBar.setRating(bundle.getFloat("rating"));
        if (userId.equals(helper.getUserId())) {
            tvRate.setVisibility(View.GONE);
        } else {
            tvRate.setVisibility(View.VISIBLE);
        }
    }

    private void addNoInterestView() {
        TextView textView = new TextView(getActivity());
        textView.setText("N/A");
        textView.setTextColor(Color.WHITE);
        Typeface railWay = Typeface.createFromAsset(getActivity().getAssets(), "fonts/raleway_light.ttf");
        textView.setTypeface(railWay);
        flowLayout.addView(textView);
    }

    private void showRateAlert() {
        final AlertDialog.Builder alertDailog = new AlertDialog.Builder(getActivity());
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.alert_rate_event, null);
        alertDailog.setView(view);
        final RatingBar rate = view.findViewById(R.id.rating_bar);
        TextView ratingTitle = view.findViewById(R.id.tv_rate);
        ratingTitle.setText("Rate this User");
        Button btnOk = view.findViewById(R.id.btn_ok);
        Button btnCancel = view.findViewById(R.id.btn_cancel);
        final Dialog dialog = alertDailog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userRatingTask(rate.getRating());
                dialog.dismiss();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void userRatingTask(final Float rating) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Please wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", helper.getUserId());
        postMap.put("profile_id", userId);
        postMap.put("rate_value", String.valueOf(rating));

        vHelper.addVolleyRequestListeners(Api.getInstance().userRateUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        try {
                            ratingBar.setRating((float) Math.ceil(Float.parseFloat(res.getString("average_rate"))));
                        } catch (Exception e) {
                            ratingBar.setRating(0);
                        }
                    } else {
                        AlertUtils.showErrorAlert(getActivity(), res.getString("message"), null);
                    }
                } catch (JSONException e) {
                    Logger.e("userRatingTask excep", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userRatingTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(getActivity(), "Error !!!", errMsg);
            }
        }, "userRatingTask");
    }
}
