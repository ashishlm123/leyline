package com.emts.leyline.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.CalEventAdapter;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.CalendarEventModel;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.UserModel;
import com.github.ik024.calendar_lib.custom.MonthView;
import com.github.ik024.calendar_lib.custom.YearView;
import com.github.ik024.calendar_lib.listeners.MonthViewClickListeners;
import com.github.ik024.calendar_lib.listeners.YearViewClickListeners;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CalendarFrag extends Fragment {
    ArrayList<CalendarEventModel> events;

    RecyclerView dragView;
    CalEventAdapter adapter;
    TextView tvErrorEvent;
    ProgressBar progressEvent;

    SlidingUpPanelLayout slidingUpPanelLayout;
    YearView yearView;
    MonthView monthView;
    TextView tabMonthView, tabYearView;
//    boolean isSliderOpen = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        events = new ArrayList<>();

        tvErrorEvent = view.findViewById(R.id.error_tv);
        progressEvent = view.findViewById(R.id.progress_event);
        dragView = view.findViewById(R.id.drag_view);
        dragView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new CalEventAdapter(getActivity(), events);
        dragView.setAdapter(adapter);

        slidingUpPanelLayout = view.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setDragView(dragView);
//        slidingUpPanelLayout.setScrollableViewHelper(new NestedScrollableViewHelper());
        slidingUpPanelLayout.setScrollableView(dragView);
        slidingUpPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });
//        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
//            @Override
//            public void onPanelSlide(View panel, float slideOffset) {
//                if (slideOffset>=1){
//                    isSliderOpen = true;
//                }else{
//                    isSliderOpen = false;
//                }
//            }
//
//            @Override
//            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState,
//                                            SlidingUpPanelLayout.PanelState newState) {
//            }
//        });

        // Setup calendar pages
        yearView = view.findViewById(R.id.calendar_year_view);
        monthView = view.findViewById(R.id.calendar_month_view);

        yearView.registerYearViewClickListener(new YearViewClickListeners() {
            @Override
            public void dateClicked(int year, int month, int day) {
                //adding events to the calendar
                List<Date> eventDate = new ArrayList<>();
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                eventDate.add(calendar.getTime());
                Logger.e("selected date in int:: ", "YEAR:" + year + " MONTH:" + month + " DAY:" + day);
                Logger.e("selected date is :", calendar.getTime().toString() + " ::");
                yearView.setEventList(eventDate);

                if (NetworkUtils.isInNetwork(getActivity())) {
                    getCalendarEventTask(year + "-" +
                            String.format("%02d", month + 1) + "-" +String.format("%02d", day) );
                    tvErrorEvent.setVisibility(View.GONE);
                } else {
                    tvErrorEvent.setText(R.string.no_internet);
                    tvErrorEvent.setVisibility(View.VISIBLE);
                }
            }
        });
        monthView.setVisibility(View.VISIBLE);

        monthView.registerClickListener(new MonthViewClickListeners() {
            @Override
            public void dateClicked(Date dateClicked) {
                //adding events to the calendar
                List<Date> eventDate = new ArrayList<>();
                eventDate.add(dateClicked);
                monthView.setEventList(eventDate);

                if (NetworkUtils.isInNetwork(getActivity())) {
                    Calendar selectedCal = Calendar.getInstance();
                    selectedCal.setTime(dateClicked);
                    getCalendarEventTask(selectedCal.get(Calendar.YEAR) + "-" +
                            String.format("%02d", (selectedCal.get(Calendar.MONTH) + 1)) + "-"
                            + String.format("%02d", selectedCal.get(Calendar.DATE)));
                    tvErrorEvent.setVisibility(View.GONE);
                } else {
                    tvErrorEvent.setText(R.string.no_internet);
                    tvErrorEvent.setVisibility(View.VISIBLE);
                }
            }
        });

        tabMonthView = view.findViewById(R.id.tab_month_view);
        tabYearView = view.findViewById(R.id.tab_year_view);
        tabMonthView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectMonthTab(true);
            }
        });
        tabYearView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectMonthTab(false);
            }
        });

        selectMonthTab(true);

        if (NetworkUtils.isInNetwork(getActivity())) {
            Calendar calendar = Calendar.getInstance();
            if (monthView.getVisibility() == View.VISIBLE) {
                getCalendarEventTask(calendar.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendar.get(Calendar.MONTH) + 1)) + "-"
                        + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)));
            } else {
                getCalendarEventTask(calendar.get(Calendar.YEAR) + "-" +
                        String.format("%02d", (calendar.get(Calendar.MONTH) + 1)) + "-" + calendar.get(Calendar.DAY_OF_MONTH));
            }
            tvErrorEvent.setVisibility(View.GONE);
        } else {
            tvErrorEvent.setText(R.string.no_internet);
            tvErrorEvent.setVisibility(View.VISIBLE);
        }
    }

    private void selectMonthTab(boolean isMonth) {
        if (isMonth) {
            tabMonthView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            tabYearView.setBackgroundColor(Color.TRANSPARENT);

            monthView.setVisibility(View.VISIBLE);
            yearView.setVisibility(View.GONE);
        } else {
            tabYearView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            tabMonthView.setBackgroundColor(Color.TRANSPARENT);

            yearView.setVisibility(View.VISIBLE);
            monthView.setVisibility(View.GONE);
        }
    }

    private void getCalendarEventTask(final String date) {
        events.clear();
        adapter.notifyDataSetChanged();
        progressEvent.setVisibility(View.VISIBLE);
        tvErrorEvent.setVisibility(View.GONE);
        dragView.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("date", date);
        postMap.put("user_id", PreferenceHelper.getInstance(getActivity()).getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getEventViaDate, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressEvent.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);

                            if (res.getBoolean("status")) {
                                JSONArray infoArray = res.getJSONArray("info");

                                if (infoArray.length() < 0) {
                                    tvErrorEvent.setText(res.getString("message"));
                                    tvErrorEvent.setVisibility(View.VISIBLE);
                                    dragView.setVisibility(View.GONE);
                                } else {

                                    for (int i = 0; i < infoArray.length(); i++) {
                                        JSONArray eventArray = infoArray.getJSONArray(i);
                                        CalendarEventModel eventList = new CalendarEventModel();
                                        ArrayList<EventModel> eventDateList = new ArrayList<>();

                                        for (int j = 0; j < eventArray.length(); j++) {
                                            JSONObject eachObj = eventArray.getJSONObject(j);

                                            EventModel eventModel = new EventModel();
                                            JSONArray hostArray = eachObj.getJSONArray("host_name");
                                            UserModel[] hostUsers = new UserModel[hostArray.length()];
                                            for (int k = 0; k < hostArray.length(); k++) {
                                                JSONObject eachHostInfo = hostArray.getJSONObject(k);
                                                UserModel hostedBy = new UserModel();
                                                hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                                hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                                if (!eachHostInfo.getString("host_image").equals("")) {
                                                    hostedBy.setImgUrl(res.getString("profileimage_default_url") + eachHostInfo.getString("host_image"));
                                                } else {
                                                    hostedBy.setImgUrl("");
                                                }
                                                hostUsers[k] = hostedBy;
                                            }
                                            eventModel.setHostedBy(hostUsers);

                                            eventModel.setEventName(eachObj.getString("event_name"));
                                            eventModel.setEventImage(res.getString("event_image_default_url") + eachObj.getString("event_image"));
                                            eventModel.setEventAttendStatus(eachObj.getString("attendance"));

                                            try {
                                                eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                            }catch (Exception e){}
                                            try {
                                                eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));
                                            }catch (Exception e){}

                                            eventModel.setEventDesc(eachObj.getString("description"));
                                            eventModel.setEventDate(eachObj.getString("event_date"));
                                            eventModel.setEventLocation(eachObj.getString("location"));
                                            eventModel.setEventId(eachObj.getString("event_id"));
                                            eventModel.setStatus(eachObj.getString("status"));

                                            try {
                                                eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                            } catch (Exception e) {
                                                eventModel.setEventRating(0);
                                            }
                                            eventModel.setEventCatId(eachObj.getString("event_category_id"));
                                            eventModel.setEventTime(eachObj.getString("event_time"));

                                            JSONObject likedBy = eachObj.getJSONObject("like");
                                            eventModel.setLiked(likedBy.getBoolean("your_like_status"));

                                            eventDateList.add(eventModel);
                                            eventList.setEventList(eventDateList);
                                        }
                                        events.add(eventList);
                                    }
                                    adapter.notifyDataSetChanged();
                                    tvErrorEvent.setVisibility(View.GONE);
                                    dragView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                tvErrorEvent.setText(res.getString("message"));
                                tvErrorEvent.setVisibility(View.VISIBLE);
                                dragView.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            Logger.e("getCalendarEventTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressEvent.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please check internet access and try again latter.";
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                        }
                        tvErrorEvent.setText(errorMsg);
                        dragView.setVisibility(View.GONE);
                        tvErrorEvent.setVisibility(View.VISIBLE);
                    }
                }, "getCalendarEventTask");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CalEventAdapter.CAL_EVENT) {
                int position = data.getIntExtra("position", -1);
                int calendarPosition = data.getIntExtra("calenderPosition", -1);
                Logger.e("position1", String.valueOf(calendarPosition) + " ");

                if (position >= 0) {
                    Logger.e("position2", String.valueOf(position) + " ");
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    if (eventModel != null) {
                        try {
                            Logger.e("position3", String.valueOf(calendarPosition) + " ");
                            events.get(calendarPosition).getEventList().set(position, eventModel);
                        } catch (Exception e) {
                            Logger.e("calonresultAct ex", e.getMessage());
                        }
                    }
                }
            }
        }
    }

    public boolean onBackPressed() {
        if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return false;
        } else {
            return true;
        }
    }
}
