package com.emts.leyline.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.leyline.R;

/**
 * Created by User on 2017-11-16.
 */

public class PrivateFrag extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_private, container, false);
        TextView tvMsg = view.findViewById(R.id.tv_msg);
        tvMsg.setText(getArguments().getString("msg"));
        return view;
    }
}
