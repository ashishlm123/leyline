package com.emts.leyline.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.LocationUpdateService;
import com.emts.leyline.R;
import com.emts.leyline.activity.AddEventActivity;
import com.emts.leyline.activity.EventDetailsActivity;
import com.emts.leyline.activity.MainActivity;
import com.emts.leyline.adapter.EventAdapter;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventCategory;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.UserModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class EventsNearMeFrag extends Fragment {
    private GoogleMap mMap;
    ArrayList<EventModel> eventList;
    TextView eventName, tvRadius;
    Spinner categorySpinner, timeSpinner;
    SeekBar radiusSeekBar;
    EditText hostName, searchEventName, searchHashTag;
    int distance = 5000;
    private int day, month, year;
    public static int EVENT_NEAR_ME = 134;
    SlidingUpPanelLayout slidingUpPanelLayout;
    EventModel eventModel;
    ArrayList<EventModel> alertEventList = new ArrayList<>();
    EventAdapter eventAdapter;
    Dialog dialog;

    String category = "", date = "", host = "", event = "", hash = "", radius = "";
    ArrayList<EventCategory> allEventCategory = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events_near_me, container, false);
        SupportMapFragment supportMapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                LatLng nyCity;

                if (LocationUpdateService.lastKnownLocation != null) {
                    nyCity = new LatLng(LocationUpdateService.lastKnownLocation.getLatitude(), LocationUpdateService.lastKnownLocation.getLongitude());
                } else {
                    nyCity = new LatLng(40.785091, -73.968285);

                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nyCity, 13));
            }
        });

        Calendar cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        categorySpinner = view.findViewById(R.id.category_spinner);
        timeSpinner = view.findViewById(R.id.time_spinner);
        hostName = view.findViewById(R.id.host_name);
        searchEventName = view.findViewById(R.id.search_event_name);
        searchHashTag = view.findViewById(R.id.search_hash_tag);
        radiusSeekBar = view.findViewById(R.id.seekbar_radius);
        tvRadius = view.findViewById(R.id.radius_value);

        slidingUpPanelLayout = view.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (slideOffset >= 1) {
                    //panel opyan
                } else {
                    AlertUtils.hideInputMethod(getContext(), tvRadius);
                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState,
                                            SlidingUpPanelLayout.PanelState newState) {
            }
        });

        allEventCategory = Config.getAllEventTypes(getActivity());

        ArrayList<String> categoryList = new ArrayList<>();
        categoryList.add("Category");
        categoryList.add("All");
        for (EventCategory category : allEventCategory) {
            categoryList.add(category.getEventCatName());
        }

        String[] timeList = getActivity().getResources().getStringArray(R.array.search_event_time);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, R.id.item_spinner, categoryList);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.item_spinner, R.id.item_spinner, timeList);
        categorySpinner.setAdapter(adapter);
        timeSpinner.setAdapter(adapter1);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    category = "";
                } else if (i != 0) {
                    category = allEventCategory.get(i - 2).getEventCatId();
                }
                if (i > 0) {
                    searchNearbyEventTask();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    date = "";
                    searchNearbyEventTask();
                } else if (i == 2) {
                    date = "today";
                    searchNearbyEventTask();
                } else if (i == 3) {
                    date = "tomorrow";
                    searchNearbyEventTask();
                } else if (i == 4) {
                    date = "this_week";
                    searchNearbyEventTask();
                } else if (i == 5) {
                    date = "this_month";
                    searchNearbyEventTask();
                } else if (i == 6) {
                    DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            date = year + "-" + String.format("%02d", monthOfYear + 1) + "-" + String.format("%02d", dayOfMonth);
                            Logger.e("fucking date selected", date + " ");
                            searchNearbyEventTask();
                        }
                    };

                    DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, year, month, day);
                    dpDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        hostName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    host = hostName.getText().toString();
                    searchNearbyEventTask();
                } else {
                    host = "";
                    searchNearbyEventTask();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        searchEventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    event = searchEventName.getText().toString();
                    searchNearbyEventTask();
                } else {
                    event = "";
                    searchNearbyEventTask();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        searchHashTag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    hash = searchHashTag.getText().toString();
                    searchNearbyEventTask();
                } else {
                    hash = "";
                    searchNearbyEventTask();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        radiusSeekBar.setMax(5);
        radiusSeekBar.setProgress(2);
        tvRadius.setText("( 2000 km)");

        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() == 0) {
                    radius = "500";
                    searchNearbyEventTask();
                    tvRadius.setText("( " + String.valueOf(radius) + " km )");
                } else {
                    radius = String.valueOf(seekBar.getProgress() * 500);
                    tvRadius.setText("( " + String.valueOf(radius) + " km )");
                    searchNearbyEventTask();
                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getNearByEventsTask();
        }
        return view;
    }

    public boolean onBackPressed() {
        if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void getNearByEventsTask() {
        eventList.clear();

        if (mMap != null) {
            mMap.clear();
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        if (LocationUpdateService.lastKnownLocation != null) {
            postMap.put("longitude", String.valueOf(LocationUpdateService.lastKnownLocation.getLongitude()));
            postMap.put("latitude", String.valueOf(LocationUpdateService.lastKnownLocation.getLatitude()));
        } else {
            postMap.put("latitude", "40.785091");
            postMap.put("longitude", "-73.968285");
        }
        postMap.put("distance", String.valueOf(distance));
        postMap.put("user_id", PreferenceHelper.getInstance(getActivity()).getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getNearByEventsUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray eventDetails = res.getJSONArray("event_details");
                                for (int i = 0; i < eventDetails.length(); i++) {
                                    JSONObject eachObj = eventDetails.getJSONObject(i);
                                    EventModel eventModel = new EventModel();

                                    JSONArray hostArray = eachObj.getJSONArray("host_name");
                                    UserModel[] hostUsers = new UserModel[hostArray.length()];
                                    for (int j = 0; j < hostArray.length(); j++) {
                                        JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                        UserModel hostedBy = new UserModel();
                                        hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                        hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                        if (!eachHostInfo.getString("host_image").equals("")) {
                                            hostedBy.setImgUrl(res.getString("profile_img_Url") + eachHostInfo.getString("host_image"));
                                        } else {
                                            hostedBy.setImgUrl("");
                                        }
                                        hostUsers[j] = hostedBy;
                                    }
                                    eventModel.setHostedBy(hostUsers);

                                    eventModel.setEventName(eachObj.getString("event_name"));
                                    String imageImage = eachObj.optString("event_image");
                                    if (!TextUtils.isEmpty(imageImage)) {
                                        eventModel.setEventImage(res.getString("event_image_url") + imageImage);
                                    } else {
                                        eventModel.setEventImage("");
                                    }
                                    eventModel.setEventAttendStatus(eachObj.getString("attendance"));

                                    eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                    eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));

                                    eventModel.setEventDesc(eachObj.getString("description"));
                                    eventModel.setEventDate(eachObj.getString("event_date"));
                                    eventModel.setEventLocation(eachObj.getString("location"));
                                    eventModel.setEventId(eachObj.getString("event_id"));
                                    eventModel.setStatus(eachObj.getString("status"));

                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }
                                    eventModel.setEventCatId(eachObj.getString("event_category_id"));
                                    eventModel.setEventTime(eachObj.getString("event_time"));
                                    JSONObject likedBy = eachObj.getJSONObject("like");
                                    eventModel.setLiked(likedBy.getBoolean("your_like_status"));
                                    eventList.add(eventModel);
                                }
                                if (mMap != null) {
                                    for (int i = 0; i < eventList.size(); i++) {
                                        showEventMarker(i, false);
                                    }
                                }
                            } else {
                                AlertUtils.showSnack(getActivity(), tvRadius,
                                        String.valueOf(Html.fromHtml(res.getString("message"))));
                            }
                        } catch (JSONException e) {
                            Logger.e("getNearByEventsTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getNearByEventTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(getActivity(), "Error !!!", errMsg);

                    }
                }, "getNearByEventsTask");
    }

    private void searchNearbyEventTask() {
        if (mMap != null) {
            mMap.clear();
        }
        eventList.clear();
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.cancelRequest("searchNearbyEventTask");
        HashMap<String, String> postMap = vHelper.getPostParams();
        if (!TextUtils.isEmpty(category)) {
            postMap.put("event_type_id", category);
        }
        if (!TextUtils.isEmpty(host)) {
            postMap.put("hostname", host);
        }
        if (!TextUtils.isEmpty(event)) {
            postMap.put("event_key", event);
        }
        if (!TextUtils.isEmpty(hash)) {
            if (hash.startsWith("#")) {
                postMap.put("hash_key", hash);
            } else {
                postMap.put("hash_key", "#" + hash);
            }

        }
        if (!TextUtils.isEmpty(date)) {
            postMap.put("date", date);
        }
        if (!TextUtils.isEmpty(String.valueOf(radius))) {
            postMap.put("distance", String.valueOf(radius));
        } else {
            postMap.put("distance", String.valueOf(distance));
        }

        if (LocationUpdateService.lastKnownLocation != null) {
            postMap.put("longitude", String.valueOf(LocationUpdateService.lastKnownLocation.getLongitude()));
            postMap.put("latitude", String.valueOf(LocationUpdateService.lastKnownLocation.getLatitude()));
        } else {
            postMap.put("latitude", "40.785091");
            postMap.put("longitude", "-73.968285");
        }
        postMap.put("user_id", PreferenceHelper.getInstance(getActivity()).getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().searchEventUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray eventDetails = res.getJSONArray("events_list");
                                for (int i = 0; i < eventDetails.length(); i++) {
                                    JSONObject eachObj = eventDetails.getJSONObject(i);
                                    EventModel eventModel = new EventModel();

                                    JSONArray hostArray = eachObj.getJSONArray("host_name");
                                    UserModel[] hostUsers = new UserModel[hostArray.length()];
                                    for (int j = 0; j < hostArray.length(); j++) {
                                        JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                        UserModel hostedBy = new UserModel();
                                        hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                        hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                        if (!eachHostInfo.getString("host_image").equals("")) {
                                            hostedBy.setImgUrl(res.getString("profile_img_Url") + eachHostInfo.getString("host_image"));
                                        } else {
                                            hostedBy.setImgUrl("");
                                        }
                                        hostUsers[j] = hostedBy;
                                    }
                                    eventModel.setHostedBy(hostUsers);

                                    eventModel.setEventName(eachObj.getString("event_name"));
                                    eventModel.setEventImage(res.getString("event_image_url") + eachObj.getString("image"));
                                    eventModel.setEventAttendStatus(eachObj.getString("attendance"));

                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eachObj.getString("rating")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }
                                    String lat = eachObj.getString("latitude");
                                    try {
                                        eventModel.setLatitude(Double.parseDouble(lat));

                                    } catch (Exception e) {
                                        eventModel.setLatitude(0);
                                    }
                                    String lng = eachObj.getString("longitude");
                                    try {
                                        eventModel.setLongitude(Double.parseDouble(lng));
                                    } catch (Exception e) {
                                        eventModel.setLongitude(0);
                                    }

                                    eventModel.setEventDesc(eachObj.getString("description"));
                                    eventModel.setEventDate(eachObj.getString("event_date"));
                                    eventModel.setEventLocation(eachObj.getString("location"));
                                    eventModel.setEventId(eachObj.getString("event_id"));
                                    eventModel.setStatus(eachObj.getString("status"));
                                    eventModel.setEventCatId(eachObj.getString("event_category_id"));

                                    JSONObject likedBy = eachObj.getJSONObject("like");
                                    eventModel.setLiked(likedBy.getBoolean("your_like_status"));

                                    eventModel.setEventTime(eachObj.getString("event_time"));
                                    eventList.add(eventModel);
                                }

                                if (mMap != null) {
                                    // markerList.clear();
                                    for (int i = 0; i < eventList.size(); i++) {
                                        showEventMarker(i, false);
                                    }
                                }
                            } else {
                                Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Logger.e("searchNearbyEventTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("searchNearbyEventTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(getActivity(), "Error !!!", errMsg);
                    }
                }, "searchNearbyEventTask");

    }

    HashMap<Integer, Marker> markerList = new HashMap<>();

    private void showEventMarker(final int position, final boolean isRefresh) {
        if (getActivity() == null) return;
        if (mMap == null) return;
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        if (inflater == null) return;
        View marker = inflater.inflate(R.layout.custom_layout_marker, null);

        eventName = marker.findViewById(R.id.event_name);
        ImageView eventIcon = marker.findViewById(R.id.event_icon);
        eventName.setText(eventList.get(position).getEventName());
        final EventCategory eventCategory = Config.getEvent(getActivity(), eventList.get(position).getEventCatId());
        Picasso.with(getActivity()).load(eventCategory.getEventCatIcon()).into(eventIcon, new Callback() {
            @Override
            public void onSuccess() {
                if (isRefresh) return;
                showEventMarker(position, true);
            }

            @Override
            public void onError() {

            }
        });
        eventIcon.setBackgroundColor(Color.parseColor(eventCategory.getEventCatColor()));

        if (isRefresh) {
            Marker marker1 = markerList.get(position);
            if (marker1 != null) {
                marker1.remove();
            }
        }

        LatLng eventLocation = new LatLng(eventList.get(position).getLatitude(), eventList.get(position).getLongitude());
        Marker customMarker = mMap.addMarker(new MarkerOptions()
                .position(eventLocation)
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));

        customMarker.setTag(position);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                EventModel eventModel1 = eventList.get((int) marker.getTag());

                userGetSameLocationEvents(String.valueOf(eventModel1.getLatitude()), String.valueOf(eventModel1.getLongitude()), marker);



                return false;
            }
        });

        markerList.put(position, customMarker);
    }

    ProgressDialog pDialog = null;

    private void userGetSameLocationEvents(String latitude, String longitude, final Marker marker) {

        pDialog = AlertUtils.showProgressDialog(getContext(), "Searching for events in same Location ...");

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("latitude", latitude);
        postMap.put("longitude", longitude);

        vHelper.addVolleyRequestListeners(Api.getInstance().eventInSameLocationUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        alertEventList.clear();
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray eventArray = res.getJSONArray("event_similar");
                                for (int i = 0; i < eventArray.length(); i++) {
                                    JSONObject eachEvent = eventArray.getJSONObject(i);
                                    EventModel eventModel = new EventModel();
                                    JSONObject eventDetail = eachEvent.getJSONObject("event_detail");
                                    eventModel.setEventName(eventDetail.getString("event_name"));
                                    eventModel.setEventId(eventDetail.getString("event_id"));
                                    eventModel.setEventTime(eventDetail.getString("event_time"));
                                    eventModel.setEventDate(eventDetail.getString("event_date"));
                                    eventModel.setStatus(eventDetail.getString("status"));
                                    eventModel.setEventDesc(eventDetail.getString("description"));
                                    eventModel.setEventLocation(eventDetail.getString("location"));
                                    eventModel.setLiked(eventDetail.getBoolean("like_status"));
                                    eventModel.setLatitude(eventDetail.getDouble("latitude"));
                                    eventModel.setLongitude(eventDetail.getDouble("longitude"));


                                    if (!TextUtils.isEmpty(eventDetail.optString("attendance"))) {
                                        eventModel.setEventAttendStatus(eventDetail.optString("attendance"));
                                    } else {
                                        eventModel.setEventAttendStatus(EventModel.EVENT_NONE);
                                    }

                                    if (!eventDetail.getString("event_image").equals("")) {
                                        eventModel.setEventImage(res.getString("event_image_default_url")
                                                + eventDetail.getString("event_image"));
                                    } else {
                                        eventModel.setEventImage("");
                                    }

                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eventDetail.getString("rate")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }

                                    JSONArray hostArray = eachEvent.getJSONArray("host_name");
                                    UserModel[] hostUsers = new UserModel[hostArray.length()];
                                    for (int j = 0; j < hostArray.length(); j++) {
                                        JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                        UserModel hostedBy = new UserModel();
                                        hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                        hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                        if (!eachHostInfo.getString("host_image").equals("")) {
                                            hostedBy.setImgUrl(res.getString("profileimage_default_url")
                                                    + eachHostInfo.getString("host_image"));
                                        } else {
                                            hostedBy.setImgUrl("");
                                        }
                                        hostUsers[j] = hostedBy;
                                    }
                                    eventModel.setHostedBy(hostUsers);
                                    eventModel.setEventCatId(eventDetail.getString("event_category_id"));

                                    alertEventList.add(eventModel);



                                }
                                if (alertEventList.size() > 1) {
                                    alertEventList();
                                }else{
                                    EventModel eventModel1 = eventList.get((int) marker.getTag());
                                    Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
                                    intent.putExtra("eventmodel", eventModel1);
                                    intent.putExtra("position", getEventPosition(eventModel1));
                                    Logger.e("=============", "============");
                                    (getActivity()).startActivityForResult(intent, EVENT_NEAR_ME);
                                }

                            } else {

                            }
                        } catch (JSONException e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }

                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                            AlertUtils.showToast(getContext(), errMsg);
                        } catch (Exception e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                            AlertUtils.showToast(getContext(), errMsg);
                        }
                    }
                }, "userGetSameLocationEvents");
    }

    public void alertEventList() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.alert_event_lisiting, null, false);
        dialog.setView(view);
        RecyclerView eventLisiting = view.findViewById(R.id.alert_event_listing);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        eventLisiting.setLayoutManager(linearLayoutManager);
        eventAdapter = new EventAdapter(alertEventList, getContext());
        if (eventModel != null) {
            if (!TextUtils.isEmpty(eventModel.getEventId())) {
                eventAdapter.mergeRequesterEvent(eventModel.getEventId());
            }
        }
        eventLisiting.setAdapter(eventAdapter);

        this.dialog = dialog.show();
    }

    private int getEventPosition(EventModel eventModel1) {
        for (int i = 0; i < eventList.size(); i++) {
            if (eventModel1.getEventId().equals(eventList.get(i).getEventId())) {
                return i;
            }
        }
        return -1;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public void onStop() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("getNearByEventsTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("searchNearbyEventTask");
        super.onStop();
    }

    @Override
    public void onDetach() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("getNearByEventsTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("searchNearbyEventTask");
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("getNearByEventsTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("searchNearbyEventTask");
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EVENT_NEAR_ME) {
                int position = data.getIntExtra("position", -1);
                if (data.getBooleanExtra("isDeleted", false)) {
                    eventList.remove(position);
                    if (mMap != null) {
                        mMap.clear();
                        markerList.clear();
                        for (int i = 0; i < eventList.size(); i++) {
                            showEventMarker(i, false);
                        }
                    }
                }

                if (position >= 0) {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    try {
                        eventList.set(position, eventModel);
                    } catch (Exception e) {
                        Logger.e("eventList ex", e.getMessage());
                    }
                }
            } else if (requestCode == MainActivity.OPEN_FEED) {
                if (data != null) {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    if (eventModel != null) {
                        for (int i = 0; i < eventList.size(); i++) {
                            if (eventList.get(i).getEventId().equals(eventModel.getEventId())) {
                                eventList.set(i, eventModel);
                                break;
                            }
                        }
                    }
                }
            } else if (requestCode == MainActivity.INTENT_ADD_EVENT) {
                boolean isAdd = data.getBooleanExtra("isAdd", false);
                if (isAdd) {
                    getNearByEventsTask();
                } else {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventmodel");
                    if (eventModel != null) {
                        for (int i = 0; i < eventList.size(); i++) {
                            if (eventList.get(i).getEventId().equals(eventModel.getEventId())) {
                                eventList.set(i, eventModel);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
