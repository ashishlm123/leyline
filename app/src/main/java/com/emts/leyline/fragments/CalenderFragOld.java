package com.emts.leyline.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.CalEventAdapter;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.CalendarEventModel;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.UserModel;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class CalenderFragOld extends Fragment {
    ArrayList<CalendarEventModel> events;

    RecyclerView dragView;
    CalEventAdapter adapter;
    TextView tvErrorEvent;
    ProgressBar progressEvent;

    SlidingUpPanelLayout slidingUpPanelLayout;
    MaterialCalendarView calendarView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar_old, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        events = new ArrayList<>();

        tvErrorEvent = view.findViewById(R.id.error_tv);
        progressEvent = view.findViewById(R.id.progress_event);
        dragView = view.findViewById(R.id.drag_view);
        dragView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new CalEventAdapter(getActivity(), events);
        dragView.setAdapter(adapter);

        slidingUpPanelLayout = view.findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setDragView(dragView);
//        slidingUpPanelLayout.setScrollableViewHelper(new NestedScrollableViewHelper());
        slidingUpPanelLayout.setScrollableView(dragView);
        slidingUpPanelLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        // Setup calendar pages
        calendarView = view.findViewById(R.id.calendarView);
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                if (selected) {
                    getCalendarEventTask(date.getYear() + "-" + String.format("%02d", date.getMonth() + 1)
                            + "-" + String.format("%02d", date.getDay()));
                }
            }
        });

        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                getCalendarEventTask(date.getYear() + "-" + String.format("%02d", date.getMonth() + 1));
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getCalendarEventTask(calendarView.getCurrentDate().getYear() + "-" + String.format("%02d", calendarView.getCurrentDate().getMonth() + 1));
            tvErrorEvent.setVisibility(View.GONE);
        } else {
            tvErrorEvent.setText(R.string.no_internet);
            tvErrorEvent.setVisibility(View.VISIBLE);
        }

    }

    private void getCalendarEventTask(final String date) {
        events.clear();
        adapter.notifyDataSetChanged();
        progressEvent.setVisibility(View.VISIBLE);
        tvErrorEvent.setVisibility(View.GONE);
        dragView.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("date", date);
        postMap.put("user_id", PreferenceHelper.getInstance(getActivity()).getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getEventViaDate, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressEvent.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);

                            if (res.getBoolean("status")) {
                                JSONArray infoArray = res.getJSONArray("info");

                                if (infoArray.length() < 0) {
                                    tvErrorEvent.setText(res.getString("message"));
                                    tvErrorEvent.setVisibility(View.VISIBLE);
                                    dragView.setVisibility(View.GONE);
                                } else {

                                    for (int i = 0; i < infoArray.length(); i++) {
                                        JSONArray eventArray = infoArray.getJSONArray(i);
                                        CalendarEventModel eventList = new CalendarEventModel();
                                        ArrayList<EventModel> eventDateList = new ArrayList<>();

                                        for (int j = 0; j < eventArray.length(); j++) {
                                            JSONObject eachObj = eventArray.getJSONObject(j);

                                            EventModel eventModel = new EventModel();
                                            JSONObject creatorObj = eachObj.getJSONObject("created_by");
                                            UserModel[] hostUsers = new UserModel[1];
                                            UserModel createdBy = new UserModel();
                                            createdBy.setUserId(creatorObj.getString("id"));
                                            createdBy.setUserName(creatorObj.getString("name"));
                                            String hostImage = creatorObj.getString("image");
                                            if (!TextUtils.isEmpty(hostImage) && !hostImage.equalsIgnoreCase("null")) {
                                                createdBy.setImgUrl(res.getString("profileimage_default_url") + hostImage);
                                            } else {
                                                createdBy.setImgUrl("");
                                            }
                                            hostUsers[0] = createdBy;
                                            eventModel.setHostedBy(hostUsers);

                                            eventModel.setEventName(eachObj.getString("event_name"));
                                            eventModel.setEventImage(res.getString("event_image_default_url") + eachObj.getString("event_image"));
                                            eventModel.setEventAttendStatus(eachObj.getString("attendence"));

                                            eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                            eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));

                                            eventModel.setEventDesc(eachObj.getString("description"));
                                            eventModel.setEventDate(eachObj.getString("event_date"));
                                            eventModel.setEventLocation(eachObj.getString("location"));
                                            eventModel.setEventId(eachObj.getString("event_id"));
                                            eventModel.setStatus(eachObj.getString("status"));

                                            try {
                                                eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                            } catch (Exception e) {
                                                eventModel.setEventRating(0);
                                            }
                                            eventModel.setEventCatId(eachObj.getString("event_category_id"));
                                            eventModel.setEventTime(eachObj.getString("event_time"));

                                            JSONObject likedBy = eachObj.getJSONObject("like");
                                            eventModel.setLiked(likedBy.getBoolean("your_like_status"));

                                            eventDateList.add(eventModel);
                                            eventList.setEventList(eventDateList);
                                        }
                                        events.add(eventList);
                                    }
                                    adapter.notifyDataSetChanged();
                                    tvErrorEvent.setVisibility(View.GONE);
                                    dragView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                tvErrorEvent.setText(res.getString("message"));
                                tvErrorEvent.setVisibility(View.VISIBLE);
                                dragView.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            Logger.e("getCalendarEventTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressEvent.setVisibility(View.GONE);
                        String errorMsg = "Unexpected error !!! Please check internet access and try again latter.";
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                        }
                        tvErrorEvent.setText(errorMsg);
                        dragView.setVisibility(View.GONE);
                        tvErrorEvent.setVisibility(View.VISIBLE);
                    }
                }, "getCalendarEventTask");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CalEventAdapter.CAL_EVENT) {
                int position = data.getIntExtra("position", -1);
                int calenderPosition = data.getIntExtra("calenderPosition", -1);
                if (position >= 0) {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    if (eventModel != null) {
                        try {
                            events.get(calenderPosition).getEventList().set(position, eventModel);
                        } catch (Exception e) {
                            Logger.e("calonresultAct ex", e.getMessage());
                        }
                    }
                }
            }
        }
    }
}
