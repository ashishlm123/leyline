package com.emts.leyline.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.EventAdapter;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class EventDetailsFragment extends Fragment {
    RecyclerView createdEventListing, attendingEventListing, attendedEventListing;
    EventAdapter createdEventAdapter, attendingEventAdapter, attendedEventAdapter;
    ArrayList<EventModel> createdEventList, attendingEventList, attendedEventList;
    PreferenceHelper helper;
    ProgressBar progressCreatedEvents, progressAttendingEvents, progressAttendedEvents;
    TextView errCreatedEvent, errAttendingEvent, errAttendedEvent;
    String profileId = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            profileId = bundle.getString("prof_id");
        }
        return inflater.inflate(R.layout.fragment_event_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        createdEventList = new ArrayList<>();
        attendingEventList = new ArrayList<>();
        attendedEventList = new ArrayList<>();

        progressCreatedEvents = view.findViewById(R.id.created_events_progress);
        progressAttendingEvents = view.findViewById(R.id.attending_events_progress);
        progressAttendedEvents = view.findViewById(R.id.attended_events_progress);

        errCreatedEvent = view.findViewById(R.id.err_created_events_text);
        errAttendingEvent = view.findViewById(R.id.err_attending_events_text);
        errAttendedEvent = view.findViewById(R.id.err_attended_events_text);

        createdEventAdapter = new EventAdapter(createdEventList, getActivity());
        createdEventAdapter.setEventAs(EventAdapter.CREATED_EVENTS);
        createdEventListing = view.findViewById(R.id.created_events_list);
        createdEventListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        createdEventListing.setNestedScrollingEnabled(false);
        createdEventListing.setAdapter(createdEventAdapter);

        attendingEventAdapter = new EventAdapter(attendingEventList, getActivity());
        attendingEventAdapter.setEventAs(EventAdapter.ATTENDING_EVENTS);
        attendingEventListing = view.findViewById(R.id.attending_events_list);
        attendingEventListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        attendingEventListing.setNestedScrollingEnabled(false);
        attendingEventListing.setAdapter(attendingEventAdapter);

        attendedEventAdapter = new EventAdapter(attendedEventList, getActivity());
        attendedEventAdapter.setEventAs(EventAdapter.ATTENDED_EVENTS);
        attendedEventListing = view.findViewById(R.id.attended_events_list);
        attendedEventListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        attendedEventListing.setNestedScrollingEnabled(false);
        attendedEventListing.setAdapter(attendedEventAdapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            userUserEventDetailsTask();
        } else {
            errAttendedEvent.setText(R.string.no_internet);
            errAttendedEvent.setVisibility(View.VISIBLE);
            errAttendingEvent.setText(R.string.no_internet);
            errAttendingEvent.setVisibility(View.VISIBLE);
            errCreatedEvent.setText(R.string.no_internet);
            errCreatedEvent.setVisibility(View.VISIBLE);
        }
    }

    private void userUserEventDetailsTask() {
        progressCreatedEvents.setVisibility(View.VISIBLE);
        progressAttendingEvents.setVisibility(View.VISIBLE);
        progressAttendedEvents.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("profile_id", profileId);

        vHelper.addVolleyRequestListeners(Api.getInstance().userEventDetailsListing, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        attendedEventList.clear();
                        createdEventList.clear();
                        attendingEventList.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject userEventList = res.getJSONObject("user_events_list");

                                JSONArray createdEventListArray = userEventList.getJSONArray("created_events");
                                if (createdEventListArray.length() == 0) {
                                    Logger.e("attending_length", String.valueOf(createdEventListArray.length()));
                                    errCreatedEvent.setVisibility(View.VISIBLE);
                                    errCreatedEvent.setText(R.string.no_created_events);
                                    createdEventListing.setVisibility(View.GONE);
                                } else {
                                    for (int i = 0; i < createdEventListArray.length(); i++) {
                                        JSONObject eachObj = createdEventListArray.getJSONObject(i);
                                        EventModel eventModel = new EventModel();
                                        eventModel.setEventId(eachObj.getString("event_id"));
                                        eventModel.setEventName(eachObj.getString("event_name"));
                                        eventModel.setEventTime(eachObj.getString("event_time"));
                                        eventModel.setEventDate(eachObj.getString("event_date"));
                                        eventModel.setEventDesc(eachObj.getString("description"));
                                        eventModel.setEventLocation(eachObj.getString("location"));
                                        eventModel.setStatus(eachObj.getString("status"));
                                        try {
                                            eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                        } catch (Exception e) {
                                        }
                                        try {
                                            eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));
                                        } catch (Exception e) {
                                        }


                                        if (!TextUtils.isEmpty(eachObj.optString("attendance"))) {
                                            eventModel.setEventAttendStatus(eachObj.optString("attendance"));
                                        } else {
                                            eventModel.setEventAttendStatus(EventModel.EVENT_ATTENDING);
                                        }
                                        if (!eachObj.getString("image_name").equals("")) {
                                            eventModel.setEventImage(res.getString("event_image_url") + eachObj.getString("image_name"));
                                        } else {
                                            eventModel.setEventImage("");
                                        }

                                        try {
                                            eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                        } catch (Exception e) {
                                            eventModel.setEventRating(0);
                                        }
                                        JSONObject likeObj = eachObj.getJSONObject("like");
                                        eventModel.setLiked(likeObj.getBoolean("your_like_status"));

                                        JSONArray hostArray = eachObj.getJSONArray("host_name");
                                        UserModel[] hostUsers = new UserModel[hostArray.length()];
                                        for (int j = 0; j < hostArray.length(); j++) {
                                            JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                            UserModel hostedBy = new UserModel();
                                            hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                            hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                            if (!eachHostInfo.getString("host_image").equals("")) {
                                                hostedBy.setImgUrl(res.getString("profile_img_Url") + eachHostInfo.getString("host_image"));
                                            } else {
                                                hostedBy.setImgUrl("");
                                            }
                                            hostUsers[j] = hostedBy;
                                        }
                                        eventModel.setHostedBy(hostUsers);

                                        eventModel.setEventCatId(eachObj.getString("event_category_id"));
                                        createdEventList.add(eventModel);
                                    }
                                    createdEventListing.setVisibility(View.VISIBLE);
                                    createdEventAdapter.notifyDataSetChanged();
                                }

                                JSONArray attendingEventListArray = userEventList.getJSONArray("attending_events");
                                if (attendingEventListArray.length() == 0) {
                                    Logger.e("attending_length", String.valueOf(attendingEventListArray.length()));
                                    errAttendingEvent.setVisibility(View.VISIBLE);
                                    errAttendingEvent.setText(R.string.no_attending_events);
                                    attendingEventListing.setVisibility(View.GONE);
                                } else {
                                    for (int j = 0; j < attendingEventListArray.length(); j++) {
                                        JSONObject eachObj = attendingEventListArray.getJSONObject(j);
                                        EventModel eventModel = new EventModel();
                                        eventModel.setEventName(eachObj.getString("event_name"));
//                                        eventModel.setCreatedDate(eachObj.getString("created_date"));
                                        eventModel.setEventTime(eachObj.getString("event_time"));
                                        eventModel.setEventDate(eachObj.getString("event_date"));
                                        eventModel.setStatus(eachObj.getString("status"));
                                        eventModel.setEventId(eachObj.getString("event_id"));
                                        eventModel.setEventAttendStatus(EventModel.EVENT_ATTENDING);
                                        try {
                                            eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                        } catch (Exception e) {
                                        }
                                        try {
                                            eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));
                                        } catch (Exception e) {
                                        }

                                        if (!eachObj.optString("image_name").equals("")) {
                                            eventModel.setEventImage(res.getString("event_image_url") + eachObj.getString("image_name"));
                                        } else {
                                            eventModel.setEventImage("");
                                        }

                                        eventModel.setEventDesc(eachObj.getString("description"));
                                        eventModel.setEventLocation(eachObj.getString("location"));
                                        try {
                                            eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                        } catch (Exception e) {
                                            eventModel.setEventRating(0);
                                        }
                                        JSONArray hostArray = eachObj.getJSONArray("host_name");
                                        UserModel[] hostUsers = new UserModel[hostArray.length()];
                                        for (int k = 0; k < hostArray.length(); k++) {
                                            JSONObject eachHostInfo = hostArray.getJSONObject(k);
                                            UserModel hostedBy = new UserModel();
                                            hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                            hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                            if (!eachHostInfo.getString("host_image").equals("")) {
                                                hostedBy.setImgUrl(res.getString("profile_img_Url") + eachHostInfo.getString("host_image"));
                                            } else {
                                                hostedBy.setImgUrl("");
                                            }
                                            hostUsers[k] = hostedBy;
                                        }
                                        eventModel.setHostedBy(hostUsers);

                                        JSONObject likeObj = eachObj.getJSONObject("like");
                                        eventModel.setLiked(likeObj.getBoolean("your_like_status"));

                                        eventModel.setEventCatId(eachObj.getString("event_category_id"));

                                        attendingEventList.add(eventModel);
                                    }
                                    attendingEventListing.setVisibility(View.VISIBLE);
                                    attendingEventAdapter.notifyDataSetChanged();
                                }

                                JSONArray attendedEventListArray = userEventList.getJSONArray("attended_events");
                                if (attendedEventListArray.length() == 0) {
                                    Logger.e("attended_length", String.valueOf(attendedEventListArray.length()));
                                    errAttendedEvent.setVisibility(View.VISIBLE);
                                    errAttendedEvent.setText(R.string.no_attended_events);
                                    attendedEventListing.setVisibility(View.GONE);
                                } else {
                                    for (int k = 0; k < attendedEventListArray.length(); k++) {
                                        JSONObject eachObj = attendedEventListArray.getJSONObject(k);
                                        EventModel eventModel = new EventModel();
                                        eventModel.setEventName(eachObj.getString("event_name"));
                                        eventModel.setEventTime(eachObj.getString("event_time"));
                                        eventModel.setEventDate(eachObj.getString("event_date"));
                                        eventModel.setStatus(eachObj.getString("status"));
                                        eventModel.setEventId(eachObj.getString("event_id"));
                                        eventModel.setEventAttendStatus(EventModel.EVENT_ATTENDED);
                                        try {
                                            eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                        } catch (Exception e) {
                                        }
                                        try {
                                            eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));
                                        } catch (Exception e) {
                                        }

                                        if (!eachObj.optString("image_name").equals("")) {
                                            eventModel.setEventImage(res.getString("event_image_url") + eachObj.getString("image_name"));
                                        } else {
                                            eventModel.setEventImage("");
                                        }

                                        eventModel.setEventDesc(eachObj.getString("description"));
                                        eventModel.setEventLocation(eachObj.getString("location"));
                                        try {
                                            eventModel.setEventRating(Float.parseFloat(eachObj.getString("rate")));
                                        } catch (Exception e) {
                                            eventModel.setEventRating(0);
                                        }
                                        JSONArray hostArray = eachObj.getJSONArray("host_name");
                                        UserModel[] hostUsers = new UserModel[hostArray.length()];
                                        for (int j = 0; j < hostArray.length(); j++) {
                                            JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                            UserModel hostedBy = new UserModel();
                                            hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                            hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                            if (!eachHostInfo.getString("host_image").equals("")) {
                                                hostedBy.setImgUrl(res.getString("profile_img_Url") + eachHostInfo.getString("host_image"));
                                            } else {
                                                hostedBy.setImgUrl("");
                                            }
                                            hostUsers[j] = hostedBy;
                                        }
                                        eventModel.setHostedBy(hostUsers);

                                        JSONObject likeObj = eachObj.getJSONObject("like");
                                        eventModel.setLiked(likeObj.getBoolean("your_like_status"));

                                        eventModel.setEventCatId(eachObj.getString("event_category_id"));
                                        attendedEventList.add(eventModel);
                                    }

                                    attendedEventListing.setVisibility(View.VISIBLE);
                                    attendedEventAdapter.notifyDataSetChanged();
                                }
                            } else {
                                errCreatedEvent.setVisibility(View.VISIBLE);
                                errAttendingEvent.setVisibility(View.VISIBLE);
                                errAttendedEvent.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            Logger.e("userUserEventDetailsTask json ex", e.getMessage() + "");
                        }
                        progressAttendedEvents.setVisibility(View.GONE);
                        progressCreatedEvents.setVisibility(View.GONE);
                        progressAttendingEvents.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressAttendedEvents.setVisibility(View.GONE);
                        progressCreatedEvents.setVisibility(View.GONE);
                        progressAttendingEvents.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userUserEventDetailsTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(getActivity(), "Error !!!", errMsg);
                    }
                }, "userUserEventDetailsTask");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == EventAdapter.CREATED_EVENTS) {
                int createPosition = data.getIntExtra("createdPosition", -1);
                if (createPosition >= 0) {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    if (eventModel != null) {
                        try {
                            createdEventList.set(createPosition, eventModel);
                            for (int i = 0; i < attendingEventList.size(); i++) {
                                if (createdEventList.get(i).getEventId().equals(eventModel.getEventId())) {
                                    attendingEventList.set(i, eventModel);
                                }
                            }
                            for (int i = 0; i < attendedEventList.size(); i++) {
                                if (createdEventList.get(i).getEventId().equals(eventModel.getEventId())) {
                                    attendedEventList.set(i, eventModel);
                                }
                            }
                        } catch (Exception e) {
                            Logger.e("resultAct ex", e.getMessage());
                        }
                    }
                }
            } else if (requestCode == EventAdapter.ATTENDING_EVENTS) {
                int attendingPosition = data.getIntExtra("attendingPosition", -1);
                if (attendingPosition >= 0) {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    if (eventModel != null) {
                        try {
                            attendingEventList.set(attendingPosition, eventModel);
                            for (int i = 0; i < createdEventList.size(); i++) {
                                if (createdEventList.get(i).getEventId().equals(eventModel.getEventId())) {
                                    createdEventList.set(i, eventModel);
                                }
                            }
                        } catch (Exception e) {
                            Logger.e("onResulAct ex", e.getMessage());
                        }
                    }
                }
            } else if (requestCode == EventAdapter.ATTENDED_EVENTS) {
                int attendedPosition = data.getIntExtra("attendedPosition", -1);
                if (attendedPosition >= 0) {
                    EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                    if (eventModel != null) {
                        try {
                            attendedEventList.set(attendedPosition, eventModel);
                            for (int i = 0; i < createdEventList.size(); i++) {
                                if (createdEventList.get(i).getEventId().equals(eventModel.getEventId())) {
                                    createdEventList.set(i, eventModel);
                                }
                            }
                        } catch (Exception e) {
                            Logger.e("onResulAct ex", e.getMessage());
                        }
                    }
                }
            }
        }
    }
}
