package com.emts.leyline.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.activity.SearchActivity;
import com.emts.leyline.adapter.FriendSuggestionAdapter;
import com.emts.leyline.adapter.FriendsAdapter;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.FriendsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FriendsFragment extends Fragment {
    RecyclerView friendsListing, suggestionListView;
    RecyclerView pendingFriendsListing;
    FriendsAdapter friendsAdapter, pendingFriendsAdapter;
    FriendSuggestionAdapter suggestionAdapter;
    ArrayList<FriendsModel> friendsList, pendingFriendList, suggestionList;
    PreferenceHelper helper;
    ProgressBar pendingProgress, friendsProgress, suggestionProgress;
    TextView pendingError, friendsError, suggestionError;
    String profileId = "";
    RelativeLayout holderPending, holderSuggestion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            profileId = bundle.getString("prof_id");
        }
        return inflater.inflate(R.layout.fragment_friends, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        LinearLayout searchBar = view.findViewById(R.id.search_bar);
        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });

        holderPending = view.findViewById(R.id.holder_pending);
        TextView yourFriendLevel = view.findViewById(R.id.your_friends_text);
        if (!profileId.equals(helper.getUserId())) {
            holderPending.setVisibility(View.GONE);
            yourFriendLevel.setText("Friends");
        } else {
            yourFriendLevel.setText("Your Friends");
        }
        pendingProgress = view.findViewById(R.id.pending_request_progress);
        pendingError = view.findViewById(R.id.pending_friends_error);

        pendingFriendList = new ArrayList<>();

        pendingFriendsListing = view.findViewById(R.id.pending_request_list);
        pendingFriendsListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        pendingFriendsListing.setNestedScrollingEnabled(false);
        pendingFriendsAdapter = new FriendsAdapter(pendingFriendList, getActivity());
        pendingFriendsAdapter.setIsMyProfile(profileId.equals(helper.getUserId()));
        pendingFriendsAdapter.setUpdateFriendListListener(listener);
        pendingFriendsListing.setAdapter(pendingFriendsAdapter);

        friendsProgress = view.findViewById(R.id.your_friends_progress);
        friendsError = view.findViewById(R.id.your_friends_error);

        friendsList = new ArrayList<>();

        friendsListing = view.findViewById(R.id.friend_list);
        friendsListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        friendsListing.setNestedScrollingEnabled(false);
        friendsAdapter = new FriendsAdapter(friendsList, getActivity());
        friendsAdapter.setIsMyProfile(profileId.equals(helper.getUserId()));
        friendsAdapter.setUpdateFriendListListener(listener);
        friendsListing.setAdapter(friendsAdapter);


        //frn suggestion
        suggestionList = new ArrayList<>();

        holderSuggestion = view.findViewById(R.id.holder_friend_suggestion);
        suggestionProgress = view.findViewById(R.id.frn_sugg_progress);
        suggestionError = view.findViewById(R.id.frn_sugg_error);
        suggestionListView = view.findViewById(R.id.list_frn_suggestion);
        suggestionListView.setItemAnimator(null);
        suggestionListView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        suggestionListView.setNestedScrollingEnabled(false);
        suggestionAdapter = new FriendSuggestionAdapter(getActivity(), suggestionList);
        suggestionListView.setAdapter(suggestionAdapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            userFriendsListingTask();
        } else {
            pendingError.setText(R.string.no_internet);
            pendingError.setVisibility(View.VISIBLE);
            friendsError.setVisibility(View.VISIBLE);
            friendsError.setText(R.string.no_internet);
        }
    }

    FriendsAdapter.UpdateFriendListListener listener = new FriendsAdapter.UpdateFriendListListener() {
        @Override
        public void updateFriendList() {
            if (NetworkUtils.isInNetwork(getActivity())) {
                userFriendsListingTask();
            }
        }
    };

    public void userFriendsListingTask() {
        pendingProgress.setVisibility(View.VISIBLE);
        pendingError.setVisibility(View.GONE);
        pendingFriendsListing.setVisibility(View.GONE);

        friendsProgress.setVisibility(View.VISIBLE);
        friendsError.setVisibility(View.GONE);
        friendsListing.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("profile_id", profileId);

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserFriendlists, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pendingProgress.setVisibility(View.GONE);
                        friendsProgress.setVisibility(View.GONE);
                        friendsList.clear();
                        pendingFriendList.clear();

                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray friendsArray = res.getJSONArray("friend_list");
                                for (int i = 0; i < friendsArray.length(); i++) {
                                    JSONObject jsonObject = friendsArray.getJSONObject(i);
//                                    if (jsonObject.getString("pending_friend_id").equalsIgnoreCase("null")
//                                            || jsonObject.getString("pending_friend_id").equals("0")) {

                                    if (jsonObject.getString("friend_status").equalsIgnoreCase("isfriend")) {

//                                        if (jsonObject.getString("friend_status").equals(FriendsModel.RECEIVE)) {
                                        FriendsModel friendsModel = new FriendsModel();
                                        friendsModel.setFriends(true);
                                        friendsModel.setUserName(jsonObject.getString("name"));
                                        friendsModel.setUserId(jsonObject.getString("friend_id"));
                                        String threadId = jsonObject.getString("thread_id");
                                        if (threadId.equals("") || threadId.equals("null") || threadId.equals("0")) {
                                            friendsModel.setThreadId("0");
                                        } else {
                                            friendsModel.setThreadId(threadId);
                                        }
                                        friendsList.add(friendsModel);
                                    } else {
                                        FriendsModel friendsModel = new FriendsModel();
                                        friendsModel.setFriends(false);
                                        friendsModel.setUserName(jsonObject.getString("name"));
                                        friendsModel.setUserId(jsonObject.getString("pending_friend_id"));
                                        friendsModel.setRequestStatus(jsonObject.optString("friend_status"));
                                        pendingFriendList.add(friendsModel);
                                    }
                                }
                                pendingFriendsAdapter.notifyDataSetChanged();
                                friendsAdapter.notifyDataSetChanged();

                                if (friendsList.size() > 0) {
                                    friendsListing.setVisibility(View.VISIBLE);
                                    friendsError.setVisibility(View.GONE);
                                } else {
                                    friendsError.setText("No Friends");
                                    friendsError.setVisibility(View.VISIBLE);
                                    friendsListing.setVisibility(View.GONE);
                                }

                                if (pendingFriendList.size() > 0) {
                                    pendingFriendsListing.setVisibility(View.VISIBLE);
                                    pendingError.setVisibility(View.GONE);
                                } else {
                                    pendingError.setText("No pending requests");
                                    pendingError.setVisibility(View.VISIBLE);
                                    pendingFriendsListing.setVisibility(View.GONE);
                                }
                            } else {
                                friendsError.setText("No Friends");
                                pendingError.setText("No pending requests");
                                friendsError.setVisibility(View.VISIBLE);
                                pendingError.setVisibility(View.VISIBLE);

                                friendsListing.setVisibility(View.GONE);
                                pendingFriendsListing.setVisibility(View.GONE);
                            }
                            //if my profile show suggestion
                            if (profileId.equals(helper.getUserId())) {
                                friendSuggestionTask();
                            }
                        } catch (JSONException e) {
                            Logger.e("userFriendsListingTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pendingProgress.setVisibility(View.GONE);
                        friendsProgress.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userFriendsListingTask error ex", e.getMessage() + "");
                        }
                        friendsError.setText(errMsg);
                        pendingError.setText(errMsg);
                        friendsError.setVisibility(View.VISIBLE);
                        pendingError.setVisibility(View.VISIBLE);

                        friendsListing.setVisibility(View.GONE);
                        pendingFriendsListing.setVisibility(View.GONE);
                    }
                }, "userFriendsListingTask");
    }

    public void friendSuggestionTask() {
        suggestionProgress.setVisibility(View.VISIBLE);
        suggestionError.setVisibility(View.GONE);
        suggestionListView.setVisibility(View.GONE);
        holderSuggestion.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", profileId);
        postMap.put("limit", "50");
        postMap.put("offset", "0");

        vHelper.addVolleyRequestListeners(Api.getInstance().suggestUser, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        suggestionProgress.setVisibility(View.GONE);
                        suggestionList.clear();

                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray friendsArray = res.getJSONArray("suggested_people");
                                for (int i = 0; i < friendsArray.length(); i++) {
                                    JSONObject jsonObject = friendsArray.getJSONObject(i);

                                    FriendsModel friendsModel = new FriendsModel();
                                    friendsModel.setUserName(jsonObject.getString("name"));
                                    friendsModel.setUserId(jsonObject.getString("id"));
                                    friendsModel.seteMail(jsonObject.getString("email"));
                                    String imageUrl = jsonObject.getString("image");
                                    if (!TextUtils.isEmpty(imageUrl) && !imageUrl.equalsIgnoreCase("null")) {
                                        friendsModel.setImgUrl(res.getString("profileimage_default_url") + imageUrl);
                                    } else {
                                        friendsModel.setImgUrl("");
                                    }
                                    friendsModel.setRequestStatus(FriendsModel.NONE);

                                    suggestionList.add(friendsModel);

                                }
                                suggestionAdapter.notifyDataSetChanged();

                                if (suggestionList.size() > 0) {
                                    suggestionListView.setVisibility(View.VISIBLE);
                                    suggestionError.setVisibility(View.GONE);
                                    holderSuggestion.setVisibility(View.VISIBLE);
                                } else {
                                    suggestionListView.setVisibility(View.GONE);
                                    suggestionError.setVisibility(View.GONE);
                                    holderSuggestion.setVisibility(View.GONE);
                                }
                            } else {
                                suggestionListView.setVisibility(View.GONE);
                                suggestionError.setVisibility(View.GONE);
                                holderSuggestion.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("friendSuggestionTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("friendSuggestionTask error ex", e.getMessage() + "");
                        }
                        suggestionListView.setVisibility(View.GONE);
                        suggestionError.setVisibility(View.GONE);
                        holderSuggestion.setVisibility(View.GONE);
                    }
                }, "friendSuggestionTask");
    }
}
