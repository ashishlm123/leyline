package com.emts.leyline.helper;//package com.emts.bidforsin.helper;
//
//import android.content.Context;
//import android.util.Base64;
//import android.widget.ImageView;
//
//import com.jakewharton.picasso.OkHttp3Downloader;
//import com.squareup.picasso.OkHttpDownloader;
//import com.squareup.picasso.Picasso;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
//import okhttp3.Interceptor;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.Response;
//
///**
// * Created by User on 2017-01-12.
// */
//
//public class PicassoHelper {
//
//    public static void loadImage(Context context, String imageUrl, ImageView imageView) {
//        OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.interceptors().add(new BasicAuthInterceptor());
//
//        Picasso picasso = new Picasso.Builder(context).downloader(new OkHttp3Downloader(okHttpClient)).build();
//        picasso.load(imageUrl).fit().into(imageView);
//
//    }
//
//    private static class BasicAuthInterceptor implements Interceptor {
//
//        @Override
//        public Response intercept(Interceptor.Chain chain) throws IOException {
//            final Request original = chain.request();
//            String creds = String.format("%s:%s", "bfs", "KytQ4VJACgh5");
//            String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
//            final Request.Builder requestBuilder = original.newBuilder()
//                    .header("Authorization", auth)
//                    .method(original.method(), original.body());
//            return chain.proceed(requestBuilder.build());
//        }
//    }
//
//}
