package com.emts.leyline.helper;

import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.emts.leyline.activity.LoginActivity;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by User on 2017-01-06.
 */

public class VolleyHelper {
    public static final int STATUS_SITE_OFFLINE = 503;
    //    public static final int STATUS_AUTH_ERROR = 407;//Proxy Authentication Required
    public static final int STATUS_SESSION_OUT_ERROR = 407;//Proxy Authentication Required
    public static final int STATUS_AUTH_ERROR = 401;// Unauthorized
    public static final int STATUS_FORBIDDEN_ACCESS = 403;

    private static RequestQueue requestQueue;
    private static VolleyHelper volleyHelper;

    private Context context;

    private VolleyHelper(Context context) {
        this.context = context;
    }

    public static VolleyHelper getInstance(Context context) {
        if (volleyHelper != null) {
            return volleyHelper;
        } else {
            requestQueue = Volley.newRequestQueue(context);
            volleyHelper = new VolleyHelper(context);
            return volleyHelper;
        }

    }

    public void addVolleyRequestListeners(String url, int reqMethod, HashMap<String, String> postParams,
                                          VolleyHelperInterface callbacks, String requestTag) {
        addRequestToQueue(url, reqMethod, postParams, callbacks, requestTag);
    }


    public interface VolleyHelperInterface {
        void onSuccess(String response);

        void onError(String errorResponse, VolleyError volleyError);
    }

    private void addRequestToQueue(String url, final int reqMethod, final HashMap<String, String> postParams,
                                   final VolleyHelperInterface callbacks, final String requestTag) {
        Logger.e(requestTag + " url", url);

        StringRequest request = new StringRequest(reqMethod, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e(requestTag + " response", response);
                if (response.toLowerCase().contains("Session Expired".toLowerCase())) {
                    PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(context);
                    preferenceHelper.edit().clear().commit();
                    cancelAllRequests();

                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                    AlertUtils.showToast(context.getApplicationContext(), "Session Expired");
                } else {
                    if (callbacks != null) {
                        callbacks.onSuccess(response);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.e(requestTag + " error", error.getMessage() + "");
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    Logger.e(requestTag + " error res", errorObj.toString());
                    if (callbacks != null) {
                        callbacks.onError(errorObj.toString(), error);
                        return;
                    }
                } catch (Exception e) {
                    Logger.e(requestTag + " error ex", e.getMessage() + "");
                }
                if (callbacks != null) {
                    callbacks.onError("", error);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (reqMethod == Method.POST && postParams != null) {

                    Logger.e(requestTag + " postParams", postParams.toString());
                    return postParams;
                }
                return super.getParams();
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<>();
//                String creds = String.format("%s:%s", "bfs", "KytQ4VJACgh5");
//                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
//                params.put("Authorization", auth);
//                return params;
//            }
        };
        RetryPolicy retryPolicy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        request.setTag(requestTag);

        requestQueue.add(request);
    }

    public void cancelRequest(String tag) {
        requestQueue.cancelAll(tag);
    }

    public HashMap<String, String> getPostParams() {
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("api_key", Api.getInstance().apiKey);
        PreferenceHelper prefHelper = PreferenceHelper.getInstance(context);
        if (prefHelper.isLogin()) {
            postParams.put("user_id", prefHelper.getUserId());
            postParams.put("token", prefHelper.getToken());
        }
        return postParams;
    }

    public void addMultipartRequest(String url, final int reqMethod, final HashMap<String, String> formPart,
                                    final HashMap<String, String> filePart,
                                    final VolleyHelperInterface callbacks, final String requestTag) {
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(reqMethod, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse res) {
                String response = new String(res.data);
                Logger.e(requestTag + " response", response);
                if (callbacks != null) {
                    callbacks.onSuccess(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.e(requestTag + " error", error.getMessage() + "");
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    Logger.e(requestTag + " error res", errorObj.toString());
                    if (callbacks != null) {
                        callbacks.onError(errorObj.toString(), error);
                        return;
                    }
                } catch (Exception e) {
                    Logger.e("siteSettingTask error ex", e.getMessage() + "");
                }
                if (callbacks != null) {
                    callbacks.onError("", error);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                if (reqMethod == Method.POST && formPart != null) {
//                    if (PreferenceHelper.getInstance(context).isLogin()) {
//                        formPart.put("user_id", String.valueOf(PreferenceHelper.getInstance(context).getUserId()));
//                        formPart.put("token", String.valueOf(PreferenceHelper.getInstance(context).getToken()));
//                    }
                    Logger.e(requestTag + " postParams", formPart.toString() + "");
                }
                return formPart;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
//                params.put("avatar", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mAvatarImage.getDrawable()), "image/jpeg"));
//                params.put("cover", new DataPart("file_cover.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mCoverImage.getDrawable()), "image/jpeg"));

                Set<String> keySet = filePart.keySet();
                for (String key : filePart.keySet()) {
                    try {
                        byte[] data = FileUtils.readFile(filePart.get(key));
                        params.put(key, new DataPart(FileUtils.getFileName(filePart.get(key)), data
                                , "audio/3gpp"));
                    } catch (IOException e) {
                        Logger.e(requestTag + " data ex", e.getMessage() + " ");
                    }
                }

                return params;
            }
        };

        RetryPolicy retryPolicy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setTag(requestTag);

        requestQueue.add(multipartRequest);
    }

    private void cancelAllRequests() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                Logger.e("DEBUG", "request running: " + request.getTag().toString());
                return true;
            }
        });
    }
}
