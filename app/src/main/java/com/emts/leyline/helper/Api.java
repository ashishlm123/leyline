package com.emts.leyline.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {

    private static Api api;

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }

    }

    public final String apiKey = "EMTSEVENTappBYDcSudhan";

    private String getIp() {
//        return "http://nepaimpressions.com/dev/eventapp/api/Api_Controller/";
 //    return "http://www.createleyline.com/eventapp/api/Api_Controller/";
        return "http://202.166.198.151:7777/eventapp_v2/api/Api_Controller/";
    }

    public final String getEventCategory = getIp() + "getEventCategory";

    public final String registerUrl = getIp() + "signup";
    public final String loginUrl = getIp() + "login";
    public final String forgotPasswordUrl = getIp() + "forgot_password";
    public final String changePassword = getIp() + "change_password";

    public final String getNearByEventsUrl = getIp() + "find_near_event";
    public final String getFeedsUrl = getIp() + "get_feeds";
    public final String addEvent = getIp() + "addEvent";
    public final String searchEventUrl = getIp() + "search_events";
    public final String attendEventUrl = getIp() + "mannual_checking";
    public final String editEvent = getIp() + "update_event";
    public final String doRate = getIp() + "do_rate";
    public final String getEventViaDate = getIp() + "get_EventDetails";
    public String deleteEventApi = getIp() + "delete_event";
    public final String likeEventApi = getIp() + "likeEvent";
    public final String inviteToEventUrl = getIp() + "invite_event";
    public final String searchOtherUsers = getIp() + "search_user";
    public final String getUserProfile = getIp() + "get_user_profile";
    public final String userProfilePicUpdateUrl = getIp() + "update_ProfileImage";
    public final String userCoverPicUpdateurl = getIp() + "update_CoverImage";
    public final String userEventDetailsListing = getIp() + "user_events_list";
    public final String profileUpdate = getIp() + "profile_update";
    public final String friend_ManagementUrl = getIp() + "friend_management";
    public final String getUserFriendlists = getIp() + "user_friend_listing";
    public final String autoCheckinUrl = getIp() + "auto_checking";
    public final String licensces = getIp() + "licencing";
    public final String termsOfServices = getIp() + "terms_condition";
    public final String privacyPolicy = getIp() + "privacy_policy";
    public final String userRateUrl = getIp() + "rateUser";
    public final String getChatThread = getIp() + "get_chat_list";
    public final String getChatMessage = getIp() + "get_message";
    public final String sendMessage = getIp() + "send_message";
    public final String createThreadApi = getIp() + "create_thread";
    public final String userSupportUrl = getIp() + "userSupport";
    public final String userCommentUrl = getIp() + "commentSection";
    public final String feedsCommentListingUrl = getIp() + "event_comment_list";
    public final String feedsLikeUrl = getIp() + "feeds_like";
    public final String getNotificationsUrl = getIp() + "getNotification";
    public final String hashTagUrl = getIp() + "get_hash_lists";
    public final String replyCommentUrl = getIp() + "get_comment_replies_list";
    public final String eventInSameLocationUrl = getIp() + "show_same_location_event";
    public final String requestToMergeUrl = getIp() + "request_to_merge_event";
    public final String acceptRejectMergeUrl = getIp() + "accept_reject_merge_request";
    public final String userPrivacyUrl = getIp() + "userPrivacy";

    public final String hastSearchUrl = getIp() + "searchusinghash";
    public final String suggestEventUrl = getIp() + "suggestEvents";
    public final String suggestUser = getIp() + "suggestPeople";
}