package com.emts.leyline.helper;

/**
 * Created by Srijana on 9/17/2017.
 */

public class MyTextUtils {
    public static String checkNull(String text) {
        if (text == null) {
            return "N/A";
        }
        if (text.equalsIgnoreCase("null")) {
            return "N/A";
        }
        return text;
    }

}
