package com.emts.leyline.helper;

import android.content.Context;

import com.emts.leyline.models.EventCategory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2016-07-07.
 */
public class Config {
    public static final String Sun = "1";
    public static final String Mon = "2";
    public static final String Tues = "3";
    public static final String Wed = "4";
    public static final String Thur = "5";
    public static final String Fri = "6";
    public static final String Sat = "7";
    public static final String EVERYONE = "1";
    public static final String FRIENDS = "2";
    public static final String ONLYME = "3";

    //dynamic category
    public static HashMap<String, EventCategory> eventTypeCache = new HashMap<>();

    public static ArrayList<EventCategory> getAllEventTypes(Context context) {
        eventTypeCache.clear();
        ArrayList<EventCategory> eventTypes = new ArrayList<>();
        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(context);
        String resObj = preferenceHelper.getString(PreferenceHelper.CATEGORY_JSON_RESPONSE, "");
        try {
            JSONObject eventCatRes = new JSONObject(resObj);
            JSONArray eventCatArray = eventCatRes.getJSONArray("event_categories");
            String iconDir = eventCatRes.getString("show_icon_path");
            for (int i = 0; i < eventCatArray.length(); i++) {
                JSONObject eachEventType = eventCatArray.getJSONObject(i);

                EventCategory eventType = new EventCategory();
                eventType.setEventCatId(eachEventType.getString("showcategory_id"));
                eventType.setEventCatName(eachEventType.getString("show_name"));
                eventType.setEventCatIcon(iconDir + eachEventType.getString("show_icon"));
                eventType.setEventCatColor(eachEventType.getString("show_color_code"));

                if (!eventTypeCache.containsKey(eventType.getEventCatId())) {
                    eventTypeCache.put(eventType.getEventCatId(), eventType);
                }

                eventTypes.add(eventType);
            }
        } catch (Exception e) {
            Logger.e("Config getAllEventTypes", e.getMessage());
        }
        return eventTypes;
    }

    public static EventCategory getEvent(Context context, String eventId) {
        Logger.e("Config getEvent", "FOR ID: " + eventId);
        if (eventTypeCache.containsKey(eventId)) {
            return eventTypeCache.get(eventId);
        } else {
            getAllEventTypes(context);
            return eventTypeCache.get(eventId);
        }
    }
}
