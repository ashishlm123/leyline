package com.emts.leyline.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.emts.leyline.R;


public class RobotoTextView extends AppCompatTextView {

    public RobotoTextView(Context context) {
        super(context);
        if (isInEditMode()) return;
        parseAttributes(null);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }


    private void parseAttributes(AttributeSet attrs) {
        int typeface;
        if (attrs == null) {
            //Not created from xml
        } else {
            TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.RobotoTextView);
            typeface = values.getInt(R.styleable.RobotoTextView_typeface, -1);
            values.recycle();

            if (typeface != -1) {
                setTypeface(getCustomTypeface(getContext(), typeface));
            }
        }
    }

    public static Typeface getCustomTypeface(Context context, int typeface) {
        switch (typeface) {
            case CustomTypeface.RALEWAY_BOLD:
                if (CustomTypeface.ralewayBold == null) {
                    CustomTypeface.ralewayBold = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_bold.ttf");
                }
                return CustomTypeface.ralewayBold;

            case CustomTypeface.RALEWAY_EXTRA_BOLD:
                if (CustomTypeface.ralewayExtraBold == null) {
                    CustomTypeface.ralewayExtraBold = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_extra_bold.ttf");
                }
                return CustomTypeface.ralewayExtraBold;

            case CustomTypeface.RALEWAY_EXTRA_LIGHT:
                if (CustomTypeface.ralewayExtraLight == null) {
                    CustomTypeface.ralewayExtraLight = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_extra_light.ttf");
                }
                return CustomTypeface.ralewayExtraLight;

            case CustomTypeface.RALEWAY_HEAVY:
                if (CustomTypeface.ralewayHeavy == null) {
                    CustomTypeface.ralewayHeavy = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_heavy.ttf");
                }
                return CustomTypeface.ralewayHeavy;

            case CustomTypeface.RALEWAY_LIGHT:
                if (CustomTypeface.ralewayLight == null) {
                    CustomTypeface.ralewayLight = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_light.ttf");
                }
                return CustomTypeface.ralewayLight;

            case CustomTypeface.RALEWAY_MEDIUM:
                if (CustomTypeface.ralewayMedium == null) {
                    CustomTypeface.ralewayMedium = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_medium.ttf");
                }
                return CustomTypeface.ralewayMedium;

            case CustomTypeface.RALEWAY_REGULAR:
                if (CustomTypeface.ralewayRegular == null) {
                    CustomTypeface.ralewayRegular = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_regular.ttf");
                }
                return CustomTypeface.ralewayRegular;

            case CustomTypeface.RALEWAY_SEMI_BOLD:
                if (CustomTypeface.ralewaySemiBold == null) {
                    CustomTypeface.ralewaySemiBold = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_semi_bold.ttf");
                }
                return CustomTypeface.ralewaySemiBold;

            case CustomTypeface.RALEWAY_THIN:
                if (CustomTypeface.ralewayThin == null) {
                    CustomTypeface.ralewayThin = Typeface.createFromAsset(context.getAssets(), "fonts/raleway_thin.ttf");
                }
                return CustomTypeface.ralewayThin;

            default:
                return Typeface.DEFAULT;
        }
    }

    private static class CustomTypeface {
        static final int RALEWAY_BOLD = 19;
        static final int RALEWAY_EXTRA_BOLD = 20;
        static final int RALEWAY_EXTRA_LIGHT = 21;
        static final int RALEWAY_HEAVY = 22;
        static final int RALEWAY_LIGHT = 23;
        static final int RALEWAY_MEDIUM = 24;
        static final int RALEWAY_REGULAR = 25;
        static final int RALEWAY_SEMI_BOLD = 26;
        static final int RALEWAY_THIN = 27;



        private static Typeface ralewayBold;
        private static Typeface ralewayExtraBold;
        private static Typeface ralewayExtraLight;
        private static Typeface ralewayHeavy;
        private static Typeface ralewayLight;
        private static Typeface ralewayMedium;
        private static Typeface ralewayRegular;
        private static Typeface ralewaySemiBold;
        private static Typeface ralewayThin;
    }
}
