package com.emts.leyline.customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.TextView;

import com.emts.leyline.models.UserModel;

import java.util.ArrayList;

/**
 * Created by User on 2017-10-30.
 */

public class TagMentionInputView extends AppCompatEditText implements TextWatcher {
    MyActivityWatcher activityWatcher;

    public TagMentionInputView(Context context) {
        super(context);

        init();
    }

    public TagMentionInputView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public TagMentionInputView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        this.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public interface MyActivityWatcher {
        void onActivityStart(boolean isMention);

        void onActivity(boolean isMention, ArrayList<UserModel> userList);

        void onActivityEnd(boolean isMention);
    }

    public void setActivityWatcher(MyActivityWatcher activityWatcher) {
        this.activityWatcher = activityWatcher;
    }

}
