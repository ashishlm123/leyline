package com.emts.leyline;

import android.location.Location;

/**
 * Created by User on 2017-08-21.
 */

public class LocationEvent {
    public Location location;

    LocationEvent(Location location) {
        this.location = location;
    }
}
