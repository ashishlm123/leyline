package com.emts.leyline;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.OnSuccessListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by User on 2017-07-31.
 */

public class LocationUpdateService extends Service {
    private LocationRequest mLocationRequest;
    private static final String LOGSERVICE = "LocationUpdateService";
    FusedLocationProviderClient mFusedLocationClient;
    public static Location lastKnownLocation = null;
    Location location = null;
    private LocationCallback locationCallback;
    private LocationSettingsRequest locationSettingsRequest;

    private static long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 180000;
    private static long JOB_IN_HAND_INTERVAL = 8000;

    //my handler for location on no GPS
    Handler mHandler;

    boolean isTestCase = false;

    @Override
    public void onCreate() {
        super.onCreate();

        if (isTestCase) {
            return;
        }

        mHandler = new Handler();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        initLocationRequest(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        Logger.e(LOGSERVICE, "onCreate");

    }

    Runnable mLastLocationRunnable = new Runnable() {
        @Override
        public void run() {
            //get if last known location is available
            location = getLastKnowLocation();
            if (location == null) {
                if (!checkIfGPSEnable(LocationUpdateService.this)) {
                    Logger.e(LOGSERVICE + " getLastLocationRunnable", "GPS is off so run mLastLocationRunnable");
                    mHandler.postDelayed(mLastLocationRunnable, FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
                } else {
                    Logger.e(LOGSERVICE + " remove getLastLocationRunnable", "GPS is on so remove mLastLocationRunnable");
                    mHandler.removeCallbacks(mLastLocationRunnable);
                }
            } else {
                //get location here and update to server
                Logger.e(LOGSERVICE + " location here", location + " ");
                mHandler.removeCallbacks(mLastLocationRunnable);
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.e(LOGSERVICE, "onStartCommand");

//        if (intent != null) {
//            if (intent.getBooleanExtra("is_attending_event", false)) {
//                FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = JOB_IN_HAND_INTERVAL;
//            } else {
//                FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 180000;
//            }
//        } else {
//            FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 180000;
//        }

        Logger.e("locationupdateservice testcase interval", FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS + "");
        if (isTestCase) {
            doYourTest();
            return START_STICKY;
        }

        removeLocationUpdateCallback();
        initLocationRequest(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        startLocationUpdateCallback();

        //for last known try here
        mHandler.post(mLastLocationRunnable);

        return START_STICKY;
    }

    public void setLocationUpdateTimeInterval(long timeInterval) {
//        UPDATE_INTERVAL_IN_MILLISECONDS = timeInterval;
        FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = timeInterval;
        removeLocationUpdateCallback();
        initLocationRequest(timeInterval);
        startLocationUpdateCallback();
    }

    public void startLocationUpdateCallback() {
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Logger.e(LOGSERVICE + " permission required", " permission required here");
            } else {
                Logger.e(LOGSERVICE + " request loc update", " request location update ");
                mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                        locationCallback, Looper.myLooper());
            }
        } catch (NullPointerException e) {
            Logger.e(LOGSERVICE + " null pointer", e.getMessage() + " ");
            initLocationRequest(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
            startLocationUpdateCallback();
        }
    }

    public void removeLocationUpdateCallback() {
        try {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        } catch (Exception e) {
        }
    }

    private void initLocationRequest(long intervalTime) {
        mLocationRequest = new LocationRequest();
//        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setInterval(intervalTime);
        mLocationRequest.setFastestInterval(intervalTime);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        Logger.e(LOGSERVICE, " location interval :" + intervalTime);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        locationSettingsRequest = builder.build();

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location currentLocation = locationResult.getLastLocation();
                Logger.e(LOGSERVICE + " onLocationResult callback", currentLocation.toString() + " ");

                if (NetworkUtils.isInNetwork(LocationUpdateService.this) && currentLocation != null) {
                    updateUserLocation(String.valueOf(currentLocation.getLatitude()),
                            String.valueOf(currentLocation.getLongitude()));

                    lastKnownLocation = currentLocation;
                    EventBus.getDefault().post(new LocationEvent(currentLocation));
                } else {
                    Logger.e(LOGSERVICE, "No internet connected to update location");
                }
            }
        };
    }

    private void updateUserLocation(String lat, String lon) {
        if (!PreferenceHelper.getInstance(LocationUpdateService.this).isLogin()) {
            return;
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(LocationUpdateService.this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("latitude", String.valueOf(lat));
        postParams.put("longitude", String.valueOf(lon));
        postParams.put("event_id", "0");

        vHelper.addVolleyRequestListeners(Api.getInstance().autoCheckinUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "updateUserLocation");
    }

    @Override
    public void onDestroy() {
        removeLocationUpdateCallback();
        super.onDestroy();

        Logger.e(LOGSERVICE + " onDestroy", "service is destroyed");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stopService() {
        stopSelf();
    }

    public Location getLastKnowLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
        } else {
            //To get last known location
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location l) {
                    location = l;
                    Logger.e(LOGSERVICE + " lastknownlocation", location + " **");
                    if (location != null) {
                        updateUserLocation(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                    }
                }
            });
        }
        return location;
    }

    public static boolean checkIfGPSEnable(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    //    //my test data here
    ArrayList<String> myTestDataArray = new ArrayList<>(Arrays.asList("27.689561-85.310654",
            "27.687917-85.310128", "27.686758-85.312242", "27.686245-85.313422", "27.687224-85.313840",
            "27.685960-85.316640", "27.686720-85.317284", "27.687778-85.316379"));

    private void doYourTest() {
        mHandler1.postDelayed(runnable1, FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
    }

    Handler mHandler1 = new Handler();
    Runnable runnable1 = new Runnable() {
        @Override
        public void run() {
            try {
                if (myTestDataArray.size() > 0) {
                    String[] loc = myTestDataArray.get(0).split("-");
                    Toast.makeText(LocationUpdateService.this, loc[0] + ", " + loc[1], Toast.LENGTH_LONG).show();
                    Logger.e("locationupdateservice testcase", "Loc: " + loc[0] + ", " + loc[1]
                            + " ArraySixe now : " + myTestDataArray.size());
                    updateUserLocation(loc[0], loc[1]);
                    myTestDataArray.remove(0);

                    if (myTestDataArray.size() > 0) {
                        mHandler1.postDelayed(runnable1, FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
                    }
                }
            } catch (Exception e) {
                Logger.e("locationupdateservice testcase ex", e.getMessage() + " ");
            }
        }
    };

}
