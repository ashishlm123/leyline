package com.emts.leyline.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.LocationUpdateService;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.MyTextUtils;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventCategory;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.UserModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hendraanggrian.socialview.SocialView;
import com.hendraanggrian.widget.SocialTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import cn.lankton.flowlayout.FlowLayout;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class EventDetailsActivity extends AppCompatActivity {
    EventModel eventModel;
    Button openEvent;
    TextView eventName, hostedBy, eventDate, eventTime, eventLocation, tvRate;
    SocialTextView eventDescription;
    RatingBar ratingBar;
    ImageView eventImage;
    Button btnAttend, btnMayBe, btnDelete, btnCheckIn, btnInvite, btnLeave, btnMerge;
    PreferenceHelper helper;
    Location currentLocation;
    FlowLayout flowLayout;
    LinearLayout commentHere;
    RelativeLayout ratingHolder;
    MenuItem item;
    boolean isMergeRequest;
    String mergeRequesterEventId;
    private GoogleMap mMap;
    int position, calenderPosition, createdPosition, attendingPosition, attendedPosition;
    public static final int REQUEST_LOCATION_PERMISSION = 678;
    public static final int EDIT_REQUEST = 679;

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    private static void tintMenuItemIcon(int color, MenuItem item) {
        final Drawable drawable = item.getIcon();
        if (drawable != null) {
            final Drawable wrapped = DrawableCompat.wrap(drawable);
            drawable.mutate();
            DrawableCompat.setTint(wrapped, color);
            item.setIcon(drawable);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentLocation = LocationUpdateService.lastKnownLocation;
        helper = PreferenceHelper.getInstance(this);

        setContentView(R.layout.activity_event_details);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        eventModel = (EventModel) intent.getSerializableExtra("eventmodel");
        position = intent.getIntExtra("position", -1);
        calenderPosition = intent.getIntExtra("calendarPosition", -1);
        createdPosition = intent.getIntExtra("createdPosition", -1);
        attendingPosition = intent.getIntExtra("attendingPosition", -1);
        attendedPosition = intent.getIntExtra("attendedPosition", -1);
        isMergeRequest = intent.getBooleanExtra("requestMerge", false);
        mergeRequesterEventId = intent.getStringExtra("requesterEvent");

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Event Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.inflateMenu(R.menu.like_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                EventDetailsActivity.this.item = item;
                if (eventModel.isLiked()) {
                    if (NetworkUtils.isInNetwork(EventDetailsActivity.this)) {
                        userLikeEventTask(false);
                    } else {
                        AlertUtils.showToast(EventDetailsActivity.this, "No Internet Connection...");
                    }
                } else {
                    if (NetworkUtils.isInNetwork(EventDetailsActivity.this)) {
                        userLikeEventTask(true);
                    } else {
                        AlertUtils.showToast(EventDetailsActivity.this, "No Internet Connection...");
                    }
                }
                return false;
            }
        });

        ratingHolder = findViewById(R.id.r1);

        eventImage = findViewById(R.id.event_icon);
        eventName = findViewById(R.id.event_name);
        hostedBy = findViewById(R.id.hosted_by);
        openEvent = findViewById(R.id.btn_open_event);
        ratingBar = findViewById(R.id.rating_bar);
        eventDescription = findViewById(R.id.event_description);

        eventDescription.setOnHashtagClickListener(new Function2<SocialView, CharSequence, Unit>() {
            @Override
            public Unit invoke(SocialView socialView, CharSequence charSequence) {
                Logger.e("hashTag Clicked", "ON->" + eventDescription.getText().toString() + "/nTAG->" + charSequence);
                Intent intent = new Intent(EventDetailsActivity.this, HashTagSearchActivity.class);
                intent.putExtra("hashtag", charSequence.toString());
                startActivity(intent);
                return null;
            }
        });

        eventDate = findViewById(R.id.event_date);
        eventTime = findViewById(R.id.event_time);
        eventLocation = findViewById(R.id.event_location);
        tvRate = findViewById(R.id.tv_rate);

        flowLayout = findViewById(R.id.flowlayout);

        commentHere = findViewById(R.id.comment_here);
        commentHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), CommentActivity.class);
                intent1.putExtra("eventid", eventModel.getEventId());
                startActivity(intent1);
            }
        });

        btnAttend = findViewById(R.id.btn_attend_event);
        btnMayBe = findViewById(R.id.btn_may_be);
        btnMayBe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userAttendEventTask("maybe");
            }
        });
        btnCheckIn = findViewById(R.id.btn_checkin);
        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentLocation = LocationUpdateService.lastKnownLocation;
                if (currentLocation != null) {
                    checkInTask();
                } else {
                    try {
                        if (ActivityCompat.checkSelfPermission(EventDetailsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(EventDetailsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_LOCATION_PERMISSION);
                        } else {
                            AlertUtils.showSnack(EventDetailsActivity.this, eventDate, "Please give location permission");
                        }
                    } catch (Exception e) {
                        Logger.e("locationPermission ex", e.getMessage());
                    }

                }
            }
        });
        btnDelete = findViewById(R.id.btn_delete_event);
        btnInvite = findViewById(R.id.btn_invite_event);
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), InviteFriends.class);
                intent.putExtra("eventModel", eventModel);
                startActivity(intent);
            }
        });
        btnLeave = findViewById(R.id.btn_leave_event);

        btnMerge = findViewById(R.id.btn_merge);


        setModel(eventModel);
        //set button action for events by status
        setButtonActionsOnEventStatus();

        if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_ATTENDED)) {
            ratingHolder.setVisibility(View.VISIBLE);
        } else {
            ratingHolder.setVisibility(View.GONE);
        }

        tvRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRateAlert();
            }
        });

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                item = toolbar.getMenu().findItem(R.id.like_event);
                if (item != null) {
                    if (eventModel.isLiked()) {
                        Drawable favoriteIcon = DrawableCompat.wrap(item.getIcon());
                        ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.colorAccent, getTheme());
                        DrawableCompat.setTintList(favoriteIcon, colorSelector);
                        item.setIcon(favoriteIcon);
                    } else {
                        Drawable favoriteIcon = DrawableCompat.wrap(item.getIcon());
                        ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.white, getTheme());
                        DrawableCompat.setTintList(favoriteIcon, colorSelector);
                        item.setIcon(favoriteIcon);
                    }
                } else {
                    Logger.e("itemnull", "item is null");
                }
            }
        });

    }

    private void userLikeEventTask(final boolean isLiked) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", helper.getUserId());
        postMap.put("event_id", eventModel.getEventId());

        if (isLiked) {
            postMap.put("status", "true");
        } else {
            postMap.put("status", "false");
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().likeEventApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (isLiked) {
                                    eventModel.setLiked(true);
                                    eventModel.setEventLikes(res.getString("count"));
                                    Drawable favoriteIcon = DrawableCompat.wrap(item.getIcon());
                                    ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.colorAccent, getTheme());
                                    DrawableCompat.setTintList(favoriteIcon, colorSelector);
                                    item.setIcon(favoriteIcon);
                                } else {
                                    eventModel.setLiked(false);
                                    eventModel.setEventLikes(res.getString("count"));
                                    Drawable favoriteIcon = DrawableCompat.wrap(item.getIcon());
                                    ColorStateList colorSelector = ResourcesCompat.getColorStateList(getResources(), R.color.white, getTheme());
                                    DrawableCompat.setTintList(favoriteIcon, colorSelector);
                                    item.setIcon(favoriteIcon);
                                }
                            } else {
                                AlertUtils.showSnack(EventDetailsActivity.this, eventDescription, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userLikeEventTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userLikeEventTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", errMsg);
                    }
                }, "userLikeEventTask");
    }


    private void setButtonActionsOnEventStatus() {
        //case eventModel created by self
        if (isMergeRequest) {
            btnMerge.setVisibility(View.VISIBLE);
            btnMerge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(EventDetailsActivity.this)) {
                        userMergeEventTask();
                    }
                }
            });
            btnAttend.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnInvite.setVisibility(View.GONE);
            btnCheckIn.setVisibility(View.GONE);
            btnMayBe.setVisibility(View.GONE);
            btnLeave.setVisibility(View.GONE);
            commentHere.setVisibility(View.GONE);
            return;
        }
        UserModel[] userArray = eventModel.getHostedBy();
        for (UserModel anUserArray : userArray) {
            if (anUserArray.getUserId().equals(PreferenceHelper.getInstance(EventDetailsActivity.this).getUserId())) {
                if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_ATTENDED)) {
                    btnAttend.setText("Attended");
                    btnAttend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });
                    btnAttend.setVisibility(View.VISIBLE);
                    btnDelete.setVisibility(View.GONE);
                    btnInvite.setVisibility(View.GONE);
                    btnCheckIn.setVisibility(View.GONE);
                    btnMayBe.setVisibility(View.GONE);
                    btnLeave.setVisibility(View.GONE);
                    return;
                } else if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_CLOSED)) {
                    btnAttend.setText("Closed");
                    btnAttend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });

                    btnAttend.setVisibility(View.VISIBLE);
                    btnDelete.setVisibility(View.GONE);
                    btnInvite.setVisibility(View.GONE);
                    btnCheckIn.setVisibility(View.GONE);
                    btnMayBe.setVisibility(View.GONE);
                    btnLeave.setVisibility(View.GONE);
                    commentHere.setVisibility(View.GONE);
                    return;
                }
                btnAttend.setText("Edit");
                btnAttend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent1 = new Intent(getApplicationContext(), AddEventActivity.class);
                        intent1.putExtra("eventModel", eventModel);
                        intent1.putExtra("position", position);
                        startActivityForResult(intent1, EDIT_REQUEST);
                    }
                });

                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertUtils.showErrorAlert(EventDetailsActivity.this, "Are you sure you want to delete this event?",
                                new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        deleteEventTask();
                                    }
                                });
                    }
                });

                btnAttend.setVisibility(View.VISIBLE);
                btnDelete.setVisibility(View.VISIBLE);
                btnInvite.setVisibility(View.VISIBLE);
                btnCheckIn.setVisibility(View.VISIBLE);
                btnMayBe.setVisibility(View.GONE);
                btnLeave.setVisibility(View.GONE);
                return;
            }
        }
        if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_ATTENDED)) {
            btnAttend.setText("Attended");
            btnAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

            btnAttend.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.GONE);
            btnInvite.setVisibility(View.GONE);
            btnCheckIn.setVisibility(View.GONE);
            btnMayBe.setVisibility(View.GONE);
            btnLeave.setVisibility(View.GONE);
        } else if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_ATTENDING)) {
            btnAttend.setText("Attending");
            btnAttend.setBackgroundColor(getResources().getColor(R.color.btn_color_attending));
            btnAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

            btnAttend.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.GONE);
            btnInvite.setVisibility(View.VISIBLE);
            btnCheckIn.setVisibility(View.VISIBLE);
            btnMayBe.setVisibility(View.GONE);
            btnLeave.setVisibility(View.VISIBLE);
            btnLeave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userAttendEventTask("leave");
                }
            });
        } else if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_CLOSED)) {
            btnAttend.setText("Closed");
            btnAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

            btnAttend.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.GONE);
            btnInvite.setVisibility(View.GONE);
            btnCheckIn.setVisibility(View.GONE);
            btnMayBe.setVisibility(View.GONE);
            btnLeave.setVisibility(View.GONE);
            commentHere.setVisibility(View.GONE);
        } else if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_MAY_BE)) {
            btnAttend.setText("Attend");
            btnAttend.setBackgroundColor(getResources().getColor(R.color.event_music_color));
            btnAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userAttendEventTask("attending");
                }
            });

            btnAttend.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.GONE);
            btnInvite.setVisibility(View.GONE);
            btnCheckIn.setVisibility(View.GONE);
            btnMayBe.setVisibility(View.GONE);
            btnLeave.setVisibility(View.GONE);
        } else if (eventModel.getEventAttendStatus().equalsIgnoreCase(EventModel.EVENT_NONE)) {
            btnAttend.setText("Attend");
            btnAttend.setBackgroundColor(getResources().getColor(R.color.event_music_color));
            btnAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userAttendEventTask("attending");
                }
            });

            btnAttend.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.GONE);
            btnInvite.setVisibility(View.GONE);
            btnCheckIn.setVisibility(View.GONE);
            btnMayBe.setVisibility(View.VISIBLE);
            btnLeave.setVisibility(View.GONE);
        }
    }

    private void userMergeEventTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(EventDetailsActivity.this, "Please wait..");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = volleyHelper.getPostParams();

        postMap.put("merge_to", eventModel.getEventId());
        if (!TextUtils.isEmpty(mergeRequesterEventId) && !mergeRequesterEventId.equals("null")) {
            postMap.put("merge_by", mergeRequesterEventId);
        }

        volleyHelper.addVolleyRequestListeners(Api.getInstance().requestToMergeUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showErrorAlert(EventDetailsActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        setResult(RESULT_OK);
                                        EventDetailsActivity.this.finish();
                                    }
                                });

                            } else {
                                AlertUtils.showToast(EventDetailsActivity.this, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userMergeEventTask error ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userMergeEventTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", errMsg);
                    }
                }, "userMergeEventTask");
    }

    private void checkInTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(EventDetailsActivity.this, "Please wait..");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("event_id", eventModel.getEventId());
        postMap.put("latitude", String.valueOf(currentLocation.getLatitude()));
        postMap.put("longitude", String.valueOf(currentLocation.getLongitude()));

        vHelper.addVolleyRequestListeners(Api.getInstance().autoCheckinUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showToast(EventDetailsActivity.this, res.getString("message") + " ");
                                eventModel.setEventAttendStatus(EventModel.EVENT_ATTENDED);
                                setButtonActionsOnEventStatus();
                            } else {
                                AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", res.getString("message") + "");
                            }
                        } catch (JSONException e) {
                            Logger.e("checkInTask res Ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("checkInTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", errMsg);
                    }
                }, "checkInTask");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted :)
                    checkInTask();
                } else {
                    for (int i = 0, len = permissions.length; i < len; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            // user rejected the permission
                            boolean showRationale = false;
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                showRationale = shouldShowRequestPermissionRationale(permission);
                            }
                            if (!showRationale) {
                                AlertUtils.showSnack(EventDetailsActivity.this, eventDate, "Please give location permission in order to checkin");
                                // user also CHECKED "never ask again"
                                // you can either enable some fall back,
                                // disable features of your app
                                // or open another dialog explaining
                                // again the permission and directing to
                                // the app setting
                            } else if (Manifest.permission.WRITE_CONTACTS.equals(permission)) {
                                checkInTask();
                                // user did NOT check "never ask again"
                                // this is a good place to explain the user
                                // why you need the permission and ask if he wants
                                // to accept it (the rationale)
                            }
                        }
                    }
                }
                return;

            }

        }
    }

    private void deleteEventTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(EventDetailsActivity.this, "Please wait..");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("event_id", eventModel.getEventId());
        vHelper.addVolleyRequestListeners(Api.getInstance().deleteEventApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showToast(EventDetailsActivity.this, res.getString("message") + " ");
                                Intent intent = new Intent();
                                intent.putExtra("eventId", eventModel.getEventId());
                                intent.putExtra("isDeleted", true);
                                intent.putExtra("position", position);
                                setResult(RESULT_OK, intent);
                                EventDetailsActivity.this.finish();
                            } else {
                                AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", res.getString("message") + "");
                            }
                        } catch (JSONException e) {
                            Logger.e("deleteEventTask res Ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("deleteEventTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", errMsg);
                    }
                }, "deleteEventTask");
    }

    private void userAttendEventTask(final String status) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(EventDetailsActivity.this, "Please wait..");
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("event_id", eventModel.getEventId());
        postMap.put("status", status);
        vHelper.addVolleyRequestListeners(Api.getInstance().attendEventUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        if (res.getString("event_status").equalsIgnoreCase(EventModel.EVENT_ATTEND)) {
                            eventModel.setEventAttendStatus(EventModel.EVENT_ATTENDING);
                        } else if (res.getString("event_status").equalsIgnoreCase(EventModel.EEVENT_LEAVE)) {
                            eventModel.setEventAttendStatus(EventModel.EVENT_NONE);
                        } else if (res.getString("event_status").equalsIgnoreCase(EventModel.EVENT_MAY_BE)) {
                            eventModel.setEventAttendStatus(EventModel.EVENT_MAY_BE);
                        } else if (res.getString("event_status").equalsIgnoreCase(EventModel.EVENT_ATTENDING)) {
                            eventModel.setEventAttendStatus(EventModel.EVENT_ATTENDING);
                        }
                        setButtonActionsOnEventStatus();
                    } else {
                        AlertUtils.showSnack(EventDetailsActivity.this, eventName, res.getString("message"));
                    }

                } catch (JSONException e) {
                    Logger.e("userAttendEventTask res Ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userAttendEventTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", errMsg);
            }
        }, "userAttendEventTask");
    }

    private void showRateAlert() {
        final AlertDialog.Builder alertDailog = new AlertDialog.Builder(EventDetailsActivity.this);
        final View view = LayoutInflater.from(EventDetailsActivity.this).inflate(R.layout.alert_rate_event, null);
        alertDailog.setView(view);
        final RatingBar rate = view.findViewById(R.id.rating_bar);
        Button btnOk = view.findViewById(R.id.btn_ok);
        Button btnCancel = view.findViewById(R.id.btn_cancel);
        final Dialog dialog = alertDailog.show();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                userRatingTask(rate.getRating());
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void userRatingTask(final Float rating) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(EventDetailsActivity.this, "Please wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("event_id", eventModel.getEventId());
        postMap.put("rate", String.valueOf(rating));

        vHelper.addVolleyRequestListeners(Api.getInstance().doRate, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        ratingBar.setRating((float) Math.ceil(Float.parseFloat(res.getString("rate"))));
                        eventModel.setEventRating(Float.parseFloat(res.getString("rate")));
                    } else {
                        AlertUtils.showErrorAlert(EventDetailsActivity.this, res.getString("message"), null);
                    }
                } catch (JSONException e) {
                    Logger.e("userRatingTask excep", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userRatingTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(EventDetailsActivity.this, "Error !!!", errMsg);
            }
        }, "userRatingTask");
    }

    private void showEventMarker(final EventModel eventModel) {
        View marker = ((LayoutInflater) EventDetailsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.custom_layout_marker, null);

        TextView markerEventName = marker.findViewById(R.id.event_name);
        ImageView eventIcon = marker.findViewById(R.id.event_icon);
        markerEventName.setText(eventModel.getEventName());
        EventCategory eventCat = Config.getEvent(EventDetailsActivity.this, eventModel.getEventCatId());
        eventIcon.setBackgroundColor(Color.parseColor(eventCat.getEventCatColor()));
        btnAttend.setBackgroundColor(Color.parseColor(eventCat.getEventCatColor()));
        Picasso.with(EventDetailsActivity.this).load(eventCat.getEventCatIcon()).into(eventIcon);

        LatLng eventLocation = new LatLng(eventModel.getLatitude(), eventModel.getLongitude());
        Marker customMarker = mMap.addMarker(new MarkerOptions()
                .position(eventLocation)
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(EventDetailsActivity.this, marker))));

        customMarker.setTag(eventModel);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("eventModel", eventModel);
        intent.putExtra("position", position);
        intent.putExtra("calenderPosition", calenderPosition);
        intent.putExtra("createdPosition", createdPosition);
        intent.putExtra("attendingPosition", attendingPosition);
        intent.putExtra("attendedPosition", attendedPosition);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    private void setModel(final EventModel eventModel) {
        eventName.setText(MyTextUtils.checkNull(eventModel.getEventName()));
        ratingBar.setRating((float) Math.ceil(eventModel.getEventRating()));
        eventDescription.setText(MyTextUtils.checkNull(eventModel.getEventDesc()));

        SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf3 = new SimpleDateFormat("MM-dd-yyyy");

        Date eventStartTime;
        Date eventStartDate;

        try {
            eventStartTime = sdf1.parse(eventModel.getEventTime());
            eventStartDate = sdf2.parse(eventModel.getEventDate());
            eventDate.setText(sdf3.format(eventStartDate));
            eventTime.setText(sdf.format(eventStartTime));
        } catch (ParseException e) {
            eventTime.setText(eventModel.getEventTime());
            Logger.e("Error", String.valueOf(e));
        }

       // eventDate.setText(DateUtils.convertDate(eventModel.getEventDate()));
        eventLocation.setText(MyTextUtils.checkNull(eventModel.getEventLocation()));
        Logger.e(String.valueOf(eventModel.getHostedBy().length), "lensadaa");

        UserModel[] userArray = eventModel.getHostedBy();
        flowLayout.removeAllViews();

        for (int i = 0; i < userArray.length; i++) {
            View view = LayoutInflater.from(EventDetailsActivity.this).inflate(R.layout.item_user_interests, null, false);
            TextView textView = view.findViewById(R.id.user_interests);
            textView.setText(MyTextUtils.checkNull(eventModel.getHostedBy()[i].getUserName()));
            view.setTag(userArray[i]);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UserModel userModel2 = (UserModel) view.getTag();
                    ProfileActivity.openProfile(EventDetailsActivity.this, userModel2, "0");
                }
            });
            flowLayout.addView(view);

        }

        if (!TextUtils.isEmpty(eventModel.getEventImage())) {
            Picasso.with(EventDetailsActivity.this).load(eventModel.getEventImage()).into(eventImage);
        }

        if (eventModel.getStatus().equalsIgnoreCase(EventModel.OPEN_EVENT)) {
            openEvent.setText("Open");
            btnDelete.setVisibility(View.GONE);
        } else if (eventModel.getStatus().equalsIgnoreCase(EventModel.CLOSE_EVENT)) {
            openEvent.setText("Close");
            btnDelete.setVisibility(View.GONE);
        }

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                LatLng nyCity = new LatLng(eventModel.getLatitude(), eventModel.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nyCity, 15));
                showEventMarker(eventModel);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EDIT_REQUEST) {
                EventModel eventModel1 = (EventModel) data.getSerializableExtra("eventmodel");
                EventDetailsActivity.this.eventModel = eventModel1;
                setModel(eventModel1);
                setResult(resultCode, data);
            }
        }
    }
}
