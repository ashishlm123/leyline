package com.emts.leyline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.InviteFriendAdapter;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.FriendsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2017-10-05.
 */

public class InviteFriends extends AppCompatActivity {
    EventModel eventModel;
    RecyclerView friendListView;
    ProgressBar progressBar, infiniteProgressBar;
    PreferenceHelper helper;
    TextView errorText;
    InviteFriendAdapter inviteAdapter;
    ArrayList<FriendsModel> friendList = new ArrayList<>();
    Toolbar toolbar;
    boolean isSelectAll = false;
    MenuItem menuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        eventModel = (EventModel) intent.getSerializableExtra("eventModel");
        helper = PreferenceHelper.getInstance(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Invite Friends");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.inflateMenu(R.menu.invite_sel_all);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_select_all) {
                    menuItem = item;
                    isSelectAll = !isSelectAll;
                    if (isSelectAll) {
                        inviteAdapter.selectAllFriends();
                        item.setTitle("Deselect all");
                    } else {
                        inviteAdapter.deSelectAllFriends();
                        item.setTitle("Select all");
                    }
                }
                return false;
            }
        });

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                menuItem = toolbar.getMenu().findItem(R.id.menu_select_all);
            }
        });

        progressBar = findViewById(R.id.progress);
        infiniteProgressBar =  findViewById(R.id.progress_infinite);
        errorText = findViewById(R.id.tv_error_message);

        friendListView = findViewById(R.id.list_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        friendListView.setLayoutManager(linearLayoutManager);

        inviteAdapter = new InviteFriendAdapter(this, friendList);
        friendListView.setAdapter(inviteAdapter);

        if (NetworkUtils.isInNetwork(this)) {
            userFriendsListingTask();
        } else {
            errorText.setText(R.string.no_internet);
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            friendListView.setVisibility(View.GONE);
        }
        Button btnInvite = findViewById(R.id.btn_invite);
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> selectedId
                        = inviteAdapter.getSelectedUsers();
                if (selectedId.isEmpty()) {
                    AlertUtils.showSnack(InviteFriends.this, view, "Please choose friends to invite");
                } else {
                    if (NetworkUtils.isInNetwork(InviteFriends.this)) {
                        userInviteFriendsTask(selectedId);
                    } else {
                        AlertUtils.showToast(InviteFriends.this, "No Internet Connection...");
                    }
                }
            }
        });
    }

    private void userInviteFriendsTask(ArrayList<String> selectedId) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(InviteFriends.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("event_id", eventModel.getEventId());
        for (int i = 0; i < selectedId.size(); i++) {
            postMap.put("invited_user_id[" + i + "]", selectedId.get(i));
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().inviteToEventUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showErrorAlert(InviteFriends.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        onBackPressed();
                                    }
                                });
                            } else {
                                AlertUtils.showErrorAlert(InviteFriends.this, res.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            Logger.e("userInviteFriendsTask ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        dialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userInviteFriendsTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(InviteFriends.this, "Error !!!", errMsg);
                    }
                }, "userInviteFriendsTask");
    }

    private void userFriendsListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        friendListView.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("profile_id", helper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserFriendlists, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray friendsArray = res.getJSONArray("friend_list");
                                for (int i = 0; i < friendsArray.length(); i++) {
                                    JSONObject jsonObject = friendsArray.getJSONObject(i);
                                    if (jsonObject.getString("friend_status").equalsIgnoreCase("isfriend")) {
                                        FriendsModel friendsModel = new FriendsModel();
                                        friendsModel.setFriends(true);
                                        friendsModel.setUserName(jsonObject.getString("name"));
                                        friendsModel.setUserId(jsonObject.getString("friend_id"));
                                        friendsModel.setImgUrl(res.getString("default_profile_img_Url") + jsonObject.getString("profile_image"));
                                        String threadId = jsonObject.getString("thread_id");
                                        if (threadId.equals("") || threadId.equals("null") || threadId.equals("0")) {
                                            friendsModel.setThreadId("0");
                                        } else {
                                            friendsModel.setThreadId(threadId);
                                        }
                                        friendList.add(friendsModel);
                                    }
                                }
                                if (friendList.size() > 0) {
                                    inviteAdapter.notifyDataSetChanged();
                                    friendListView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    if (menuItem != null) {
                                        menuItem.setVisible(false);
                                    }
                                    errorText.setText("No Friends");
                                    errorText.setVisibility(View.VISIBLE);
                                    friendListView.setVisibility(View.GONE);
                                }
                            } else {
                                if (menuItem != null) {
                                    menuItem.setVisible(false);
                                }
                                errorText.setText("No Friends");
                                errorText.setVisibility(View.VISIBLE);
                                friendListView.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userFriendsListingTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userFriendsListingTask error ex", e.getMessage() + "");
                        }
                        errorText.setText(errMsg);
                        errorText.setVisibility(View.VISIBLE);
                        friendListView.setVisibility(View.GONE);
                    }
                }, "userFriendsListingTask");
    }
}
