package com.emts.leyline.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity {
    EditText currentPassword, newPassword, confirmPassword;
    Button changePassword;
    TextView errCurrPass, errNewPass;
    PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Change Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        init();

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(ChangePasswordActivity.this)) {
                        userChangePasswordTask();
                    } else {
                        AlertUtils.showToast(ChangePasswordActivity.this, "No Internet Connection...");
                    }
                }
            }
        });

    }

    private void userChangePasswordTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ChangePasswordActivity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", helper.getUserId());
        postMap.put("cur_password", currentPassword.getText().toString().trim());
        postMap.put("new_password", confirmPassword.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().changePassword, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                dialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showErrorAlert(ChangePasswordActivity.this, "Password has been changed successfully.", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                onBackPressed();
                            }
                        });
                    } else {
                        AlertUtils.showErrorAlert(ChangePasswordActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                dialog.dismiss();
                            }
                        });
                    }
                } catch (JSONException e) {
                    Logger.e("userChangePasswordTask error ex", e.getMessage() + "");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                dialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userLicenscesTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(ChangePasswordActivity.this, "Error !!!", errMsg);
            }
        }, "userChangePasswordTask");
    }

    private void init() {
        currentPassword = (EditText) findViewById(R.id.current_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_new_password);
        changePassword = (Button) findViewById(R.id.btn_change_password);
        errCurrPass = (TextView) findViewById(R.id.err_current_password);
        errNewPass = (TextView) findViewById(R.id.err_new_password);
    }

    public boolean validation() {
        boolean isValid = true;

        //For Password
        if (currentPassword.getText().toString().trim().equals("")) {
            errCurrPass.setVisibility(View.VISIBLE);
            errCurrPass.setText("This field is required...");
            isValid = false;
        } else {
            if (currentPassword.getText().toString().trim().length() < 6) {
                errCurrPass.setVisibility(View.VISIBLE);
                errCurrPass.setText("Password should have more than or equal to six characters.");
                isValid = false;
            } else {
                errCurrPass.setVisibility(View.GONE);
            }
        }


        if (newPassword.getText().toString().trim().equals("")) {
            errNewPass.setVisibility(View.VISIBLE);
            errNewPass.setText("This field is required...");
            isValid = false;
        } else

        {
            if (newPassword.getText().toString().trim().length() >= 6) {
                if (newPassword.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                    errNewPass.setVisibility(View.GONE);
                } else {
                    errNewPass.setText("Password do not match");
                    errNewPass.setVisibility(View.VISIBLE);
                    isValid = false;
                }
            } else {
                errNewPass.setText("Password should have more than or equal to six characters.");
                errNewPass.setVisibility(View.VISIBLE);
                isValid = false;
            }
        }

        return isValid;


    }
}
