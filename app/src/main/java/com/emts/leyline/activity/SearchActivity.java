package com.emts.leyline.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.InviteFriendAdapter;
import com.emts.leyline.customviews.EndlessRecyclerViewScrollListener;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.FriendsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchActivity extends AppCompatActivity {
    RecyclerView searchResultListing;
    ProgressBar progressBar, infiniteProgressBar;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    TextView errorText;
    EditText searchBar;
    PreferenceHelper helper;
    InviteFriendAdapter searchResultAdapter;
    ArrayList<FriendsModel> searchList;
    int limit = 50;
    int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        searchList = new ArrayList<>();
        searchResultAdapter = new InviteFriendAdapter(SearchActivity.this, searchList);
        searchResultAdapter.setSearchFriends(true);
        searchResultListing = (RecyclerView) findViewById(R.id.list_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchActivity.this);
        searchResultListing.setLayoutManager(linearLayoutManager);
        searchResultListing.setAdapter(searchResultAdapter);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        infiniteProgressBar = findViewById(R.id.progress_infinite);
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (searchList.size() >= limit) {
                    userSearchProfileTask(searchBar.getText().toString().trim(), true);
                }
            }
        };
        searchResultListing.addOnScrollListener(infiniteScrollListener);

        errorText = (TextView) findViewById(R.id.tv_error_message);
        searchBar = (EditText) findViewById(R.id.search_bar);

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (NetworkUtils.isInNetwork(SearchActivity.this)) {
                    userSearchProfileTask(charSequence.toString(), false);
                } else {
                    AlertUtils.showToast(SearchActivity.this, "No Internet Connection...");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void userSearchProfileTask(String searchText, boolean isLoadMore) {
        if (isLoadMore) {

        } else {
            offset = 0;
            searchList.clear();
            searchResultAdapter.notifyDataSetChanged();
        }

        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        vHelper.cancelRequest("userSearchProfileTask");
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("searchkey", searchText);
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().searchOtherUsers, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                infiniteProgressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray searchResult = res.getJSONArray("users_list");
                        if (offset == 0) {
                            searchList.clear();
                        }
                        for (int i = 0; i < searchResult.length(); i++) {
                            JSONObject eachObj = searchResult.getJSONObject(i);
                            FriendsModel friendsModel = new FriendsModel();
                            friendsModel.setUserName(eachObj.getString("name"));
                            friendsModel.seteMail(eachObj.getString("email"));
                            friendsModel.setUserId(eachObj.getString("id"));
                            if (eachObj.getString("image").equals("") || eachObj.getString("image").equals("null")) {
                                friendsModel.setImgUrl("");
                            }
                            friendsModel.setImgUrl(res.getString("profile_img_Url") + eachObj.getString("image"));
                            searchList.add(friendsModel);
                        }
                        offset = offset + limit;
                        searchResultAdapter.notifyDataSetChanged();
                        searchResultListing.setVisibility(View.VISIBLE);
                        errorText.setVisibility(View.GONE);
                        if (searchList.size() > 0) {
                            searchResultListing.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                            searchResultAdapter.notifyDataSetChanged();
                        } else {
                            searchResultListing.setVisibility(View.GONE);
                            errorText.setVisibility(View.VISIBLE);
                            errorText.setText(res.getString("message"));
                        }

                    } else {
                        if (offset == 0) {
                            errorText.setText(res.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                            searchResultListing.setVisibility(View.GONE);
                        } else {
                            searchResultListing.setVisibility(View.VISIBLE);
                            AlertUtils.showSnack(
                                    SearchActivity.this, errorText, " No more notifications");
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("userSearchProfileTask ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userSearchProfileTask error ex", e.getMessage() + "");
                }
                errorText.setText(errMsg);
                errorText.setVisibility(View.VISIBLE);
                searchResultListing.setVisibility(View.GONE);
            }

        }, "userSearchProfileTask");

    }

    @Override
    public void onBackPressed() {
        VolleyHelper.getInstance(this).cancelRequest("userSearchProfileTask");
        AlertUtils.hideInputMethod(this, searchBar);
        super.onBackPressed();
    }
}
