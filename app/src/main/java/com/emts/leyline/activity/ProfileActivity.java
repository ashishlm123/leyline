package com.emts.leyline.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.EventAdapter;
import com.emts.leyline.customviews.CustomViewPager;
import com.emts.leyline.fragments.EventDetailsFragment;
import com.emts.leyline.fragments.FriendsFragment;
import com.emts.leyline.fragments.PrivateFrag;
import com.emts.leyline.fragments.UserDetailsFragment;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.ChatThread;
import com.emts.leyline.models.FriendsModel;
import com.emts.leyline.models.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.Gravity.CENTER;
import static com.emts.leyline.helper.Config.EVERYONE;
import static com.emts.leyline.helper.Config.FRIENDS;

public class ProfileActivity extends AppCompatActivity {
    PreferenceHelper helper;
    LinearLayout sendFriendResHolder, cancelFriendReqHolder, friendHolder, respondToReq;
    TextView tvSent, userName, userEmail;
    UserModel userProfile;
    ImageView profilePic, chatList, coverPic;
    ViewPagerAdapter adapter;
    Toolbar toolbar;
    String threadId = "0";
    RelativeLayout relativeLayout;
    ProgressBar progressBar;
    TextView errorText;
    TabLayout tabLayout;
    ViewPager viewPager;
    Bundle userBundle;
    String profilePrivacy = "";
    String eventPrivacy = "";
    String friendsPrivacy = "";
    boolean isFriend = false;

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public static void openProfile(Context context, UserModel userModel, String userThreadId) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra("user", userModel);
        intent.putExtra("threadId", userThreadId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_profile);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.inflateMenu(R.menu.profile_edit_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.edit_profile) {
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent);
                } else if (item.getItemId() == R.id.search_profile) {
                    Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });

        Intent intent = getIntent();
        if (intent == null) {
            return;
        } else {
            userProfile = (UserModel) intent.getSerializableExtra("user");
            threadId = intent.getStringExtra("threadId");
        }
        if (userProfile == null) {
            AlertUtils.showToast(this, "Give me usermodel in intent --> user");
            return;
        }

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                MenuItem item = toolbar.getMenu().findItem(R.id.edit_profile);
                if (item != null) {
                    if (userProfile.getUserId().equals(helper.getUserId())) {
                        item.setVisible(true);
                    } else {
                        item.setVisible(false);
                    }
                }
            }
        });

        relativeLayout = findViewById(R.id.relative_layout);
        progressBar = findViewById(R.id.progress_bar);
        errorText = findViewById(R.id.error_text);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);

        userName = findViewById(R.id.full_name);
        userEmail = findViewById(R.id.email_address);
        sendFriendResHolder = findViewById(R.id.btn_send_request);
        cancelFriendReqHolder = findViewById(R.id.btn_cancel_request);
        friendHolder = findViewById(R.id.btn_friends);
        respondToReq = findViewById(R.id.respond_to_request);
        profilePic = findViewById(R.id.profile_pic);
        coverPic = findViewById(R.id.cover_pic);
        tvSent = findViewById(R.id.tv_sent);
        chatList = findViewById(R.id.img_chat);
        chatList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.e("threadId here", threadId + " ");
                if (TextUtils.isEmpty(threadId) || threadId.equals("0") || threadId.equals("null")) {
                    createThreadTask(userProfile);
                } else {
                    Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                    ChatThread thread = new ChatThread();
                    thread.setSentBy(userProfile);
                    thread.setThreadId(threadId);
                    intent.putExtra("chat_thread", thread);
                    startActivity(intent);
                }

            }
        });

        setUserInfo(userProfile);

        cancelFriendReqHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(ProfileActivity.this)) {
                    getFriendStatusTask(FriendsModel.CANCEL_REQUEST, userProfile.getUserId());
                } else {
                    AlertUtils.showToast(ProfileActivity.this, String.valueOf(R.string.no_internet));
                }
            }
        });

        respondToReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRespondPopUp();
            }
        });

        if (NetworkUtils.isInNetwork(ProfileActivity.this)) {
            userDetailsTask();
        } else {
            AlertUtils.showToast(ProfileActivity.this, getResources().getString(R.string.no_internet));
        }
    }

    private void userDetailsTask() {
        relativeLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        tabLayout.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(ProfileActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("profile_id", userProfile.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserProfile, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        userBundle = new Bundle();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject user = res.getJSONObject("user_details");
                                UserModel userModel = new UserModel();
                                userModel.setUserName(user.getString("name"));
                                userModel.setUserId(user.getString("id"));
                                userModel.seteMail(user.getString("email"));
                                if (user.getString("image").equals("") || user.getString("image").equals("null")) {
                                    userModel.setImgUrl("");
                                } else {
                                    userModel.setImgUrl(res.getString("profileimage_default_url") + user.getString("image"));
                                }

                                if (user.getString("cover_image").equals("") || user.getString("cover_image").equals("null")) {
                                    userModel.setCoverImgUrl("");
                                } else {
                                    userModel.setImgUrl(res.getString("coverimage_default_url") + user.getString("cover_image"));
                                }

                                userBundle.putString("dob", user.getString("dob_year") + "/"
                                        + user.getString("dob_month") + "/" + user.getString("dob_day"));
                                userBundle.putString("gender", user.getString("gender").equals("2") ? "F" : "M");
                                userBundle.putString("hometown", user.getString("city"));
                                String bio = user.getString("user_bio");
                                userBundle.putString("bio", bio + "");

                                String website = user.getString("website_url");
                                userBundle.putString("website", website);

                                setThreadId(user.optString("thread_id"));

                                if (!helper.getUserId().equals(userProfile.getUserId())) {
                                    profilePrivacy = user.getString("profile_privacy");
                                    if (profilePrivacy == null) {
                                        profilePrivacy = "null";
                                    }
                                    friendsPrivacy = user.getString("friend_privacy");
                                    if (friendsPrivacy == null) {
                                        friendsPrivacy = "null";
                                    }
                                    eventPrivacy = user.getString("event_privacy");
                                    if (eventPrivacy == null) {
                                        eventPrivacy = "null";
                                    }

                                    isFriend = user.getString("friend_status").equals("isfriend");
                                }

                                ArrayList<String> interestList = new ArrayList<>();
                                JSONArray interestArray = user.getJSONArray("interest");
                                StringBuilder interestConcat = new StringBuilder();
                                for (int i = 0; i < interestArray.length(); i++) {
                                    JSONObject interestObject = interestArray.getJSONObject(i);
                                    interestList.add(interestObject.getString("interest_title"));
                                    interestConcat.append(interestObject.getString("interest_title"))
                                            .append(SettingsActivity.INTEREST_CONCAT_TEXT);
                                }
                                if (helper.getUserId().equals(userProfile.getUserId())) {
                                    //its me save mine
                                    if (!TextUtils.isEmpty(interestConcat.toString())) {
                                        helper.edit().putString(PreferenceHelper.APP_USER_INTERESTS, interestConcat.toString()).commit();
                                        Logger.e("k xa ta intrest not na",PreferenceHelper.APP_USER_INTERESTS );
                                    }
                                }
                                userBundle.putStringArrayList("interestList", interestList);

                                userBundle.putFloat("rating", (float) Math.ceil(Float.parseFloat(res.getString("user_rating"))));

                                if (!userModel.getUserId().equals(helper.getUserId())) {
                                    changeBtn(user.optString("friend_status"), userModel);
                                }

                                progressBar.setVisibility(View.GONE);
                                errorText.setVisibility(View.GONE);
                                relativeLayout.setVisibility(View.GONE);
                                setupViewPager(viewPager);
//                                tabLayout.setVisibility(View.VISIBLE);
                                viewPager.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("getUserDetailsTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getUserDetailsTask error ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(errMsg);
                        errorText.setVisibility(View.VISIBLE);
                        tabLayout.setVisibility(View.GONE);
                        viewPager.setVisibility(View.GONE);
                        relativeLayout.setVisibility(View.VISIBLE);
                    }
                }, "getUserDetailsTask");
    }

    public void setUserInfo(final UserModel userModel) {
        if (userModel.getUserId().equals(helper.getUserId())) {
            userName.setText(helper.getUserName());
            userEmail.setText(helper.getUserEmail());
            if (!TextUtils.isEmpty(helper.getString(PreferenceHelper.APP_USER_PIC, ""))) {
                Picasso.with(ProfileActivity.this).load(helper.getString(PreferenceHelper.APP_USER_PIC, "")).into(profilePic);
            }
            Logger.e("coverImg", helper.getString(PreferenceHelper.APP_USER_COVER_PIC, ""));
            if (!TextUtils.isEmpty(helper.getString(PreferenceHelper.APP_USER_COVER_PIC, ""))) {
                Picasso.with(ProfileActivity.this).load(helper.getString(PreferenceHelper.APP_USER_COVER_PIC, "")).into(coverPic);
            }
            sendFriendResHolder.setVisibility(View.GONE);
            cancelFriendReqHolder.setVisibility(View.GONE);
        } else {
            userName.setText(userModel.getUserName());
            if (!TextUtils.isEmpty(userModel.getImgUrl())) {
                Picasso.with(ProfileActivity.this).load(userModel.getImgUrl()).into(profilePic);
            }
        }

        sendFriendResHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(ProfileActivity.this)) {
                    getFriendStatusTask(FriendsModel.SENT, userModel.getUserId());
                } else {
                    AlertUtils.showToast(ProfileActivity.this, String.valueOf(R.string.no_internet));
                }
            }
        });
    }

    PopupWindow popupWindow;

    private void showRespondPopUp() {
        // initialize a pop up window type
        popupWindow = new PopupWindow(this);

        ArrayList<String> sortList = new ArrayList<>();
        sortList.add("Accept Request");
        sortList.add("Cancel Request");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.item_popup, sortList);
        // the drop down list is a list view
        ListView listViewSort = new ListView(this);

        // set our adapter and pass our pop up window contents
        listViewSort.setAdapter(adapter);

        // set on item selected
        listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                popupWindow.dismiss();
                if (i == 0) {
                    //accept friend
                    getFriendStatusTask(FriendsModel.ACCEPT, userProfile.getUserId());
                } else if (i == 1) {
                    //reject friend
                    getFriendStatusTask(FriendsModel.CANCEL_REQUEST, userProfile.getUserId());
                }
            }
        });

        // some other visual settings for popup window
        popupWindow.setFocusable(true);
        popupWindow.setWidth(600);
        // popupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.white));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the listview as popup content
        popupWindow.setContentView(listViewSort);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        popupWindow.showAsDropDown(respondToReq, 0, 0); // show popup like dropdown list
    }

    private void getFriendStatusTask(final String request, String friendId) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(ProfileActivity.this, "Please wait...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("friend_id", friendId);
        postMap.put("type", request);

        vHelper.addVolleyRequestListeners(Api.getInstance().friend_ManagementUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
//                                if (FriendsModel.SENT.equals(request)) {
//                                    cancelFriendReqHolder.setVisibility(View.VISIBLE);
//                                    sendFriendResHolder.setVisibility(View.GONE);
//                                } else if (FriendsModel.CANCEL_REQUEST.equals(request)) {
//                                    cancelFriendReqHolder.setVisibility(View.GONE);
//                                    sendFriendResHolder.setVisibility(View.VISIBLE);
//                                }
                                changeBtn(request, userProfile);
                                if (adapter != null) {
                                    Fragment fragment = adapter.getItem(2);
                                    if (fragment != null && fragment instanceof FriendsFragment) {
                                        ((FriendsFragment) fragment).userFriendsListingTask();
                                    }
                                }
                            } else {
                                AlertUtils.showErrorAlert(ProfileActivity.this, res.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            Logger.e("getFriendStatusTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("getFriendStatusTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(ProfileActivity.this, "Error !!!", errMsg);
                    }
                }, "getFriendStatusTask");
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (helper.getUserId().equals(userProfile.getUserId())) {
            //its me cant i view my own.??
            showUserDetailsFragment(true);
            showEventDetailsFragment(true);
            showFriendDetailsFragment(true);

            viewPager.setAdapter(adapter);
            tabLayout.setVisibility(View.VISIBLE);
            return;
        }

        boolean isUserPvt = false, isEventPvt = false, isFrnPvt = false;

        if (profilePrivacy.equals(EVERYONE) || profilePrivacy.equals("null")
                || (profilePrivacy.equals(FRIENDS) && isFriend)) {
            showUserDetailsFragment(true);
            Logger.e("profilePrivacy", profilePrivacy);
        } else {
            isUserPvt = true;
            showUserDetailsFragment(false);
        }

        if (eventPrivacy.equals(EVERYONE) || (eventPrivacy.equals(FRIENDS) && isFriend)
                || (eventPrivacy.equals("null") && isFriend)) {
            showEventDetailsFragment(true);
            Logger.e("eventPrivacy", eventPrivacy);
        } else {
            isEventPvt = true;
            showEventDetailsFragment(false);
        }

        if (friendsPrivacy.equals(EVERYONE) || (friendsPrivacy.equals(FRIENDS) && isFriend) ||
                (friendsPrivacy.equals("null") && isFriend)) {
            showFriendDetailsFragment(true);
            Logger.e("friendsPrivacy", friendsPrivacy);
        } else {
            isFrnPvt = true;
            showFriendDetailsFragment(false);
        }

        if (isUserPvt && isEventPvt && isFrnPvt) {
            errorText.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.VISIBLE);
            errorText.setText("This profile is private.");
            tabLayout.setVisibility(View.GONE);
        } else {
            viewPager.setAdapter(adapter);
            tabLayout.setVisibility(View.VISIBLE);

            if (isUserPvt) {
                disableTab(0);
                if (isEventPvt) {
                    viewPager.setCurrentItem(2);
                } else {
                    viewPager.setCurrentItem(1);
                }
            }
            if (isEventPvt) {
                disableTab(1);
            }
            if (isFrnPvt) {
                disableTab(2);
            }

            //dissable swipe
            if (!isUserPvt && (isEventPvt && isFrnPvt)) {
                ((CustomViewPager) viewPager).setPagingEnabled(false);
            }
//            else{
//                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                    @Override
//                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                    }
//
//                    @Override
//                    public void onPageSelected(int position) {
//
//                    }
//
//                    @Override
//                    public void onPageScrollStateChanged(int state) {
//
//                    }
//                });
//            }
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void disableTab(int index) {
        TabLayout.Tab tab = tabLayout.getTabAt(index);
        if (tab == null) {
            Logger.e("DISSABLE TAB", "Tab here is null");
            return;
        }
        //custom tab layout
        TextView tabOne = new TextView(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        lp.gravity = CENTER;
        tabOne.setLayoutParams(lp);
        tabOne.setGravity(CENTER);
        tabOne.setText(tab.getText().toString() + " ");
        tabOne.setTextColor(getResources().getColor(R.color.disableTab));
        tabOne.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        tab.setCustomView(tabOne);
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
//            Fragment fragment = (Fragment) object;
//            if (fragment.toString().equals("Analytics")) {
//                return 1;
//            } else {
//                return POSITION_NONE;
//            }
            return POSITION_NONE;
            //return POSITION_NONE for those items which has changed and
            // return POSITION_UNCHANGED for those that haven't change
        }
    }

    public void changeBtn(String friendStatus, UserModel userModel) {
        setUserInfo(userModel);
        if (FriendsModel.SEND.equalsIgnoreCase(friendStatus) || FriendsModel.SENT.equalsIgnoreCase(friendStatus)) {
            cancelFriendReqHolder.setVisibility(View.VISIBLE);
            sendFriendResHolder.setVisibility(View.GONE);
            friendHolder.setVisibility(View.GONE);
            respondToReq.setVisibility(View.GONE);
            chatList.setVisibility(View.GONE);
        } else if (FriendsModel.CANCEL_REQUEST.equalsIgnoreCase(friendStatus)) {
            cancelFriendReqHolder.setVisibility(View.GONE);
            sendFriendResHolder.setVisibility(View.VISIBLE);
            friendHolder.setVisibility(View.GONE);
            respondToReq.setVisibility(View.GONE);
            chatList.setVisibility(View.GONE);
        } else if (FriendsModel.FRIEND.equalsIgnoreCase(friendStatus) || FriendsModel.ACCEPT.equalsIgnoreCase(friendStatus)) {
            friendHolder.setVisibility(View.VISIBLE);
            cancelFriendReqHolder.setVisibility(View.GONE);
            sendFriendResHolder.setVisibility(View.GONE);
            respondToReq.setVisibility(View.GONE);
            chatList.setVisibility(View.VISIBLE);
        } else if (FriendsModel.RECEIVE.equalsIgnoreCase(friendStatus)) {
            friendHolder.setVisibility(View.GONE);
            cancelFriendReqHolder.setVisibility(View.GONE);
            sendFriendResHolder.setVisibility(View.GONE);
            respondToReq.setVisibility(View.VISIBLE);
            chatList.setVisibility(View.GONE);
        } else if (FriendsModel.NONE.equalsIgnoreCase(friendStatus)) {
            friendHolder.setVisibility(View.GONE);
            cancelFriendReqHolder.setVisibility(View.GONE);
            sendFriendResHolder.setVisibility(View.VISIBLE);
            respondToReq.setVisibility(View.GONE);
            chatList.setVisibility(View.GONE);
        }
    }

    private void createThreadTask(final UserModel userModel) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(ProfileActivity.this, "Please wait...");
        progressDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(ProfileActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("receiver_id", userModel.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().createThreadApi, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent(getApplication(), ChatActivity.class);
                                ChatThread thread = new ChatThread();
                                thread.setSentBy(userModel);
                                threadId = res.getString("thread_id");
                                thread.setThreadId(threadId);
                                intent.putExtra("chat_thread", thread);
                                startActivity(intent);
                            } else {
                                AlertUtils.showErrorAlert(ProfileActivity.this, res.getString("message"), null);
                            }
                        } catch (JSONException e) {
                            Logger.e("createThreadTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressDialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("createThreadTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(ProfileActivity.this, "Error !!!", errMsg);
                    }
                }, "createThreadTask");
    }

    @Override
    public void onBackPressed() {
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("getFriendStatusTask");
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("userUserEventDetailsTask");
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("userFriendsListingTask");
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("userDetailsTask");
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("getFriendStatusTask");
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("userUserEventDetailsTask");
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("userFriendsListingTask");
        VolleyHelper.getInstance(ProfileActivity.this).cancelRequest("userDetailsTask");
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EventAdapter.CREATED_EVENTS || requestCode == EventAdapter.ATTENDING_EVENTS
                    || requestCode == EventAdapter.ATTENDED_EVENTS) {

                if (adapter == null) {
                    return;
                }
                Fragment fragment = adapter.getItem(1);
                if (fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

        }
    }

    private void showUserDetailsFragment(boolean isShow) {
        if (!isShow) {
            adapter.addFrag(showEmptyFragment("User details are private"), "User Details");
            return;
        }
        UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("prof_id", userProfile.getUserId());
        bundle.putString("profilePrivacy", profilePrivacy);
        if (userBundle != null) {
            bundle.putString("dob", userBundle.getString("dob"));
            bundle.putString("gender", userBundle.getString("gender"));
            bundle.putString("hometown", userBundle.getString("city"));
            bundle.putFloat("rating", userBundle.getFloat("rating"));
            bundle.putStringArrayList("interestList", userBundle.getStringArrayList("interestList"));
            bundle.putString("bio", userBundle.getString("bio"));
        }
        userDetailsFragment.setArguments(bundle);
        adapter.addFrag(userDetailsFragment, "User Details");
    }

    private void showEventDetailsFragment(boolean isShow) {
        if (!isShow) {
            adapter.addFrag(showEmptyFragment(getString(R.string.event_private_msg)), getString(R.string.event_details));
            return;
        }
        EventDetailsFragment eventDetailsFragment = new EventDetailsFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("prof_id", userProfile.getUserId());
        bundle1.putString("eventPrivacy", eventPrivacy);
        eventDetailsFragment.setArguments(bundle1);
        adapter.addFrag(eventDetailsFragment, "My Shows");
    }

    private Fragment showEmptyFragment(String message) {
        Fragment fragment = new PrivateFrag();
        Bundle bundle = new Bundle();
        bundle.putString("msg", message);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void showFriendDetailsFragment(boolean isShow) {
        if (!isShow) {
            adapter.addFrag(showEmptyFragment("User friend details are private"), "Friends");
            return;
        }
        FriendsFragment friendsFragment = new FriendsFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("prof_id", userProfile.getUserId());
        bundle2.putString("friendsPrivacy", friendsPrivacy);
        friendsFragment.setArguments(bundle2);
        adapter.addFrag(friendsFragment, "Friends");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (helper.getUserId().equals(userProfile.getUserId())) {
            userProfile.setUserName(helper.getUserName());
            userProfile.seteMail(helper.getUserEmail());
            setUserInfo(userProfile);
        }
    }
}
