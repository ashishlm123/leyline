package com.emts.leyline.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.CalEventAdapter;
import com.emts.leyline.fragments.CalendarFrag;
import com.emts.leyline.fragments.CalenderFragOld;
import com.emts.leyline.fragments.EventDetailsFragment;
import com.emts.leyline.fragments.EventsNearMeFrag;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.ImageHelper;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.Utils;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.UserModel;
import com.google.android.gms.maps.MapsInitializer;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    FragmentManager fm;
    Integer test = 0;
    PreferenceHelper prefsHelper;
    public static Uri fileUri = null;
    NavigationView navigationView;
    View views;
    MenuItem mPreviousMenuItem;
    String selectedImagePath, selectedCoverImagePath;
    Toolbar toolbar;
    TextView fullName, eMail, editProfile;
    ImageView profilePic, edtProfilePic, coverPic;
    boolean isCoverUpload;
    RelativeLayout edtCoverPic;
    public static int OPEN_FEED = 1234;
    public static int INTENT_ADD_EVENT = 3412;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);

        MapsInitializer.initialize(this);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("LeyLine");

        toolbar.inflateMenu(R.menu.add_event_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.add_event) {
                    Intent intent = new Intent(getApplicationContext(), AddEventActivity.class);
                    startActivityForResult(intent, INTENT_ADD_EVENT);
                } else if (item.getItemId() == R.id.my_notifications) {
                    Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });
        fm = getSupportFragmentManager();
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.inflateMenu(R.menu.activity_main_drawer);

        views = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);

//        setHeaderData();

        //default home fragment
        fm.beginTransaction().add(R.id.content_main, new EventsNearMeFrag(), getString(R.string.event_nearby)).commit();
        setTitle();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment calMayBe = fm.findFragmentById(R.id.content_main);
            if (calMayBe instanceof CalendarFrag) {
                if (((CalendarFrag) calMayBe).onBackPressed())
                    super.onBackPressed();
            } else if (calMayBe instanceof EventsNearMeFrag) {
                if (((EventsNearMeFrag) calMayBe).onBackPressed())
                    super.onBackPressed();
            } else {
                super.onBackPressed();
            }
        }
        setTitle();
    }

    public void openEventNearMe() {
        fm.beginTransaction().add(R.id.content_main, new EventsNearMeFrag(), getString(R.string.event_nearby)).commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        item.setCheckable(true);
        item.setChecked(true);
        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false);
        }
        mPreviousMenuItem = item;

        int id = item.getItemId();
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        Intent intent;
        switch (id) {
            case R.id.nav_events_near_me:
                fragment = new EventsNearMeFrag();
                break;
            case R.id.nav_calender:
//                fragment = new CalenderFragOld();
                fragment = new CalendarFrag();
                break;
            case R.id.nav_my_profile:
                intent = new Intent(getApplicationContext(), ProfileActivity.class);
                UserModel userModel = new UserModel();
                userModel.setUserId(prefsHelper.getUserId());
                userModel.setUserName(prefsHelper.getUserName());
                userModel.setImgUrl(prefsHelper.getString(PreferenceHelper.APP_USER_PIC, ""));
                intent.putExtra("user", userModel);
                startActivity(intent);
                break;
            case R.id.nav_my_events:
                fragment = new EventDetailsFragment();
               String isFromMainActivity = "1";
                Bundle bundle1 = new Bundle();
                bundle1.putString("prof_id", prefsHelper.getUserId());
                fragment.setArguments(bundle1);
                Intent intent1 = new Intent();

                break;
            case R.id.nav_my_notifications:
                intent = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_chat:
                intent = new Intent(getApplicationContext(), ChatListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_feeds:
                intent = new Intent(getApplicationContext(), FeedsActivity.class);
                startActivityForResult(intent, OPEN_FEED);
                break;
            case R.id.nav_settings:
                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_logout:
                prefsHelper.edit().clear().commit();
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;

        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_main, fragment)
                    .addToBackStack(null)
                    .commit();
            drawer.closeDrawer(GravityCompat.START);
        }

        setTitle();

        return true;
    }

    public void setTitle() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Fragment fragment = fm.findFragmentById(R.id.content_main);
                    if (fragment instanceof EventsNearMeFrag) {
                        toolbar.setTitle(getString(R.string.event_nearby));
                    } else if (fragment instanceof CalenderFragOld || fragment instanceof CalendarFrag) {
                        toolbar.setTitle("Calendar");
                    } else if (fragment instanceof EventDetailsFragment) {
                        toolbar.setTitle("My Shows");
                    } else {
                        toolbar.setTitle(getString(R.string.app_name));
                    }
                } catch (Exception e) {
                }
            }
        }, 200);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AlertUtils.hideInputMethod(this, views);
        setHeaderData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Fragment fragment = fm.findFragmentById(R.id.content_main);
            if (requestCode == EventsNearMeFrag.EVENT_NEAR_ME || requestCode == OPEN_FEED) {
                if (fragment instanceof EventsNearMeFrag) {
                    ((EventsNearMeFrag) fragment).onActivityResult(requestCode, resultCode, data);
                }
            } else if (requestCode == CalEventAdapter.CAL_EVENT) {
                if (fragment instanceof CalendarFrag) {
                    ((CalendarFrag) fragment).onActivityResult(requestCode, resultCode, data);
                }
            } else if (requestCode == RegisterActivity.RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);

                if (test == 1) {
                    CropImage.activity(selectedImageUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(16, 9)
                            .start(MainActivity.this);
                    test = 0;
                } else {
                    CropImage.activity(selectedImageUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(1, 1)
                            .start(MainActivity.this);
                }


            } else if (requestCode == RegisterActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (fileUri == null) {
                    return;
                }
                if (test == 1) {
                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(16, 9)
                            .start(MainActivity.this);
                    test = 0;
                } else {
                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(1, 1)
                            .start(MainActivity.this);
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                final Uri resultUri = result.getUri();
                if (isCoverUpload) {
                    selectedCoverImagePath = resultUri.getPath();
                    Logger.e("selected cover image", selectedCoverImagePath);
                } else {
                    selectedImagePath = resultUri.getPath();
                    Logger.e("selected image", selectedImagePath);
                }

                if (resultUri.getPath() != null) {
                    if (NetworkUtils.isInNetwork(MainActivity.this)) {
                        if (isCoverUpload) {
                            userCoverPicUploadTask(selectedCoverImagePath);
                        } else {
                            userProfilePicUploadTask(selectedImagePath);
                        }
                    } else {
                        AlertUtils.showToast(MainActivity.this, "No Internet Connection...");
                    }
                }
            } else if (requestCode == INTENT_ADD_EVENT) {
                Fragment fragment1 = fm.findFragmentById(R.id.content_main);
                if (fragment1 instanceof EventsNearMeFrag) {
                    fragment1.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }


    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Upload Profile Picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RegisterActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RegisterActivity.RESULT_LOAD_IMAGE);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(boolean video) {
        Logger.e("choose from gallery ma pugis", "ah yaar");
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RegisterActivity.RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RegisterActivity.RESULT_LOAD_IMAGE);
        }
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(RegisterActivity.MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, RegisterActivity.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_leyline");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_leyline" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == RegisterActivity.MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == RegisterActivity.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    private void correctOrientation(String path) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(path, opts);

        int rotationAngle = getCameraPhotoOrientation(MainActivity.this, fileUri, fileUri.getPath());

        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Logger.e("fileEx", e.getMessage());
        } catch (IOException e) {
            Logger.e("ioe Ex", e.getMessage());
        }

    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = 0;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }


    private void userProfilePicUploadTask(final String selectedImagePath) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        String encodedImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                (int) Utils.pxFromDp(MainActivity.this, 100f), (int) Utils.pxFromDp(MainActivity.this, 100f)));

        postMap.put("user_id", prefsHelper.getUserId());
        postMap.put("image", encodedImage);

        vHelper.addVolleyRequestListeners(Api.getInstance().userProfilePicUpdateUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
                        String profileImg = res.getString("profileimage_default_url") + res.getString("profile_image");
                        Picasso.with(MainActivity.this).load(profileImg).into(profilePic);
                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_PIC, profileImg).commit();
                        AlertUtils.showToast(MainActivity.this, res.getString("message"));

//                            }
//                        }, 200);
                    } else {
                        AlertUtils.showErrorAlert(MainActivity.this, res.getString("message"), null);
                    }
                } catch (JSONException e) {
                    Logger.e("userProfilePicUploadTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userProfilePicUploadTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(MainActivity.this, "Error !!!", errMsg);
            }
        }, "userProfilePicUploadTask");

    }

    private void userCoverPicUploadTask(final String selectedCoverImagePath) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        String encodedCoverImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(selectedCoverImagePath,
                (int) Utils.pxFromDp(MainActivity.this, 100f), (int) Utils.pxFromDp(MainActivity.this, 100f)));

        postMap.put("user_id", prefsHelper.getUserId());
        postMap.put("cover_image", encodedCoverImage);

        vHelper.addVolleyRequestListeners(Api.getInstance().userCoverPicUpdateurl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
                        String coverPicImg = res.getString("coverimage_default_url") + res.getString("cover_image");
                        Picasso.with(MainActivity.this).load(coverPicImg).into(coverPic);
                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_COVER_PIC, coverPicImg).commit();
                        AlertUtils.showToast(MainActivity.this, res.getString("message"));

//                            }
//                        }, 200);
                    } else {
                        AlertUtils.showErrorAlert(MainActivity.this, res.getString("message"), null);
                    }
                } catch (JSONException e) {
                    Logger.e("userProfilePicUploadTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userProfilePicUploadTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(MainActivity.this, "Error !!!", errMsg);
            }
        }, "userProfilePicUploadTask");

    }

    public void gotoProfile() {
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        UserModel userModel = new UserModel();
        userModel.setUserId(prefsHelper.getUserId());
        userModel.setUserName(prefsHelper.getUserName());
        userModel.setImgUrl(prefsHelper.getString(PreferenceHelper.APP_USER_PIC, ""));
        intent.putExtra("user", userModel);
        startActivity(intent);
    }


    public void setHeaderData() {
        RelativeLayout mainHeader = views.findViewById(R.id.l1);
        if (prefsHelper.isLogin()) {
            mainHeader.setVisibility(View.VISIBLE);
            fullName = views.findViewById(R.id.full_name);
            eMail = views.findViewById(R.id.email_address);
            fullName.setText(prefsHelper.getUserName());
            eMail.setText(prefsHelper.getUserEmail());

            edtProfilePic = views.findViewById(R.id.edt_profile_pic);
            edtProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCoverUpload = false;
                    alertForPhoto();
                }
            });

            edtCoverPic = views.findViewById(R.id.edit_cover_pic);
            edtCoverPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCoverUpload = true;
                    test = 1;
                    alertForPhoto();
                }
            });

            editProfile = views.findViewById(R.id.edit_profile);
            editProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent);
                }
            });

            profilePic = views.findViewById(R.id.profile_pic);
            if (!TextUtils.isEmpty(prefsHelper.getString(PreferenceHelper.APP_USER_PIC, ""))) {
                Picasso.with(MainActivity.this).load(prefsHelper.getString(PreferenceHelper.APP_USER_PIC, "")).into(profilePic);
            }

            coverPic = views.findViewById(R.id.cover_pic);
            if (!TextUtils.isEmpty(prefsHelper.getString(PreferenceHelper.APP_USER_COVER_PIC, ""))) {
                Picasso.with(MainActivity.this).load(prefsHelper.getString(PreferenceHelper.APP_USER_COVER_PIC, "")).into(coverPic);
            }

            fullName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoProfile();
                }
            });

            eMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gotoProfile();
                }
            });

            profilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertForPhoto();
                }
            });


        }
    }
}
