package com.emts.leyline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 10/12/2017.
 */

public class ContactUsActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText edtName, edtEmail, edtMessage;
    TextView errorName, errorEmail, errorMessage;
    Button btnSubmit;
    PreferenceHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_contact_us);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Contact Us");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        init();

        edtEmail.setText(helper.getUserEmail());
        edtEmail.setEnabled(false);

        edtName.setText(helper.getUserName());
        edtName.setEnabled(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validation()) {
                    if (NetworkUtils.isInNetwork(ContactUsActivity.this)) {
                        contactUsTaskUrl();
                    } else {
                        AlertUtils.showSnack(ContactUsActivity.this, errorEmail, String.valueOf(R.string.no_internet));
                    }
                }
            }
        });

    }

    private void contactUsTaskUrl() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ContactUsActivity.this, "Please wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", helper.getUserId());
        postMap.put("email", helper.getUserEmail());
        postMap.put("message", edtMessage.getText().toString());

        vHelper.addVolleyRequestListeners(Api.getInstance().userSupportUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showErrorAlert(ContactUsActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                onBackPressed();
                            }
                        });
                    } else {
                        AlertUtils.showErrorAlert(ContactUsActivity.this, res.getString("message"), null);
                    }
                } catch (JSONException e) {
                    Logger.e("contactUsTaskUrl excep", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("contactUsTaskUrl error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(ContactUsActivity.this, "Error !!!", errMsg);
            }
        }, "contactUsTaskUrl");
    }

    private void init() {
        edtName = (EditText) findViewById(R.id.edt_name);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtMessage = (EditText) findViewById(R.id.edt_message);
        errorName = (TextView) findViewById(R.id.error_name);
        errorEmail = (TextView) findViewById(R.id.error_email);
        errorMessage = (TextView) findViewById(R.id.error_message);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
    }

    public boolean Validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(edtName.getText().toString().trim())) {
            errorName.setText(R.string.txt_req);
            errorName.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            errorName.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            errorEmail.setText(R.string.txt_req);
            isValid = false;
            errorEmail.setVisibility(View.VISIBLE);
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches()) {
                errorEmail.setVisibility(View.GONE);
            } else {
                isValid = false;
                errorEmail.setText("Invalid email address.");
                errorEmail.setVisibility(View.VISIBLE);
            }
        }

        if (TextUtils.isEmpty(edtMessage.getText().toString().trim())) {
            errorMessage.setText(R.string.txt_req);
            isValid = false;
            errorMessage.setVisibility(View.VISIBLE);
        } else {
            errorMessage.setVisibility(View.GONE);
        }
        return isValid;
    }
}
