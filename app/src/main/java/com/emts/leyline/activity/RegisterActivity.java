package com.emts.leyline.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.ImageHelper;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.Utils;
import com.emts.leyline.helper.VolleyHelper;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {
    TextView errPass, errEm, errUserName, errDOB, errPhoto, errorGender, errorHometown, errorZipCode;
    ImageView receiptImage;
    Button signUp;
    EditText name, eMail, passWord, edtDob, etHometown, etZipcode;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    PreferenceHelper helper;

    String selectedImagePath;
    public static Uri fileUri = null;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int RESULT_LOAD_VIDEO = 124;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;
    public static final int IMAGE_SELECTED = 789;

    private int day, month, year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_register);

        init();

        TextView takeUploadPhoto = (TextView) findViewById(R.id.take_upload_photo);
        takeUploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto();
            }
        });

        receiptImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto();
            }
        });


        edtDob.setInputType(InputType.TYPE_NULL);
        edtDob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    Logger.e("hello", "hello");
                    datePicker();
                }
                return true;
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(RegisterActivity.this)) {
                        userSignUpTask2();
                    } else {
                        AlertUtils.showSnack(RegisterActivity.this, view, "No internet connection...");
                    }
                } else {
                    AlertUtils.showToast(RegisterActivity.this, "Please validate the form...");
                }
            }
        });
    }


    private void userSignUpTask2() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(RegisterActivity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("name", name.getText().toString().trim());
        postMap.put("email", eMail.getText().toString().trim());
        postMap.put("password", passWord.getText().toString().trim());
        postMap.put("dob_day", String.format("%02d", day));
        postMap.put("dob_month", String.format("%02d", month));
        postMap.put("dob_year", String.format("%02d", year));

        postMap.put("gender", rgGender.getCheckedRadioButtonId() == R.id.rbMale ? "M" : "F");
        postMap.put("hometown", etHometown.getText().toString().trim());
        postMap.put("zipcode", etZipcode.getText().toString().trim());

        Logger.e("usersignuptask 2 post form part", postMap.toString());

        String encodedImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                (int) Utils.pxFromDp(RegisterActivity.this, 300f), (int) Utils.pxFromDp(RegisterActivity.this, 300f)));

        postMap.put("image", encodedImage);
        Logger.printLongLog("image", encodedImage);

        vHelper.addVolleyRequestListeners(Api.getInstance().registerUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (res.getString("login").equals("yes")) {
                                    JSONObject user = res.getJSONObject("user_detail");
                                    helper.edit().putString(PreferenceHelper.TOKEN, user.getString("token")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_NAME, user.getString("name")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, user.getString("email")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_ID, user.getString("id")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_PIC, res.getString("profile_img_Url")).commit();
                                    helper.edit().putString(PreferenceHelper.APP_USER_DOB, user.getString("dob_day") + "/"
                                            + user.getString("dob_month") + "/" + user.getString("dob_year")).commit();
                                    if (user.getString("user_bio").equals("null")) {
                                        helper.edit().putString(PreferenceHelper.APP_USER_BIO, "").commit();
                                    } else {
                                        helper.edit().putString(PreferenceHelper.APP_USER_BIO, user.getString("user_bio")).commit();
                                    }
                                    helper.edit().putString(PreferenceHelper.APP_USER_WEBSITE, "").commit();
                                    String gender = user.optString("gender");
                                    gender = gender.equals("2") ? "F" : "M";
                                    helper.edit().putString(PreferenceHelper.USER_GENDER, gender).commit();
                                    helper.edit().putString(PreferenceHelper.USER_HOME_TOWN, user.optString("city")).commit();
                                    helper.edit().putString(PreferenceHelper.USER_ZIP_CODE, user.optString("zipcode")).commit();

                                    helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();

                                    if (user.getString("profile_privacy").equals("1")) {
                                        helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "everyone").commit();
                                    } else if (user.getString("profile_privacy").equals("2")) {
                                        helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "friends").commit();
                                    } else if (user.getString("profile_privacy").equals("3")) {
                                        helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "only me").commit();
                                    } else {
                                        helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "everyone").commit();
                                    }

                                    if (user.getString("friend_privacy").equals("1")) {
                                        helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "everyone").commit();
                                    } else if (user.getString("friend_privacy").equals("2")) {
                                        helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "friends").commit();
                                    } else if (user.getString("friend_privacy").equals("3")) {
                                        helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "only me").commit();
                                    } else {
                                        helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "friends").commit();
                                    }

                                    if (user.getString("event_privacy").equals("1")) {
                                        helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "everyone").commit();
                                    } else if (user.getString("event_privacy").equals("2")) {
                                        helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "friends").commit();
                                    } else if (user.getString("event_privacy").equals("3")) {
                                        helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "only me").commit();
                                    } else {
                                        helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "friends").commit();
                                    }

                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage(res.getString("message"));
                                    builder.setPositiveButton("Login Now", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                                    builder.show();
                                }
                            } else {
                                dialog.dismiss();
                                AlertUtils.showSnack(RegisterActivity.this, eMail, String.valueOf(Html.fromHtml(res.getString("message"))));
                            }
                        } catch (JSONException e) {
                            Logger.e("userSignUpTask2 json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(RegisterActivity.this, eMail, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                }, "userSignUpTask2");
    }

    private void init() {
        receiptImage = findViewById(R.id.profile_pic);
        errPass = findViewById(R.id.error_password);
        errEm = findViewById(R.id.error_email);
        errUserName = findViewById(R.id.error_name);
        errDOB = findViewById(R.id.error_dob);
        edtDob = findViewById(R.id.user_dob);
        errPhoto = findViewById(R.id.err_take_upload_text);
        errorGender = findViewById(R.id.errorGender);
        errorHometown = findViewById(R.id.errorHometown);
        errorZipCode = findViewById(R.id.errorZipcode);

        name = findViewById(R.id.edt_name);
        eMail = findViewById(R.id.edt_email);
        passWord = findViewById(R.id.edt_password);

        Calendar cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);

        rgGender = findViewById(R.id.rgGender);
        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        etHometown = findViewById(R.id.userHomeTown);
        etZipcode = findViewById(R.id.userZipcode);

        signUp = findViewById(R.id.btn_signup);
    }

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(name.getText().toString().trim())) {
            isValid = false;
            errUserName.setVisibility(View.VISIBLE);
            errUserName.setText("This field is required...");
        } else {
            errUserName.setVisibility(View.GONE);
        }

        //For Email Address
        if (TextUtils.isEmpty(eMail.getText().toString().trim())) {
            isValid = false;
            errEm.setVisibility(View.VISIBLE);
            errEm.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(eMail.getText().toString().trim()).matches()) {
                errEm.setVisibility(View.GONE);
            } else {
                isValid = false;
                errEm.setText("Invalid email address.");
                errEm.setVisibility(View.VISIBLE);
            }
        }

        //For Password
        if (passWord.getText().toString().trim().equals("")) {
            isValid = false;
            errPass.setVisibility(View.VISIBLE);
            errPass.setText("This Field is required...");
        } else {
            if (passWord.length() < 6) {
                errPass.setText("The password must be more than 6 characters");
            } else {
                errPass.setVisibility(View.GONE);
            }
        }

        //For DOB
        if (TextUtils.isEmpty(edtDob.getText().toString())) {
            isValid = false;
            errDOB.setVisibility(View.VISIBLE);
            errDOB.setText("This Field is required...");
        } else {
            errDOB.setVisibility(View.GONE);
        }

        if (selectedImagePath == null) {
            isValid = false;
            errPhoto.setText("Please upload profile picture");
            errPhoto.setVisibility(View.VISIBLE);
        } else {
            errPhoto.setVisibility(View.GONE);
        }

        if (rgGender.getCheckedRadioButtonId() == -1) {
            isValid = false;
            errorGender.setText("Please select gender");
            errorGender.setVisibility(View.VISIBLE);
        } else {
            errorGender.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(etHometown.getText().toString().trim())) {
            isValid = false;
            errorHometown.setText("Please enter your hometown");
            errorHometown.setVisibility(View.VISIBLE);
        } else {
            errorHometown.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(etZipcode.getText().toString().trim())) {
            isValid = false;
            errorZipCode.setText("Please enter your zipcode");
            errorZipCode.setVisibility(View.VISIBLE);
        } else {
            errorZipCode.setVisibility(View.GONE);
        }

        return isValid;
    }

    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Upload Profile Picture");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case RESULT_LOAD_IMAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseFrmGallery(false);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public void chooseFrmGallery(boolean video) {
        Logger.e("choose from gallery ma pugis", "ah yaar");
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_leyline");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_leyline" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    private void correctOrientation(String path) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(path, opts);

        int rotationAngle = getCameraPhotoOrientation(RegisterActivity.this, fileUri, fileUri.getPath());

        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Logger.e("fileEx", e.getMessage());
        } catch (IOException e) {
            Logger.e("ioe Ex", e.getMessage());
        }

    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = 0;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(RegisterActivity.this);

            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (fileUri == null) {
                    return;
                }

                CropImage.activity(fileUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(RegisterActivity.this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                selectedImagePath = resultUri.getPath();
                if (resultUri.getPath() != null) {
                    Picasso.with(RegisterActivity.this).load(resultUri).into(receiptImage);
                } else {

                }
            }
        }
    }

    public void datePicker() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                day = dayOfMonth;
                month = monthOfYear + 1;
                RegisterActivity.this.year = year;
                edtDob.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", (monthOfYear + 1)) + "/" + year);
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.show();
    }
}
