package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.ChatAdapter;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.ChatModel;
import com.emts.leyline.models.ChatThread;
import com.emts.leyline.models.UserModel;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatActivity extends AppCompatActivity {
    public ChatThread chatThread;
    ChatAdapter chatAdapter;
    RecyclerView recyclerChat;
    LinearLayoutManager layoutManager;
    ArrayList<ChatModel> chatList;
    Toolbar toolbar;
    ProgressBar progressBar, infiniteProgressBar;
    PreferenceHelper prefHelper;
    TextView errorText;

    int limit = 10;
    int offset = 0;

    private int CHAT_POOL_TIME = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();
        chatThread = (ChatThread) intent.getSerializableExtra("chat_thread");

        prefHelper = PreferenceHelper.getInstance(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(chatThread.getSentBy().getUserName());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progress);
        infiniteProgressBar = (ProgressBar) findViewById(R.id.progress_infinite);
        errorText = (TextView) findViewById(R.id.tv_error_message);

        chatList = new ArrayList<>();

        recyclerChat = (RecyclerView) findViewById(R.id.list_recycler);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        recyclerChat.setLayoutManager(layoutManager);
        recyclerChat.setNestedScrollingEnabled(false);
        chatAdapter = new ChatAdapter(ChatActivity.this, chatList);
        recyclerChat.setAdapter(chatAdapter);
        recyclerChat.setItemAnimator(null);

        if (NetworkUtils.isInNetwork(this)) {
            chatTask(false);
        } else {
            recyclerChat.setVisibility(View.GONE);
            errorText.setText("No Internet Connected !!!");
            errorText.setVisibility(View.VISIBLE);
        }

        final EditText etChat = (EditText) findViewById(R.id.edt_enter_message);
        TextView btnSendMsg = (TextView) findViewById(R.id.btn_send_message);
        btnSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(etChat.getText().toString().trim())) {
                    ChatModel sendChat = new ChatModel();

                    Logger.e("chatFrag emoji", etChat.getText().toString().trim() + "   --");
                    String toServerUnicodeEncoded = StringEscapeUtils.escapeJava(etChat.getText().toString().trim());
                    Logger.e("chatFrag escapedString ", toServerUnicodeEncoded + "  --");
                    String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(toServerUnicodeEncoded);
                    Logger.e("chatFrag unescapedString", fromServerUnicodeDecoded + "  --");

                    sendChat.setMessage(toServerUnicodeEncoded);
                    sendChat.setMessageTime("now");
                    sendChat.setFromMe(true);
                    UserModel userModel = new UserModel();
                    userModel.setImgUrl(prefHelper.getString(PreferenceHelper.APP_USER_PIC, ""));
                    userModel.setUserId(prefHelper.getUserId());
                    userModel.setUserName(prefHelper.getString(PreferenceHelper.APP_USER_NAME, ""));
                    sendChat.setSentBy(userModel);
                    sendChat.setMsgStatus(ChatModel.MESSAGE_SENDING);
                    sendChat.setThreadId(chatThread.getThreadId());

                    chatList.add(sendChat);
                    chatAdapter.notifyItemInserted(chatList.size() - 1);
                    recyclerChat.smoothScrollToPosition(chatList.size() - 1);
                    recyclerChat.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);

                    //send chat message
                    sendMessageTask(toServerUnicodeEncoded, chatList.size() - 1);

                    etChat.setText("");
                }
            }
        });
    }

    private void chatTask(boolean isSwipeRefresh) {
        if (!isSwipeRefresh) {
            progressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(ChatActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("thread_id", chatThread.getThreadId());
        postParams.put("offset", String.valueOf(offset));
        postParams.put("limit", String.valueOf(limit));

        vHelper.addVolleyRequestListeners(Api.getInstance().getChatMessage, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                try {
                                    JSONArray chatArrayObj = resObj.getJSONArray("chat");
                                    for (int i = 0; i < chatArrayObj.length(); i++) {
                                        JSONObject eachChat = chatArrayObj.getJSONObject(i);
                                        ChatModel chat = new ChatModel();
                                        chat.setMessageId(eachChat.getString("message_id"));
                                        chat.setThreadId(eachChat.getString("thread_id"));
                                        UserModel sentBy = new UserModel();
                                        sentBy.setUserId(eachChat.getString("sender_id"));
                                        sentBy.setUserName(eachChat.getString("sender_name"));
                                        String senderImage = eachChat.getString("sender_image");
                                        if (!senderImage.equals("") && !senderImage.equals("null")) {
                                            sentBy.setImgUrl(resObj.getString("default_profile_img_Url") + senderImage);
                                        }
                                        chat.setSentBy(sentBy);
                                        chat.setFromMe(chat.getSentBy().getUserId().equals(prefHelper.getUserId()));
                                        chat.setMessage(eachChat.getString("message"));
                                        chat.setMessageTime(eachChat.getString("message_date"));
                                        chat.setMsgStatus(ChatModel.MESSAGE_SENT);

                                        chatList.add(chat);
                                    }

                                    errorText.setVisibility(View.GONE);
                                    recyclerChat.setVisibility(View.VISIBLE);
                                    chatAdapter.notifyDataSetChanged();
                                    recyclerChat.scrollToPosition(chatList.size() - 1);

                                    if (chatList.size() == 0 && offset == 0) {
                                        errorText.setText("There are no messages.");
                                        errorText.setVisibility(View.VISIBLE);
                                        recyclerChat.setVisibility(View.GONE);
                                    }
                                } catch (JSONException ex) {

                                    errorText.setText(resObj.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    recyclerChat.setVisibility(View.GONE);
                                }
                            } else {
                                errorText.setText(resObj.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                recyclerChat.setVisibility(View.GONE);
                            }
                            listenForIncoming();
                        } catch (JSONException e) {
                            Logger.e("chatTask resObj ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMessage = "Error !!! Please try again";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMessage = errorObj.getString("message");
                        } catch (Exception e) {
                            errorMessage = "Unexpected error occur please reload again";
                            Logger.e("chatTask error ex", e.getMessage() + "");
                        }
                        if (chatList.size() == 0) {
                            progressBar.setVisibility(View.GONE);
                            recyclerChat.setVisibility(View.GONE);
                            errorText.setText(errorMessage);
                            errorText.setVisibility(View.VISIBLE);
                        } else {
                            AlertUtils.showToast(ChatActivity.this, errorMessage + "");
                        }
                    }
                }, "chatTask");
    }

    private void sendMessageTask(final String body, final int sendPos) {
        VolleyHelper vHelper = VolleyHelper.getInstance(ChatActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("thread_id", chatThread.getThreadId());
        postParams.put("message", body);

        vHelper.addVolleyRequestListeners(Api.getInstance().sendMessage, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                chatList.get(sendPos).setMessageId(resObj.getString("message_id"));
                                chatList.get(sendPos).setMessageTime(resObj.getString("message_date"));
                                chatList.get(sendPos).setMsgStatus(ChatModel.MESSAGE_SENT);
                            } else {
                                chatList.get(sendPos).setMsgStatus(ChatModel.MESSAGE_FAILED);
                            }

                            chatAdapter.notifyItemChanged(sendPos);

                        } catch (JSONException e) {
                            Logger.e("sendMessageTask resObj ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMessage = "Error !!! Please try again";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            Logger.e("sendMessageTask error res", errorObj.toString());
                            errorMessage = errorObj.getString("message");
                        } catch (Exception e) {
                            errorMessage = "Unexpected error occur please reload again";
                            Logger.e("sendMessageTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showToast(ChatActivity.this, errorMessage + "");

                        chatList.get(sendPos).setMsgStatus(ChatModel.MESSAGE_FAILED);
                        chatAdapter.notifyItemChanged(sendPos);
                    }
                }, "sendMessageTask");
    }

    Handler handler;
    Runnable runnable;

    private void listenForIncoming() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Logger.e("pooling interval called", System.currentTimeMillis() + "");
                //needs last msg id so give it
                String lastMsgId = "0";
                if (chatList.size() > 0) {
                    lastMsgId = chatList.get(chatList.size() - 1).getMessageId();
                }
                if (lastMsgId != null) {
                    if (!lastMsgId.equals("0")) {
                        getMessageViaPolling(chatThread.getThreadId(), lastMsgId);
                    }
                }
                handler.postDelayed(runnable, CHAT_POOL_TIME);
            }
        };
        handler.postDelayed(runnable, CHAT_POOL_TIME);
    }

    private void getMessageViaPolling(final String threadId, final String msgId) {
        VolleyHelper vHelper = VolleyHelper.getInstance(ChatActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("thread_id", threadId);
        postParams.put("message_id", msgId);

        vHelper.addVolleyRequestListeners(Api.getInstance().getChatMessage, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            JSONArray chatArrayObj = resObj.getJSONArray("chat");
                            for (int i = 0; i < chatArrayObj.length(); i++) {
                                JSONObject eachChat = chatArrayObj.getJSONObject(i);
                                ChatModel chat = new ChatModel();
                                chat.setMessageId(eachChat.getString("message_id"));
                                chat.setThreadId(eachChat.getString("thread_id"));
                                UserModel sentBy = new UserModel();
                                sentBy.setUserId(eachChat.getString("sender_id"));
                                sentBy.setUserName(eachChat.getString("sender_name"));
                                String senderImage = eachChat.getString("sender_image");
                                if (!senderImage.equals("") && !senderImage.equals("null")) {
                                    sentBy.setImgUrl(resObj.getString("default_profile_img_Url") + senderImage);
                                }
                                chat.setSentBy(sentBy);
                                chat.setFromMe(chat.getSentBy().getUserId().equals(prefHelper.getUserId()));
                                chat.setMessage(eachChat.getString("message"));
                                chat.setMessageTime(eachChat.getString("message_date"));
                                chat.setMsgStatus(ChatModel.MESSAGE_SENT);

                                chatList.add(chat);
                            }

                            chatAdapter.notifyDataSetChanged();
                            recyclerChat.scrollToPosition(chatList.size() - 1);
                        } catch (JSONException e) {
                            Logger.e("getMessageViaPolling resObj ex", e.getMessage() + "");
                        } catch (Exception e) {
                            Logger.e("chatFrag getMessageViaPolline ex 111", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMessage = "Error !!! Please try again";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMessage = errorObj.getString("message");
                        } catch (Exception e) {
                            errorMessage = "Unexpected error occur please reload again";
                            Logger.e("getMessageViaPolling error ex", e.getMessage() + "");
                        }
                    }
                }, "getMessageViaPolling");
    }


    private void deleteChatTask(final int position) {
        final ChatModel chatToDel = chatList.get(position);

        VolleyHelper vHelper = VolleyHelper.getInstance(ChatActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (!resObj.getBoolean("error")) {
                                chatList.remove(position);
                                chatAdapter.notifyItemRemoved(position);
                                if (chatList.size() == 0) {
                                    recyclerChat.setVisibility(View.GONE);
                                    errorText.setText("No chat messages");
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            }
                            AlertUtils.showToast(ChatActivity.this, resObj.getString("message"));

                        } catch (JSONException e) {
                            Logger.e("deleteChatTask resObj ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMessage = "Error !!! Please try again";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMessage = errorObj.getString("message");
                        } catch (Exception e) {
                            errorMessage = "Unexpected error occur please reload again";
                            Logger.e("deleteChatTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showToast(ChatActivity.this, errorMessage + "");
                    }
                }, "deleteChatTask");
    }

    @Override
    public void onBackPressed() {
        removeCallbacks();
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        removeCallbacks();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        removeCallbacks();
        super.onStop();
    }

    public void removeCallbacks() {
        try {
            if (handler != null)
                handler.removeCallbacks(runnable);
        } catch (Exception e) {
        }
    }


    public void chatMessages(String message, Boolean fromMe, String messageTime) {
        ChatModel chatModel = new ChatModel();
        chatModel.setMessage(message);
        chatModel.setFromMe(fromMe);
        chatModel.setMessageTime(messageTime);
        chatList.add(chatModel);
    }
}
