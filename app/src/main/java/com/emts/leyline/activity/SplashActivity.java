package com.emts.leyline.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.LocationUpdateService;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class SplashActivity extends AppCompatActivity {
    private final int splashTimer = 3000;
    PreferenceHelper prefsHelper;
    boolean showGpsAlert = false;
    public static final int REQUEST_LOCATION_PERMISSION = 678;
    Handler mHandler = new Handler();
    private boolean goodToGo = false;
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (!showGpsAlert) {
                checkGoodToGoForward();
            }
        }
    };

    private void checkGoodToGoForward() {
        if (goodToGo) {
            if (prefsHelper.isLogin()) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
            SplashActivity.this.finish();
        } else {
            goodToGo = true;
        }
    }

    Dialog gpsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_splash);

        startService(new Intent(getApplicationContext(), LocationUpdateService.class));

        prefsHelper = PreferenceHelper.getInstance(this);

        if (NetworkUtils.isInNetwork(this)) {
            getEventsTask();
        } else {
            errorAlertCannotGetEvents();
        }
    }

    public void checkGPS() {
        if (!LocationUpdateService.checkIfGPSEnable(this)) {
            showGpsAlert = true;
            final AlertDialog.Builder enableGpsAlert = new AlertDialog.Builder(this);
            enableGpsAlert.setCancelable(false);
            enableGpsAlert.setTitle("GPS is off");
            enableGpsAlert.setMessage("The app need GPS to be enabled to work properly. Please enable GPS to proceed.");
            enableGpsAlert.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Intent gpsOptionsIntent = new Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsOptionsIntent);
                }
            });
            enableGpsAlert.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    onBackPressed();
                }
            });
            gpsDialog = enableGpsAlert.show();
        } else {
            showGpsAlert = false;
            getLocationPermission();
        }
    }

    @Override
    public void onBackPressed() {
        if (gpsDialog != null && gpsDialog.isShowing()) {
            gpsDialog.dismiss();
        }
        mHandler.removeCallbacks(mRunnable);
        super.onBackPressed();
    }

    private void getLocationPermission() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
            } else {
                mHandler.postDelayed(mRunnable, splashTimer);
            }
        } catch (Exception e) {
            Logger.e("locationPermission ex", e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGPS();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted :)
                    mHandler.postDelayed(mRunnable, splashTimer);
                } else {
                    for (int i = 0, len = permissions.length; i < len; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            // user rejected the permission
                            boolean showRationale = false;
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                showRationale = shouldShowRequestPermissionRationale(permission);
                            }
                            if (!showRationale) {
                                AlertUtils.showErrorAlert(SplashActivity.this, "Please give location permission in order to run this application", null);
                                // user also CHECKED "never ask again"
                                // you can either enable some fall back,
                                // disable features of your app
                                // or open another dialog explaining
                                // again the permission and directing to
                                // the app setting
                            } else if (Manifest.permission.WRITE_CONTACTS.equals(permission)) {
                                mHandler.postDelayed(mRunnable, splashTimer);
                                // user did NOT check "never ask again"
                                // this is a good place to explain the user
                                // why you need the permission and ask if he wants
                                // to accept it (the rationale)
                            }
                        }
                    }
                }
                return;
            }

        }
    }

    private void getEventsTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(SplashActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getEventCategory, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                //save the category data in memory (preference)
                                prefsHelper.edit().putString(PreferenceHelper.CATEGORY_JSON_RESPONSE, response).commit();
                                Logger.e("k ayo ta", response);
                                checkGoodToGoForward();
                            } else {
                                //try again alert
                                errorAlertCannotGetEvents();
                            }
                        } catch (JSONException e) {
                            Logger.e("getEventsTask ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                        }
                        Logger.e("getEventsTask error msg", errMsg);
                        errorAlertCannotGetEvents();
                    }
                }, "getEventsTask");

    }

    private void errorAlertCannotGetEvents() {
        AlertUtils.simpleAlert(SplashActivity.this, "Error",
                "Cannot get required data to proceed forward.\nPlease try again with internet access.",
                "Try Again", "Exit", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            getEventsTask();
                        } else {
                            SplashActivity.this.finish();
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance(this).cancelRequest("getEventsTask");
    }
}
