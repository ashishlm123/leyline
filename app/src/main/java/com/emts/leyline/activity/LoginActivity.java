package com.emts.leyline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class LoginActivity extends AppCompatActivity {
    TextView registerNow, forgotPassword, errEm, errPass;
    EditText eMail, passWord;
    Button logIn;
    PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_login);

        init();

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                        userLoginTask();
                        getEventsTask();
                    } else {
                        AlertUtils.showSnack(LoginActivity.this, view, "No internet connection...");
                    }
                }
            }
        });

        registerNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        errEm = (TextView) findViewById(R.id.error_email);
        errPass = (TextView) findViewById(R.id.error_password);

        eMail = (EditText) findViewById(R.id.edt_email);
        passWord = (EditText) findViewById(R.id.edt_password);

        logIn = (Button) findViewById(R.id.btn_login);
        registerNow = (TextView) findViewById(R.id.register_now);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
    }

    public boolean validation() {
        boolean isValid = true;

        //For Email Address
        if (TextUtils.isEmpty(eMail.getText().toString().trim())) {
            isValid = false;
            errEm.setVisibility(View.VISIBLE);
            errEm.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(eMail.getText().toString().trim()).matches()) {
                errEm.setVisibility(View.GONE);
            } else {
                isValid = false;
                errEm.setText("Invalid email address...");
                errEm.setVisibility(View.VISIBLE);
            }
        }

        //For Password
        if (passWord.getText().toString().trim().equals("")) {
            isValid = false;
            errPass.setVisibility(View.VISIBLE);
            errPass.setText("This Field is required...");
        } else {
            errPass.setVisibility(View.GONE);
        }

        return isValid;
    }

    private void getEventsTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(LoginActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getEventCategory, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                //save the category data in memory (preference)
                                helper.edit().putString(PreferenceHelper.CATEGORY_JSON_RESPONSE, response).commit();
                                Logger.e("k ayo ta", response);

                            } else {
                                //try again alert
                                errorAlertCannotGetEvents();
                            }
                        } catch (JSONException e) {
                            Logger.e("getEventsTask ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                        }
                        Logger.e("getEventsTask error msg", errMsg);
                        errorAlertCannotGetEvents();
                    }
                }, "getEventsTask");

    }

    private void errorAlertCannotGetEvents() {
        AlertUtils.simpleAlert(LoginActivity.this, "Error",
                "Cannot get required data to proceed forward.\nPlease try again with internet access.",
                "Try Again", "Exit", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            getEventsTask();
                        } else {
                            LoginActivity.this.finish();
                        }
                    }
                });
    }

    private void userLoginTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging In...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("email", eMail.getText().toString().trim());
        postMap.put("password", passWord.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject user = res.getJSONObject("user_detail");
                                helper.edit().putString(PreferenceHelper.TOKEN, user.getString("token")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_NAME, user.getString("name")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, user.getString("email")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_ID, user.getString("id")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_DOB, user.getString("dob_day") + "/"
                                        + user.getString("dob_month") + "/" + user.getString("dob_year")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_PIC, user.getString("profile_img_Url")).commit();
                                if (user.getString("user_bio").equals("null")) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_BIO, "").commit();
                                } else {
                                    helper.edit().putString(PreferenceHelper.APP_USER_BIO, user.getString("user_bio")).commit();
                                }
                                helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_WEBSITE, user.getString("website_url")).commit();
                                helper.edit().putString(PreferenceHelper.APP_USER_COVER_PIC, user.getString("cover_img_Url")).commit();
                                String gender = user.optString("gender");
                                gender = gender.equals("2") ? "F" : "M";
                                helper.edit().putString(PreferenceHelper.USER_GENDER, gender).commit();
                                helper.edit().putString(PreferenceHelper.USER_HOME_TOWN, user.optString("city")).commit();
                                helper.edit().putString(PreferenceHelper.USER_ZIP_CODE, user.optString("zipcode")).commit();

                                if (user.getString("profile_privacy").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "everyone").commit();
                                } else if (user.getString("profile_privacy").equals("2")) {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "friends").commit();
                                } else if (user.getString("profile_privacy").equals("3")) {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "only me").commit();
                                } else {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "everyone").commit();
                                }

                                if (user.getString("friend_privacy").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "everyone").commit();
                                } else if (user.getString("friend_privacy").equals("2")) {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "friends").commit();
                                } else if (user.getString("friend_privacy").equals("3")) {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "only me").commit();
                                } else {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "friends").commit();
                                }

                                if (user.getString("event_privacy").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "everyone").commit();
                                } else if (user.getString("event_privacy").equals("2")) {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "friends").commit();
                                } else if (user.getString("event_privacy").equals("3")) {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "only me").commit();
                                } else {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "friends").commit();
                                }

                                JSONArray interestArray = user.getJSONArray("interest");
                                StringBuilder interestConcat = new StringBuilder();
                                for (int i = 0; i < interestArray.length(); i++) {
                                    JSONObject interestObject = interestArray.getJSONObject(i);
                                    interestConcat.append(interestObject.getString("interest_title"))
                                            .append(SettingsActivity.INTEREST_CONCAT_TEXT);
                                }
                                if (!TextUtils.isEmpty(interestConcat.toString())) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_INTERESTS, interestConcat.toString()).commit();
                                }

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                AlertUtils.hideInputMethod(LoginActivity.this, eMail);
                                startActivity(intent);

                            } else {
                                AlertUtils.showSnack(LoginActivity.this, eMail, String.valueOf(Html.fromHtml(res.getString("message"))));
                            }

                        } catch (JSONException e) {
                            Logger.e("userLoginTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userLoginTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(LoginActivity.this, "Error !!!", errMsg);
                    }
                }, "userLoginTask");
    }

}
