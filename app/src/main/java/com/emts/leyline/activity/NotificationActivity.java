package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.NotificationAdapter;
import com.emts.leyline.customviews.EndlessRecyclerViewScrollListener;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.NotificationModel;
import com.emts.leyline.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationActivity extends AppCompatActivity {
    RecyclerView notificationListing;
    TextView errorText;
    ProgressBar progressBar, infiniteProgressBar;
    NotificationAdapter notificationAdapter;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    ArrayList<NotificationModel> notificationList = new ArrayList<>();
    Toolbar toolbar;
    PreferenceHelper helper;
    int limit = 30;
    int offset = 0;
    public static final int MERGE_REQUEST_REQUEST = 976;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_list);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Notifications");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        errorText = findViewById(R.id.tv_error_message);

        notificationListing = findViewById(R.id.list_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotificationActivity.this);
        notificationListing.setLayoutManager(linearLayoutManager);
        notificationAdapter = new NotificationAdapter(notificationList, NotificationActivity.this);
        notificationListing.setAdapter(notificationAdapter);

        progressBar = findViewById(R.id.progress);
        infiniteProgressBar = findViewById(R.id.progress_infinite);
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (notificationList.size() >= limit) {
                    userNotificationsTask();
                }
            }
        };
        notificationListing.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(this)) {
            userNotificationsTask();
        } else {
            AlertUtils.showToast(NotificationActivity.this, "No Internet Connection...");
        }

    }

    private void userNotificationsTask() {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("offset", String.valueOf(offset));
        postMap.put("limit", String.valueOf(limit));

        vHelper.addVolleyRequestListeners(Api.getInstance().getNotificationsUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray notificationsList = res.getJSONArray("notification");
                                if (offset == 0) {
                                    notificationList.clear();
                                }

                                for (int i = 0; i < notificationsList.length(); i++) {
                                    NotificationModel notificationModel = new NotificationModel();
                                    JSONObject eachNotifications = notificationsList.getJSONObject(i);
                                    notificationModel.setNotId(eachNotifications.getString("notification_id"));
                                    notificationModel.setNotTime(eachNotifications.getString("created_date"));

                                    UserModel userModel = new UserModel();
                                    userModel.setUserId(eachNotifications.getString("user_id"));
                                    userModel.setUserName(eachNotifications.getString("name"));
                                    String profileImage = eachNotifications.getString("image");

                                    UserModel creator = new UserModel();
                                    creator.setUserId(eachNotifications.optString("created_by"));
                                    notificationModel.setCreatedBy(creator);

                                    String notificationType = eachNotifications.getString("notification_type");
                                    if (notificationType.equals(NotificationModel.NOTI_STARTTODAY)) {
                                        //reuse the user image field for event image
                                        String eventImage = eachNotifications.getString("event_image");
                                        if (!TextUtils.isEmpty(profileImage)) {
                                            userModel.setImgUrl(res.getString("event_image_default_url") + eventImage);
                                        } else {
                                            userModel.setImgUrl("");
                                        }
                                    } else {
                                        if (!TextUtils.isEmpty(profileImage)) {
                                            userModel.setImgUrl(res.getString("profileimage_default_url") + profileImage);
                                        } else {
                                            userModel.setImgUrl("");
                                        }
                                    }
                                    //comment by and mention by username
                                    if (notificationType.equalsIgnoreCase(NotificationModel.NOTI_MENTION)) {
                                        String mentionBy = eachNotifications.optString("mention_by");
                                        if (mentionBy != null && !mentionBy.equalsIgnoreCase("null")
                                                && !TextUtils.isEmpty(mentionBy)) {
                                            userModel.setUserName(mentionBy);
                                        }else{
                                            userModel.setUserName("Your friend");
                                        }
                                    }

                                    notificationModel.setNotiUser(userModel);

                                    notificationModel.setEventId(eachNotifications.getString("event_id"));
                                    notificationModel.setEventName(eachNotifications.getString("event_name"));
                                    notificationModel.setNotType(notificationType);
                                    notificationModel.setMergeReqStat(eachNotifications.getString("request_stat"));

                                    notificationList.add(notificationModel);
                                }
                                offset = offset + limit;
                                notificationAdapter.notifyDataSetChanged();
                                notificationListing.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    notificationListing.setVisibility(View.GONE);
                                } else {
                                    notificationListing.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(NotificationActivity.this, errorText, " No more notifications");
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("userNotificationsTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userNotificationsTask error ex", e.getMessage() + "");
                        }
                        if (offset == 0) {
                            errorText.setText(errMsg);
                            errorText.setVisibility(View.VISIBLE);
                            notificationListing.setVisibility(View.GONE);
                        } else {
                            AlertUtils.showToast(NotificationActivity.this, errMsg);
                        }
                    }
                }, "userNotificationsTask");

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == MERGE_REQUEST_REQUEST) {
                int position = data.getIntExtra("position", -1);
                if (position >= 0) {
                    NotificationModel notificationMdl = (NotificationModel) data.getSerializableExtra("notification");
                    notificationList.set(position, notificationMdl);
                    notificationAdapter.notifyItemChanged(position);
                }
            }
        }
    }
}
