package com.emts.leyline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPasswordActivity extends AppCompatActivity {
    Button sendResetCode;
    TextView errEm;
    EditText eMail;
    PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_forgot_password);

        init();

        sendResetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(ForgotPasswordActivity.this)) {
                        userForgotPasswordTask();
                    }
                }
            }
        });
    }

    private void userForgotPasswordTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("email", eMail.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().forgotPasswordUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                dialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showErrorAlert(ForgotPasswordActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                onBackPressed();
                            }
                        });
                    } else {
                        dialog.dismiss();
                        AlertUtils.showSnack(ForgotPasswordActivity.this, eMail, res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("userForgotPasswordTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                dialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                }
                AlertUtils.showToast(ForgotPasswordActivity.this, errMsg);
            }
        }, "userForgotPasswordTask");
    }

    private boolean validation() {
        boolean isValid = true;

        //For Email Address
        if (TextUtils.isEmpty(eMail.getText().toString().trim())) {
            isValid = false;
            errEm.setVisibility(View.VISIBLE);
            errEm.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(eMail.getText().toString().trim()).matches()) {
                errEm.setVisibility(View.GONE);
            } else {
                isValid = false;
                errEm.setText("Invalid email address...");
                errEm.setVisibility(View.VISIBLE);
            }
        }
        return isValid;
    }


    private void init() {
        sendResetCode = (Button) findViewById(R.id.btn_send_reset_code);
        errEm = (TextView) findViewById(R.id.error_email);
        eMail = (EditText) findViewById(R.id.edt_email);
    }
}
