package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.HashSearchAdapter;
import com.emts.leyline.customviews.EndlessRecyclerViewScrollListener;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.FeedModel;
import com.emts.leyline.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HashTagSearchActivity extends AppCompatActivity {
    RecyclerView searchResultListing;
    ProgressBar progressBar, infiniteProgressBar;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    TextView errorText;
    EditText searchBar;
    PreferenceHelper helper;
    HashSearchAdapter searchResultAdapter;
    ArrayList<FeedModel> searchList;
    int limit = 50;
    int offset = 0;
    String hashTag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_search);

        Intent intent = getIntent();
        if (intent != null) {
            hashTag = intent.getStringExtra("hashtag");
        }
        if (hashTag == null) {
            hashTag = "";
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Search");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        searchList = new ArrayList<>();
        searchResultAdapter = new HashSearchAdapter(HashTagSearchActivity.this, searchList);
        searchResultAdapter.setHastTag(hashTag);
        searchResultListing = findViewById(R.id.list_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HashTagSearchActivity.this);
        searchResultListing.setLayoutManager(linearLayoutManager);
        searchResultListing.setAdapter(searchResultAdapter);
        progressBar = findViewById(R.id.progress);

        infiniteProgressBar = findViewById(R.id.progress_infinite);
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (searchList.size() >= limit) {
                    hashSearchTask(searchBar.getText().toString().trim(), true);
                }
            }
        };
        searchResultListing.addOnScrollListener(infiniteScrollListener);

        errorText = findViewById(R.id.tv_error_message);
        searchBar = findViewById(R.id.search_bar);
        searchBar.setHint("Search #hashtag...");
        searchBar.setText(hashTag);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (NetworkUtils.isInNetwork(HashTagSearchActivity.this)) {
                    if (!(charSequence.toString().equals(""))) {
                        if (!(charSequence.length() == 1 && charSequence.equals("#"))) {
                            hashSearchTask(charSequence.toString(), false);
                            hashTag = charSequence.toString();
                            searchResultAdapter.setHastTag(hashTag);
                        }
                    }
                } else {
                    AlertUtils.showToast(HashTagSearchActivity.this, "No Internet Connection...");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        if (NetworkUtils.isInNetwork(HashTagSearchActivity.this)) {
            if (!hashTag.equals("")) {
                hashSearchTask(hashTag, false);
            }
        }
    }

    private void hashSearchTask(String searchText, boolean isLoadMore) {
        errorText.setVisibility(View.GONE);
        if (isLoadMore) {

        } else {
            offset = 0;
            searchList.clear();
            searchResultAdapter.notifyDataSetChanged();
        }

        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        vHelper.cancelRequest("hashSearchTask");
        HashMap<String, String> postMap = vHelper.getPostParams();

        if (searchText.contains("#")) {
            postMap.put("hash_key", searchText);
        } else {
            postMap.put("hash_key", "#" + searchText);
        }
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().hastSearchUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        infiniteProgressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray searchResult = res.getJSONArray("search_result");
                                if (offset == 0) {
                                    searchList.clear();
                                }
                                for (int i = 0; i < searchResult.length(); i++) {
                                    JSONObject eachObj = searchResult.getJSONObject(i);
//                            FriendsModel friendsModel = new FriendsModel();
//                            friendsModel.setUserName(eachObj.getString("name"));
//                            friendsModel.seteMail(eachObj.getString("email"));
//                            friendsModel.setUserId(eachObj.getString("id"));
//                            if (eachObj.getString("image").equals("") || eachObj.getString("image").equals("null")) {
//                                friendsModel.setImgUrl("");
//                            }
//                            friendsModel.setImgUrl(res.getString("profile_img_Url") + eachObj.getString("image"));
//                            searchList.add(friendsModel);
                                    JSONObject eventDetails = eachObj.getJSONObject("details");
                                    FeedModel eventModel = new FeedModel();

                                    eventModel.setEventId(eventDetails.getString("event_id"));
                                    eventModel.setEventName(eventDetails.getString("event_name"));
                                    eventModel.setEventDesc(eventDetails.getString("description"));
                                    eventModel.setEventLocation(eventDetails.getString("location"));
                                    eventModel.setFeedTitle(eventDetails.getString("body"));
                                    eventModel.setEventDate(eventDetails.getString("event_date"));
                                    eventModel.setEventTime(eventDetails.getString("event_time"));
                                    eventModel.setStatus(eventDetails.getString("status"));
                                    eventModel.setEventAttendStatus(eventDetails.getString("attendance"));

                                    String eventImage = eventDetails.getString("event_image");
                                    if (!TextUtils.isEmpty(eventImage)) {
                                        eventModel.setEventImage(res.getString("event_image_url") + eventImage);
                                    } else {
                                        eventModel.setEventImage("");
                                    }

                                    eventModel.setLiked(Boolean.parseBoolean(eventDetails.getString("like_status")));
                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eventDetails.getString("rating")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }

                                    if (eventDetails.getString("latitude").equals("null")) {
                                        eventModel.setLatitude(0);
                                    } else {
                                        eventModel.setLatitude(Double.parseDouble(eventDetails.getString("latitude")));
                                    }
                                    if (eventDetails.getString("longitude").equals("null")) {
                                        eventModel.setLongitude(0);
                                    } else {
                                        eventModel.setLongitude(Double.parseDouble(eventDetails.getString("longitude")));
                                    }
                                    eventModel.setEventCatId(eachObj.getString("event_category_id"));

                                    JSONArray hostArray = eachObj.getJSONArray("host_details");
                                    UserModel[] hostUsers = new UserModel[hostArray.length()];
                                    for (int j = 0; j < hostArray.length(); j++) {
                                        JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                        UserModel hostedBy = new UserModel();
                                        hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                        hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                        if (!eachHostInfo.getString("host_image").equals("")) {
                                            hostedBy.setImgUrl(res.getString("profileimage_default_url") + eachHostInfo.getString("host_image"));
                                        } else {
                                            hostedBy.setImgUrl("");
                                        }
                                        hostUsers[j] = hostedBy;
                                    }
                                    eventModel.setHostedBy(hostUsers);

                                    UserModel commentModel = new UserModel();
                                    commentModel.setUserName(eventDetails.getString("commentor_name"));
                                    commentModel.setUserId(eventDetails.getString("comment_by"));
                                    String commentorImage = eventDetails.getString("commentor_image");
                                    if (!TextUtils.isEmpty(commentorImage)) {
                                        commentModel.setImgUrl(res.getString("profileimage_default_url") + commentorImage);
                                    } else {
                                        commentModel.setImgUrl("");
                                    }
                                    eventModel.setRelationBy(commentModel);

                                    searchList.add(eventModel);
                                }
                                offset = offset + limit;
                                searchResultAdapter.notifyDataSetChanged();
                                searchResultListing.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                                if (searchList.size() > 0) {
                                    searchResultListing.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                    searchResultAdapter.notifyDataSetChanged();
                                } else {
                                    searchResultListing.setVisibility(View.GONE);
                                    errorText.setVisibility(View.VISIBLE);
                                    errorText.setText(res.getString("message"));
                                }

                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    searchResultListing.setVisibility(View.GONE);
                                } else {
                                    searchResultListing.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(
                                            HashTagSearchActivity.this, errorText, " No more notifications");
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("hashSearchTask ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("hashSearchTask error ex", e.getMessage() + "");
                        }
                        errorText.setText(errMsg);
                        errorText.setVisibility(View.VISIBLE);
                        searchResultListing.setVisibility(View.GONE);
                    }

                }, "hashSearchTask");

    }

    @Override
    public void onBackPressed() {
        VolleyHelper.getInstance(this).cancelRequest("hashSearchTask");
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == HashSearchAdapter.EVENT_RATE) {
                EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                for (int i = 0; i < searchList.size(); i++) {
                    if (eventModel.getEventId().equals(searchList.get(i).getEventId())) {
                        searchList.get(i).setLiked(eventModel.isLiked());
                        searchList.get(i).setEventLikes(eventModel.getEventLikes());
                        searchList.get(i).setEventRating(eventModel.getEventRating());
                        searchList.get(i).setEventAttendStatus(eventModel.getEventAttendStatus());

                        searchResultAdapter.notifyItemChanged(i);
                    }
                }

                Intent intent = new Intent();
                intent.putExtra("eventModel", eventModel);
                setResult(RESULT_OK, intent);
            }
        }
    }
}
