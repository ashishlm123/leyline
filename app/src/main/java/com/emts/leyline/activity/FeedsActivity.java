package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.EventSuggestionAdaper;
import com.emts.leyline.adapter.FeedsAdapter;
import com.emts.leyline.customviews.EndlessRecyclerViewScrollListener;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.FeedModel;
import com.emts.leyline.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FeedsActivity extends AppCompatActivity {

    RecyclerView feedsListView;
    ProgressBar progressBar, infiniteProgressBar;
    PreferenceHelper helper;
    TextView errorText;
    FeedsAdapter feedsAdapter;
    EventSuggestionAdaper eventSuggestionAdaper;
    ArrayList<FeedModel> feedsList = new ArrayList<>();
    Toolbar toolbar;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    int limit = 20, offset = 0;
    ArrayList<EventModel> eventSuggestionList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Feeds");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        RelativeLayout feedHolder = (RelativeLayout) findViewById(R.id.lay_feed_view);
        feedsListView = feedHolder.findViewById(R.id.list_recycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FeedsActivity.this);
        feedsListView.setLayoutManager(linearLayoutManager);
        feedsAdapter = new FeedsAdapter(feedsList, FeedsActivity.this);
        eventSuggestionAdaper = new EventSuggestionAdaper(eventSuggestionList, FeedsActivity.this);
        feedsListView.setAdapter(feedsAdapter);

        progressBar = feedHolder.findViewById(R.id.progress);

        infiniteProgressBar = feedHolder.findViewById(R.id.progress_infinite);
        errorText = feedHolder.findViewById(R.id.tv_error_message);

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (feedsList.size() >= limit) {
                    getNewsFeedTask(offset);
                }
            }
        };
        feedsListView.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(this)) {
            errorText.setVisibility(View.GONE);
            getNewsFeedTask(offset);
        } else {
            errorText.setText(R.string.no_internet);
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            feedsListView.setVisibility(View.GONE);
        }

    }

    private void getNewsFeedTask(int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            infiniteProgressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("user_id", helper.getUserId());
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offsetValue));

        vHelper.addVolleyRequestListeners(Api.getInstance().getFeedsUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            Logger.printLongLog("long", response);
                            if (res.getBoolean("status")) {
                                JSONArray feedArray = res.getJSONArray("feeds");
                                if (feedArray.length() == 0) {
                                    errorText.setText("No any feeds");
                                    errorText.setVisibility(View.VISIBLE);
                                    feedsListView.setVisibility(View.GONE);
                                } else {
                                    if (offset == 0) {
                                        feedsList.clear();
                                    }
                                    for (int i = 0; i < feedArray.length(); i++) {
                                        JSONObject eachObj = feedArray.getJSONObject(i);
                                        FeedModel eventModel = new FeedModel();
//                                        UserModel userModel = new UserModel();
//                                        userModel.setUserName(eachObj.getString("host_name"));
//                                        eventModel.setHostedBy(userModel);
                                        eventModel.setEventLocation(eachObj.getString("location"));
                                        eventModel.setFeedId(eachObj.getString("feed_id"));
                                        eventModel.setEventId(eachObj.getString("event_id"));
                                        eventModel.setFeedTitle(eachObj.getString("feed_title"));
                                        eventModel.setEventDate(eachObj.getString("event_date"));
                                        eventModel.setStatus(eachObj.getString("event_status"));

                                        UserModel relationModel = new UserModel();
                                        relationModel.setUserName(eachObj.getString("feeder_name"));
                                        relationModel.setUserId(eachObj.getString("feeder_id"));
                                        String relImage = eachObj.getString("feeder_image");
                                        if (!TextUtils.isEmpty(relImage)) {
                                            relationModel.setImgUrl(res.getString("profile_img_Url") + relImage);
                                        } else {
                                            relationModel.setImgUrl("");
                                        }
                                        eventModel.setRelationBy(relationModel);
                                        eventModel.setEventAttendStatus(eachObj.getString("attendance"));

                                        JSONArray hostArray = eachObj.getJSONArray("host_details");
                                        UserModel[] hostUsers = new UserModel[hostArray.length()];
                                        for (int j = 0; j < hostArray.length(); j++) {
                                            JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                            UserModel hostedBy = new UserModel();
                                            hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                            hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                            if (!eachHostInfo.getString("host_image").equals("")) {
                                                hostedBy.setImgUrl(res.getString("profile_img_Url") + eachHostInfo.getString("host_image"));
                                            } else {
                                                hostedBy.setImgUrl("");
                                            }
                                            hostUsers[j] = hostedBy;
                                        }
                                        eventModel.setHostedBy(hostUsers);


                                        eventModel.setEventName(eachObj.getString("event_name"));
                                        eventModel.setEventDesc(eachObj.getString("description"));
                                        eventModel.setEventCatId(eachObj.getString("event_category_id"));

                                        if (eachObj.getString("latitude").equals("null")) {
                                            eventModel.setLatitude(0);
                                        } else {
                                            eventModel.setLatitude(Double.parseDouble(eachObj.getString("latitude")));
                                        }
                                        if (eachObj.getString("longitude").equals("null")) {
                                            eventModel.setLongitude(0);
                                        } else {
                                            eventModel.setLongitude(Double.parseDouble(eachObj.getString("longitude")));
                                        }
                                        eventModel.setEventTime(eachObj.getString("event_time"));
                                        try {
                                            eventModel.setEventRating(Float.parseFloat(eachObj.getString("event_rate")));
                                        } catch (Exception e) {
                                            eventModel.setEventRating(0);
                                        }

                                        String eventImage = eachObj.getString("image_name");
                                        if (!TextUtils.isEmpty(eventImage)) {
                                            eventModel.setEventImage(res.getString("event_image_url") + eventImage);
                                        } else {
                                            eventModel.setEventImage("");
                                        }

                                        eventModel.setEventLikes(eachObj.getString("event_like_count"));
                                        eventModel.setLiked(Boolean.parseBoolean(eachObj.getString("your_like_status")));

                                        String commentCount = eachObj.getString("event_comment_count");
                                        try {
                                            Integer.parseInt(commentCount); //to check if null
                                            eventModel.setEventCommentCount(commentCount);
                                        } catch (Exception e) {
                                            eventModel.setEventCommentCount("0");
                                        }

                                        feedsList.add(eventModel);
                                    }
                                    if (feedsList.size() > 4 && offset == 0) {
                                        feedsList.add(4, new FeedModel());
                                    }
                                    offset = feedsList.size();
                                    feedsAdapter.notifyDataSetChanged();
                                    feedsListView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);

                                    userEventSuggestionsTask();
                                }
                            } else {
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    feedsListView.setVisibility(View.GONE);
                                } else {
                                    feedsListView.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(FeedsActivity.this, errorText, " No more feeds...");
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("getNewsFeedTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                        infiniteProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userAttendEventTask error ex", e.getMessage() + "");
                        }
                        errorText.setText(errMsg);
                        errorText.setVisibility(View.VISIBLE);
                        feedsListView.setVisibility(View.GONE);
                    }

                }, "getNewsFeedTask");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == FeedsAdapter.EVENT_RATE) {
                EventModel eventModel = (EventModel) data.getSerializableExtra("eventModel");
                for (int i = 0; i < feedsList.size(); i++) {
                    if (eventModel.getEventId().equals(feedsList.get(i).getEventId())) {
                        feedsList.get(i).setLiked(eventModel.isLiked());
                        feedsList.get(i).setEventLikes(eventModel.getEventLikes());
                        feedsList.get(i).setEventRating(eventModel.getEventRating());
                        feedsList.get(i).setEventAttendStatus(eventModel.getEventAttendStatus());

                        feedsAdapter.notifyItemChanged(i);
                    }
                }

                for (int j = 0; j < eventSuggestionList.size(); j++) {
                    if (eventModel.getEventId().equals(eventSuggestionList.get(j).getEventId())) {
                        eventSuggestionList.set(j, eventModel);
                        eventSuggestionAdaper.notifyItemChanged(j);
                    }
                }
                Intent intent = new Intent();
                intent.putExtra("eventModel", eventModel);
                setResult(RESULT_OK, intent);
            }
        }
    }

    private void userEventSuggestionsTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(FeedsActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("offset", "0");
        postMap.put("limit", "50");

        vHelper.addVolleyRequestListeners(Api.getInstance().suggestEventUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        eventSuggestionList.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray suggestedEvents = res.getJSONArray("suggested_events");
                                for (int i = 0; i < suggestedEvents.length(); i++) {
                                    JSONObject eachObj = suggestedEvents.getJSONObject(i);
                                    EventModel eventModel = new EventModel();

                                    JSONArray hostArray = eachObj.getJSONArray("host_details");
                                    UserModel[] hostUsers = new UserModel[hostArray.length()];
                                    for (int j = 0; j < hostArray.length(); j++) {
                                        JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                        UserModel hostedBy = new UserModel();
                                        hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                        hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                        if (!eachHostInfo.getString("host_image").equals("")) {
                                            hostedBy.setImgUrl(res.getString("profileimage_default_url") + eachHostInfo.getString("host_image"));
                                        } else {
                                            hostedBy.setImgUrl("");
                                        }
                                        hostUsers[j] = hostedBy;
                                    }
                                    eventModel.setHostedBy(hostUsers);

                                    JSONObject eventDetails = eachObj.getJSONObject("event_detail");
                                    eventModel.setEventId(eventDetails.getString("event_id"));
                                    eventModel.setEventName(eventDetails.getString("event_name"));
                                    eventModel.setEventImage(res.getString("event_image_url") + eventDetails.getString("event_image"));
                                    eventModel.setEventAttendStatus(eventDetails.getString("attendance"));

                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eventDetails.getString("event_rate")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }

                                    String lat = eventDetails.getString("latitude");
                                    try {
                                        eventModel.setLatitude(Double.parseDouble(lat));

                                    } catch (Exception e) {
                                        eventModel.setLatitude(0);
                                    }
                                    String lng = eventDetails.getString("longitude");
                                    try {
                                        eventModel.setLongitude(Double.parseDouble(lng));
                                    } catch (Exception e) {
                                        eventModel.setLongitude(0);
                                    }

                                    eventModel.setEventDesc(eventDetails.getString("description"));
                                    eventModel.setEventDate(eventDetails.getString("event_date"));
                                    eventModel.setEventLocation(eventDetails.getString("location"));
                                    eventModel.setStatus(eventDetails.getString("status"));
                                    eventModel.setLiked(Boolean.parseBoolean(eventDetails.getString("like_status")));
                                    eventModel.setEventTime(eventDetails.getString("event_time"));
                                    eventModel.setEventCatId(eachObj.getString("event_category_id"));

                                    eventSuggestionList.add(eventModel);
                                }
                            }
                            feedsAdapter.setEventSuggestionList(eventSuggestionList);
                        } catch (JSONException e) {
                            Logger.e("userEventSuggestionsTask error ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "userEventSuggestionsTask");
    }
}
