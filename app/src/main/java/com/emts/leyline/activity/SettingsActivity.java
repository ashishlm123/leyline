package com.emts.leyline.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class SettingsActivity extends AppCompatActivity implements EditText.OnEditorActionListener {
    PreferenceHelper helper;
    EditText userName, userEmail, userBio, userWebiste, userHometown;
    Calendar cal;
    String gender;
    StringBuilder interestConcat;
    private int day, month, year;
    boolean[] checkedColors;
    TextView errName, errEmail, errBio, errDob, edtDob, errWebsite, errHomeTown;
    TextView profileEveryOne, profileFriends, profileOnlyMe, friendsEveryOne, friendsFriends,
            friendsOnlyMe, eventsEveryOne, eventsFriends, eventsOnlyMe;
    TextView profilePrivacy, friendsPrivacy, eventsPrivacy;
    String privacyFriend, privacyEvent, privacyProfile;
    private static String EVERYONE = "everyone";
    private static String FRIENDS = "friends";
    private static String ONLYME = "only me";
    public static String INTEREST_CONCAT_TEXT = "<-->";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_settings);

        privacyProfile = helper.getString(PreferenceHelper.USER_PROFILE_PRIVACY, EVERYONE);
        privacyFriend = helper.getString(PreferenceHelper.USER_FRIEND_PRIVACY, FRIENDS);
        privacyEvent = helper.getString(PreferenceHelper.USER_EVENT_PRIVACY, FRIENDS);

        profilePrivacy = findViewById(R.id.profile_privacy);
        profilePrivacy.setText(privacyProfile);

        friendsPrivacy = findViewById(R.id.friends_privacy);
        friendsPrivacy.setText(privacyFriend);

        eventsPrivacy = findViewById(R.id.events_privacy);
        eventsPrivacy.setText(privacyEvent);

        errName = findViewById(R.id.error_name);
        errBio = findViewById(R.id.error_bio);
        errEmail = findViewById(R.id.error_email);
        errDob = findViewById(R.id.error_dob);
        errWebsite = findViewById(R.id.error_website);
        errHomeTown = findViewById(R.id.errorHometown);

        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Settings");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        userName = findViewById(R.id.full_name);
        userName.setText(helper.getUserName());
        userName.setOnEditorActionListener(this);

        edtDob = findViewById(R.id.date_of_birth);
        edtDob.setText(helper.getUserDob());
        edtDob.setInputType(InputType.TYPE_NULL);
        edtDob.setEnabled(false);

        userEmail = findViewById(R.id.email_address);
        userEmail.setText(helper.getUserEmail());
        userEmail.setOnEditorActionListener(this);

        userHometown = findViewById(R.id.etHomeTown);
        userHometown.setText(helper.getUserHomeTown());
        userHometown.setOnEditorActionListener(this);

        userBio = findViewById(R.id.user_bio);
        userBio.setText(helper.getUserBio());
        userBio.setOnEditorActionListener(this);

        userWebiste = findViewById(R.id.user_website);
        userWebiste.setText(helper.getUserWebsite());
        userWebiste.setOnEditorActionListener(this);

        LinearLayout holderName = findViewById(R.id.holder_full_name);
        holderName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName.requestFocus();
            }
        });

        LinearLayout holderEmail = findViewById(R.id.holder_email);
        holderEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userEmail.requestFocus();
            }
        });



        RadioGroup rgGender = findViewById(R.id.rgGender);
        RadioButton rbMale = findViewById(R.id.rbMale);
        RadioButton rbFemale = findViewById(R.id.rbFemale);
        if (PreferenceHelper.getInstance(this).getString(PreferenceHelper.USER_GENDER, "M").equalsIgnoreCase("M")) {
            rgGender.check(R.id.rbMale);
        } else {
            rgGender.check(R.id.rbFemale);
        }
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (NetworkUtils.isInNetwork(SettingsActivity.this)) {
                    String gender = i == R.id.rbMale ? "M" : "F";
                    userProfileUpdateTask("", "", gender , "", "", "","","N/A");
                } else {
                    AlertUtils.showToast(getApplicationContext(), "No internet connection...");
                }
            }
        });

        LinearLayout holderHometown = findViewById(R.id.holderHomeTown);
        holderHometown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userHometown.requestFocus();
            }
        });


        LinearLayout holderBio = findViewById(R.id.holder_bio);
        holderBio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userBio.requestFocus();
            }
        });

        LinearLayout holderDOB = findViewById(R.id.holder_dob);
        holderDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker();
            }
        });

        LinearLayout holderMobile = findViewById(R.id.holder_mobile);
        holderMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingsActivity.this, "TO: DO", Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayout holderInterests = findViewById(R.id.holder_interests);
        holderInterests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] userInterests = getResources().getStringArray(R.array.user_interests);
                checkedColors = new boolean[userInterests.length];
                showListAlert(userInterests, checkedColors);
            }
        });

        LinearLayout holderChangePswd = findViewById(R.id.holder_change_password);
        holderChangePswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout holderProfile = findViewById(R.id.holder_profile);
        holderProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                privacyAlert();
            }
        });

        LinearLayout holderFriends = findViewById(R.id.holder_friends);
        holderFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                privacyAlert();
            }
        });

        LinearLayout holderEvents = findViewById(R.id.holder_events);
        holderEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                privacyAlert();
            }
        });

        TextView support = findViewById(R.id.support);
        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ContactUsActivity.class));
            }
        });

        TextView privacyPolicy = findViewById(R.id.privacy_policy);
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MoreInfoActivity.class);
                intent.putExtra("isPrivacy", true);
                startActivity(intent);
            }
        });

        TextView termsOfServices = findViewById(R.id.terms_of_services);
        termsOfServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MoreInfoActivity.class);
                intent.putExtra("isTerms", true);
                startActivity(intent);
            }
        });

        TextView licensces = findViewById(R.id.licensces);
        licensces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MoreInfoActivity.class);
                intent.putExtra("isLicensces", true);
                startActivity(intent);
            }
        });

        TextView logOut = findViewById(R.id.logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.edit().clear().commit();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    public void datePicker() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Logger.e("hello", "hello");
                if (!view.isShown()) {
                    return;
                }
                day = dayOfMonth;
                month = monthOfYear + 1;
                SettingsActivity.this.year = year;
                edtDob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                if (validationDob()) {
                    if (NetworkUtils.isInNetwork(SettingsActivity.this)) {
                        userProfileUpdateTask("", "", "", "", edtDob.getText().toString().trim(), "", "", "N/A");
                    } else {
                        AlertUtils.showToast(SettingsActivity.this, "No internet connection...");
                    }
                }
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.show();
    }

    private void privacyAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(SettingsActivity.this);
        View view = LayoutInflater.from(SettingsActivity.this).inflate(R.layout.alert_privacy_settings, null, false);
        dialog.setView(view);

        privacyProfile = helper.getString(PreferenceHelper.USER_PROFILE_PRIVACY, EVERYONE);
        privacyFriend = helper.getString(PreferenceHelper.USER_FRIEND_PRIVACY, FRIENDS);
        privacyEvent = helper.getString(PreferenceHelper.USER_EVENT_PRIVACY, FRIENDS);

        profileEveryOne = view.findViewById(R.id.profile_everyone);
        profileEveryOne.setOnClickListener(onClickListener);

        profileFriends = view.findViewById(R.id.profile_friends);
        profileFriends.setOnClickListener(onClickListener);

        profileOnlyMe = view.findViewById(R.id.profile_only_me);
        profileOnlyMe.setOnClickListener(onClickListener);

        friendsEveryOne = view.findViewById(R.id.friends_everyone);
        friendsEveryOne.setOnClickListener(onClickListener);

        friendsFriends = view.findViewById(R.id.friends_friends);
        friendsFriends.setOnClickListener(onClickListener);

        friendsOnlyMe = view.findViewById(R.id.friends_only_me);
        friendsOnlyMe.setOnClickListener(onClickListener);

        eventsEveryOne = view.findViewById(R.id.events_everyone);
        eventsEveryOne.setOnClickListener(onClickListener);

        eventsFriends = view.findViewById(R.id.events_friends);
        eventsFriends.setOnClickListener(onClickListener);

        eventsOnlyMe = view.findViewById(R.id.events_only_me);
        eventsOnlyMe.setOnClickListener(onClickListener);

        final Dialog dialog1 = dialog.show();
        Button cancel = view.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

        Button update = view.findViewById(R.id.btn_update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(SettingsActivity.this)) {
                    userProfilePrivacyUpdateTask(privacyProfile, privacyFriend, privacyEvent);
                    dialog1.dismiss();
                } else {
                    AlertUtils.showToast(SettingsActivity.this, getResources().getString(R.string.no_internet));
                }
            }
        });

        if (privacyProfile.equals(EVERYONE) || privacyProfile.equals("1")) {
            selectPrivacy(profileEveryOne.getId());
        } else if (privacyProfile.equals(FRIENDS) || privacyProfile.equals("2")) {
            selectPrivacy(profileFriends.getId());
        } else if (privacyProfile.equals(ONLYME) || privacyProfile.equals("3")) {
            selectPrivacy(profileOnlyMe.getId());
        }

        if (privacyFriend.equals(EVERYONE) || privacyFriend.equals("1")) {
            selectPrivacy(friendsEveryOne.getId());
        } else if (privacyFriend.equals(FRIENDS) || privacyFriend.equals("2")) {
            selectPrivacy(friendsFriends.getId());
        } else if (privacyFriend.equals(ONLYME) || privacyFriend.equals("3")) {
            selectPrivacy(friendsOnlyMe.getId());
        }

        if (privacyEvent.equals(EVERYONE) || privacyEvent.equals("1")) {
            selectPrivacy(eventsEveryOne.getId());
        } else if (privacyEvent.equals(FRIENDS) || privacyEvent.equals("2")) {
            selectPrivacy(eventsFriends.getId());
        } else if (privacyEvent.equals(ONLYME) || privacyEvent.equals("3")) {
            selectPrivacy(eventsOnlyMe.getId());
        }


    }

    private void userProfilePrivacyUpdateTask(String privacyProfile, String privacyFriend, String privacyEvent) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(SettingsActivity.this, "Please Wait");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        if (privacyFriend == null) {
            postMap.put("friend_privacy", "2");
        } else {
            postMap.put("friend_privacy", privacyFriend);
        }

        if (privacyEvent == null) {
            postMap.put("event_privacy", "2");
        } else {
            postMap.put("event_privacy", privacyEvent);
        }

        if (privacyProfile == null) {
            postMap.put("profile_privacy", "1");
        } else {
            postMap.put("profile_privacy", privacyProfile);
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().userPrivacyUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject object = res.getJSONObject("privacy");
                                if (object.getString("profile_privacy").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, EVERYONE).commit();
                                    profilePrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_PROFILE_PRIVACY, ""));
                                } else if (object.getString("profile_privacy").equals("2")) {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, FRIENDS).commit();
                                    profilePrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_PROFILE_PRIVACY, ""));
                                } else if (object.getString("profile_privacy").equals("3")) {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, ONLYME).commit();
                                    profilePrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_PROFILE_PRIVACY, ""));
                                } else {
                                    helper.edit().putString(PreferenceHelper.USER_PROFILE_PRIVACY, "everyone").commit();
                                    profilePrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_PROFILE_PRIVACY, ""));
                                }

                                if (object.getString("friend_privacy").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, EVERYONE).commit();
                                    friendsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_FRIEND_PRIVACY, ""));
                                } else if (object.getString("friend_privacy").equals("2")) {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, FRIENDS).commit();
                                    friendsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_FRIEND_PRIVACY, ""));
                                } else if (object.getString("friend_privacy").equals("3")) {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, ONLYME).commit();
                                    friendsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_FRIEND_PRIVACY, ""));
                                } else {
                                    helper.edit().putString(PreferenceHelper.USER_FRIEND_PRIVACY, "only me").commit();
                                    friendsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_FRIEND_PRIVACY, ""));
                                }

                                if (object.getString("event_privacy").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, EVERYONE).commit();
                                    eventsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_EVENT_PRIVACY, ""));
                                } else if (object.getString("event_privacy").equals("2")) {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, FRIENDS).commit();
                                    eventsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_EVENT_PRIVACY, ""));
                                } else if (object.getString("event_privacy").equals("3")) {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, ONLYME).commit();
                                    eventsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_EVENT_PRIVACY, ""));
                                } else {
                                    helper.edit().putString(PreferenceHelper.USER_EVENT_PRIVACY, "only me").commit();
                                    eventsPrivacy.setText(PreferenceHelper.getInstance(SettingsActivity.this).
                                            getString(PreferenceHelper.USER_EVENT_PRIVACY, ""));
                                }
                                AlertUtils.showErrorAlert(SettingsActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                    }
                                });

                            } else {
                                AlertUtils.showErrorAlert(SettingsActivity.this, res.getString("message"), new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            Logger.e("userLoginTask error ex", e.getMessage() + "");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                    }
                }, "userProfilePrivacyUpdateTask");
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            selectPrivacy(view.getId());
        }
    };

    private void selectPrivacy(int id) {
        switch (id) {
            case R.id.profile_everyone:
                profileEveryOne.setBackgroundResource(R.color.colorAccent);
                profileEveryOne.setTextColor(getResources().getColor(R.color.white));
                profileFriends.setBackgroundResource(android.R.color.transparent);
                profileFriends.setTextColor(getResources().getColor(R.color.appBlack));
                profileOnlyMe.setBackgroundResource(android.R.color.transparent);
                profileOnlyMe.setTextColor(getResources().getColor(R.color.appBlack));
                privacyProfile = "1";
                break;
            case R.id.profile_friends:
                profileFriends.setBackgroundResource(R.color.colorAccent);
                profileFriends.setTextColor(getResources().getColor(R.color.white));
                profileEveryOne.setBackgroundResource(android.R.color.transparent);
                profileEveryOne.setTextColor(getResources().getColor(R.color.appBlack));
                profileOnlyMe.setBackgroundResource(android.R.color.transparent);
                profileOnlyMe.setTextColor(getResources().getColor(R.color.appBlack));
                privacyProfile = "2";
                break;
            case R.id.profile_only_me:
                profileOnlyMe.setBackgroundResource(R.color.colorAccent);
                profileOnlyMe.setTextColor(getResources().getColor(R.color.white));
                profileFriends.setBackgroundResource(android.R.color.transparent);
                profileFriends.setTextColor(getResources().getColor(R.color.appBlack));
                profileEveryOne.setBackgroundResource(android.R.color.transparent);
                profileEveryOne.setTextColor(getResources().getColor(R.color.appBlack));
                privacyProfile = "3";
                break;

            case R.id.friends_everyone:
                friendsEveryOne.setBackgroundResource(R.color.colorAccent);
                friendsEveryOne.setTextColor(getResources().getColor(R.color.white));
                friendsFriends.setBackgroundResource(android.R.color.transparent);
                friendsFriends.setTextColor(getResources().getColor(R.color.appBlack));
                friendsOnlyMe.setBackgroundResource(android.R.color.transparent);
                friendsOnlyMe.setTextColor(getResources().getColor(R.color.appBlack));
                privacyFriend = "1";
                break;
            case R.id.friends_friends:
                friendsFriends.setBackgroundResource(R.color.colorAccent);
                friendsFriends.setTextColor(getResources().getColor(R.color.white));
                friendsEveryOne.setBackgroundResource(android.R.color.transparent);
                friendsEveryOne.setTextColor(getResources().getColor(R.color.appBlack));
                friendsOnlyMe.setBackgroundResource(android.R.color.transparent);
                friendsOnlyMe.setTextColor(getResources().getColor(R.color.appBlack));
                privacyFriend = "2";
                break;
            case R.id.friends_only_me:
                friendsOnlyMe.setBackgroundResource(R.color.colorAccent);
                friendsOnlyMe.setTextColor(getResources().getColor(R.color.white));
                friendsEveryOne.setBackgroundResource(android.R.color.transparent);
                friendsEveryOne.setTextColor(getResources().getColor(R.color.appBlack));
                friendsFriends.setBackgroundResource(android.R.color.transparent);
                friendsFriends.setTextColor(getResources().getColor(R.color.appBlack));
                privacyFriend = "3";
                break;

            case R.id.events_everyone:
                eventsEveryOne.setBackgroundResource(R.color.colorAccent);
                eventsEveryOne.setTextColor(getResources().getColor(R.color.white));
                eventsFriends.setBackgroundResource(android.R.color.transparent);
                eventsFriends.setTextColor(getResources().getColor(R.color.appBlack));
                eventsOnlyMe.setBackgroundResource(android.R.color.transparent);
                eventsOnlyMe.setTextColor(getResources().getColor(R.color.appBlack));
                privacyEvent = "1";
                break;
            case R.id.events_friends:
                eventsFriends.setBackgroundResource(R.color.colorAccent);
                eventsFriends.setTextColor(getResources().getColor(R.color.white));
                eventsOnlyMe.setBackgroundResource(android.R.color.transparent);
                eventsOnlyMe.setTextColor(getResources().getColor(R.color.appBlack));
                eventsEveryOne.setBackgroundResource(android.R.color.transparent);
                eventsEveryOne.setTextColor(getResources().getColor(R.color.appBlack));
                privacyEvent = "2";
                break;
            case R.id.events_only_me:
                eventsOnlyMe.setBackgroundResource(R.color.colorAccent);
                eventsOnlyMe.setTextColor(getResources().getColor(R.color.white));
                eventsEveryOne.setBackgroundResource(android.R.color.transparent);
                eventsEveryOne.setTextColor(getResources().getColor(R.color.appBlack));
                eventsFriends.setBackgroundResource(android.R.color.transparent);
                eventsFriends.setTextColor(getResources().getColor(R.color.appBlack));
                privacyEvent = "3";
                break;
        }

    }

    private void userProfileUpdateTask(final String fullName, final String email,
                                       final String gender, final String homeTown,
                                       final String dateOfBirth, final String userWebsite,
                                       final String bio, final String interestList) {
        AlertUtils.hideInputMethod(SettingsActivity.this, edtDob);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

      /*  postMap.put("name", userName.getText().toString().trim());
        postMap.put("email", userEmail.getText().toString().trim());
        postMap.put("gender", String.valueOf(gender));
        postMap.put("hometown", userHometown.getText().toString().trim());
        postMap.put("dob_year", String.valueOf(year));
        postMap.put("dob_month", String.valueOf(month));
        postMap.put("dob_day", String.valueOf(day));
        postMap.put("bio", userBio.getText().toString().trim());
        postMap.put("interest_title", interestConcat.toString());
        postMap.put("website_url", userWebiste.getText().toString().trim());*/

        if (!TextUtils.isEmpty(fullName)) {
            postMap.put("name", fullName);
        }

        if (!TextUtils.isEmpty(email)) {
            postMap.put("email", email);
        }

        if (!TextUtils.isEmpty(gender)) {
            postMap.put("gender", gender);
        }
        if (!TextUtils.isEmpty(homeTown)) {
            postMap.put("hometown", homeTown);
        }

        if (!TextUtils.isEmpty(dateOfBirth)) {
            postMap.put("dob_year", String.valueOf(year));
            postMap.put("dob_month", String.valueOf(month));
            postMap.put("dob_day", String.valueOf(day));
        }

        if (!TextUtils.isEmpty(bio)) {
            postMap.put("bio", bio);
        }

        if (!interestList.equals("N/A")) {
            postMap.put("interest_title", interestList);
        }

        if (!TextUtils.isEmpty(userWebsite)) {
            postMap.put("website_url", userWebsite);
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().profileUpdate, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                              /*  helper.edit().putString(PreferenceHelper.APP_USER_NAME, "name").apply();
                                helper.edit().putString(PreferenceHelper.APP_USER_DOB, dateOfBirth).apply();
                                helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, email).apply();
                                helper.edit().putString(PreferenceHelper.APP_USER_BIO, bio).apply();
                                helper.edit().putString(PreferenceHelper.APP_USER_WEBSITE, userWebsite).apply();*/


                                if (!TextUtils.isEmpty(fullName)) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_NAME, fullName).apply();
                                }
                                if (!TextUtils.isEmpty(dateOfBirth)) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_DOB, dateOfBirth).apply();
                                }
                                if (!TextUtils.isEmpty(email)) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_EMAIL, email).apply();
                                }
                                if (!TextUtils.isEmpty(bio)) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_BIO, bio).apply();
                                }
                                /*if (!TextUtils.isEmpty(interestList)) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_INTERESTS, interestList.replace("|", INTEREST_CONCAT_TEXT)).apply();
                                    Logger.e("k xa ta intrest not empty", interestList.replace("|", INTEREST_CONCAT_TEXT));
                                } else */if (!interestList.equals("N/A")) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_INTERESTS, interestList.replace("|", INTEREST_CONCAT_TEXT)).apply();
                                    Logger.e("k xa ta intrest not na", interestList.replace("|", INTEREST_CONCAT_TEXT));

                                }
                                if (!TextUtils.isEmpty(userWebsite)) {
                                    helper.edit().putString(PreferenceHelper.APP_USER_WEBSITE, userWebsite).apply();
                                } /*else {
                                    helper.edit().putString(PreferenceHelper.APP_USER_WEBSITE, "").apply();
                                }*/
                                if (!TextUtils.isEmpty(gender)) {
                                    helper.edit().putString(PreferenceHelper.USER_GENDER, gender).apply();
                                } /*else {
                                    helper.edit().putString(PreferenceHelper.USER_GENDER, "").apply();
                                }*/
                                if (!TextUtils.isEmpty(homeTown)) {
                                    helper.edit().putString(PreferenceHelper.USER_HOME_TOWN, homeTown).apply();
                                }/* else {
                                    helper.edit().putString(PreferenceHelper.USER_HOME_TOWN, "").apply();
                                }*/

                                AlertUtils.showToast(SettingsActivity.this, res.getString("message"));
                            } else {
                                AlertUtils.showToast(SettingsActivity.this, res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("userProfileUpdateTask error ex", e.getMessage() + "");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(SettingsActivity.this, edtDob, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(SettingsActivity.this, "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(SettingsActivity.this, "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(SettingsActivity.this, "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(SettingsActivity.this, "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(SettingsActivity.this, "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userProfileUpdateTask");
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_DONE) {
            AlertUtils.hideInputMethod(SettingsActivity.this, errEmail);

            /*if (validationName() || validationEmail() || validationHometown() || validationBio() || validationWebsite()) {
                if (NetworkUtils.isInNetwork(this)) {
                    userProfileUpdateTask(userName.getText().toString().trim(), userEmail.getText().toString().trim(), gender, userHometown.getText().toString().trim(),
                            edtDob.getText().toString().trim(), userWebiste.getText().toString().trim(), userBio.getText().toString().trim(), "");

                }
            }*/

            switch (textView.getId()) {
                case R.id.full_name:
                    if (validationName()) {
                        if (NetworkUtils.isInNetwork(this)) {
                            userProfileUpdateTask(textView.getText().toString().trim(), "", "", "", "", "", "", "N/A");
                        } else {
                            AlertUtils.showToast(this, "No internet connection...");
                        }
                    }
                    break;

                case R.id.email_address:
                    if (validationEmail()) {
                        if (NetworkUtils.isInNetwork(this)) {
                            userProfileUpdateTask("", userEmail.getText().toString().trim(), "", "", "", "", "", "N/A");
                        } else {
                            AlertUtils.showToast(this, "No internet connection...");
                        }
                    }
                    break;

                case R.id.etHomeTown:
                    if (validationHometown()) {
                        if (NetworkUtils.isInNetwork(this)) {
                            userProfileUpdateTask("", "", "", userHometown.getText().toString().trim(), "", "", "", "N/A");
                        } else {
                            AlertUtils.showToast(this, "No internet connection...");
                        }
                    }
                    break;

                case R.id.user_bio:
                    if (validationBio()) {
                        if (NetworkUtils.isInNetwork(this)) {
                            userProfileUpdateTask("", "", "", "", "", "", userBio.getText().toString().trim(), "N/A");
                        } else {
                            AlertUtils.showToast(this, "No internet connection...");
                        }
                    }
                    break;

                case R.id.user_website:
                    if (validationWebsite()) {
                        if (NetworkUtils.isInNetwork(this)) {
                            userProfileUpdateTask("", "", "", "", "", userWebiste.getText().toString().trim(),
                                    "", "N/A");
                        } else {
                            AlertUtils.showToast(this, "No internet connection...");
                        }
                    }
            }
            return true;
        }
        return false;
    }

    private void showListAlert(final String[] interests, final boolean[] checkedInterests) {
        String savedInterests = helper.getString(PreferenceHelper.APP_USER_INTERESTS, "");
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        boolean wasInterest = false;
        // String array for alert dialog multi choice items
        for (int i = 0; i < interests.length; i++) {

            // Boolean array for initial selected items
            if (savedInterests.toLowerCase().contains(interests[i].toLowerCase())) {
                checkedInterests[i] = true;
                wasInterest = true;
            } else {
                checkedInterests[i] = false;
            }
        }

        // Convert the color array to list
        final List<String> colorsList = Arrays.asList(interests);

        // Set multiple choice items for alert dialog
                /*
                    AlertDialog.Builder setMultiChoiceItems(CharSequence[] items, boolean[]
                    checkedItems, DialogInterface.OnMultiChoiceClickListener listener)
                        Set a list of items to be displayed in the dialog as the content,
                        you will be notified of the selected item via the supplied listener.
                 */
                /*
                    DialogInterface.OnMultiChoiceClickListener
                    public abstract void onClick (DialogInterface dialog, int which, boolean isChecked)

                        This method will be invoked when an item in the dialog is clicked.

                        Parameters
                        dialog The dialog where the selection was made.
                        which The position of the item in the list that was clicked.
                        isChecked True if the click checked the item, else false.
                 */
        builder.setMultiChoiceItems(interests, checkedInterests, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                // Update the current focused item's checked status
                checkedInterests[which] = isChecked;

                // Get the current focused item
                String currentItem = colorsList.get(which);

                // Notify the current action
//                Toast.makeText(getApplicationContext(),
//                        currentItem + " " + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        // Specify the dialog is not cancelable
        builder.setCancelable(false);

        // Set a title for alert dialog
        builder.setTitle("Select Interests");

        // Set the positive/yes button click listener
        final boolean finalWasInterest = wasInterest;
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (NetworkUtils.isInNetwork(SettingsActivity.this)) {
                    StringBuilder interestConcat = new StringBuilder("");
                    for (int i = 0; i < checkedInterests.length; i++) {
                        boolean checked = checkedInterests[i];
                        if (checked) {
                            interestConcat.append(interests[i]).append(" | ");
                        }
                    }
                    if (interestConcat.length() >= 1 || finalWasInterest) {
                        if (interestConcat.length() > 0) {
                            interestConcat = new StringBuilder(interestConcat.substring(0, interestConcat.length() - 2));
                        }
                        userProfileUpdateTask("", "", "", "", "", "", "", interestConcat.toString());
                    }
                } else {
                    AlertUtils.showToast(SettingsActivity.this, "No Internet Connection...");
                }
            }
        });

        // Set the negative/no button click listener
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do something when click the negative button
            }
        });

        AlertDialog dialog = builder.create();
        // Display the alert dialog on interface
        dialog.show();

    }

    private boolean validationName() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userName.getText().toString().trim())) {
            isValid = false;
            errName.setVisibility(View.VISIBLE);
        } else {
            errName.setVisibility(View.GONE);
        }
        return isValid;
    }

    private boolean validationEmail() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userEmail.getText().toString().trim())) {
            isValid = false;
            errEmail.setVisibility(View.VISIBLE);
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(userEmail.getText().toString().trim()).matches()) {
                errEmail.setVisibility(View.GONE);
            } else {
                isValid = false;
                errEmail.setText("Invalid email address.");
                errEmail.setVisibility(View.VISIBLE);
            }
        }
        return isValid;
    }

    private boolean validationHometown() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userHometown.getText().toString().trim())) {
            isValid = false;
            errHomeTown.setVisibility(View.VISIBLE);
        } else {
            errHomeTown.setVisibility(View.GONE);
        }
        return isValid;
    }

    private boolean validationBio() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userBio.getText().toString().trim())) {
            isValid = false;
            errBio.setVisibility(View.VISIBLE);
        } else {
            errBio.setVisibility(View.GONE);
        }
        return isValid;
    }

    private boolean validationDob() {
        boolean isValid = true;
        if (TextUtils.isEmpty((edtDob.getText().toString().trim()))) {
            isValid = false;
            errDob.setVisibility(View.VISIBLE);
        } else {
            errDob.setVisibility(View.GONE);
        }
        return isValid;
    }

    private boolean validationWebsite() {
        boolean isValid = true;
        if (TextUtils.isEmpty((userWebiste.getText().toString().trim()))) {
            isValid = false;
            errWebsite.setVisibility(View.VISIBLE);
        } else {
            errWebsite.setVisibility(View.GONE);
        }
        return isValid;
    }


    @Override
    public void onBackPressed() {
        AlertUtils.hideInputMethod(SettingsActivity.this, edtDob);
        super.onBackPressed();
    }
}
