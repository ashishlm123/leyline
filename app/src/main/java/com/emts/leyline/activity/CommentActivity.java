package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.CommentAdapter;
import com.emts.leyline.adapter.MentionPersonAdapter;
import com.emts.leyline.customviews.EndlessRecyclerViewScrollListener;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.CommentModel;
import com.emts.leyline.models.FriendsModel;
import com.hendraanggrian.socialview.Hashtag;
import com.hendraanggrian.socialview.SocialView;
import com.hendraanggrian.widget.HashtagAdapter;
import com.hendraanggrian.widget.SocialAutoCompleteTextView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;

/**
 * Created by Srijana on 10/4/2017.
 */

public class CommentActivity extends AppCompatActivity {
    RecyclerView cmtListView;
    ProgressBar progressBar;//, infiniteProgressBar;
    PreferenceHelper helper;
    TextView errorText;
    Button btnPOST;
    CommentAdapter adapter;
    ArrayList<CommentModel> cmtList = new ArrayList<>();
    Toolbar toolbar;
    public String eventId;
    //    EditText edtComment;
    SocialAutoCompleteTextView edtComment;
    ArrayList<FriendsModel> mentionList = new ArrayList<>();
    ArrayList<String> friendId = new ArrayList<>();
    ArrayAdapter<Hashtag> hashtagAdapter;
    MentionPersonAdapter mentionAdapter;
    boolean isGetFriendList = false;
    boolean isGetFriendListInProgress = false;
//    EndlessRecyclerViewScrollListener infiniteScrollListener;

    JSONArray mentionArray = new JSONArray();

    int limit = 10;
    int offset = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_comment);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Comments");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //mention and hash tag input field
        edtComment = findViewById(R.id.edt_comment);
        edtComment.setThreshold(1);
        edtComment.setMentionEnabled(true);
        edtComment.setHashtagEnabled(true);

        hashtagAdapter = new HashtagAdapter(CommentActivity.this);
        edtComment.setHashtagAdapter(hashtagAdapter);

        mentionAdapter = new MentionPersonAdapter(this);
        edtComment.setMentionAdapter(mentionAdapter);

        edtComment.setHashtagTextChangedListener(new Function2<SocialView, CharSequence, Unit>() {
            @Override
            public Unit invoke(SocialView socialView, CharSequence charSequence) {
                Logger.e("Hash Tag text change listener", charSequence + " " + socialView.getText().toString());
                userHashTagTask(charSequence.toString());
                return null;
            }
        });
        edtComment.setMentionTextChangedListener(new Function2<SocialView, CharSequence, Unit>() {
            @Override
            public Unit invoke(SocialView socialView, CharSequence charSequence) {
                Logger.e("Mention text change listener", charSequence + " " + socialView.getText().toString());
                if (!isGetFriendList && !isGetFriendListInProgress) {
                    userFriendsListingTask();
                }
                return null;
            }
        });

        edtComment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getAdapter() instanceof MentionPersonAdapter) {
                    Logger.e("selected item ", "POSITION:" + i + " ID:" + l);

                    FriendsModel frnMdl = (FriendsModel) view.findViewById(R.id.user_name).getTag();
                    if (frnMdl == null) {
                        return;
                    }
                    Logger.e("my tag", frnMdl.getUserName() + " ");

                    try {
                        for (int k = 0; k < mentionArray.length(); k++) {
                            JSONObject kJsonObj = mentionArray.getJSONObject(k);
                            if (kJsonObj.getString("user_id").equalsIgnoreCase(frnMdl.getUserId())) {
                                return;
                            }
                        }

                        //my json here for the mentions
                        JSONObject mentionObj = new JSONObject();
                        mentionObj.put("username", frnMdl.getUserName());
                        mentionObj.put("user_id", frnMdl.getUserId());
                        mentionObj.put("image", frnMdl.getImgUrl());

                        mentionArray.put(mentionObj);

                        Logger.e("myMentionObj =", mentionArray.toString() + " ");
                    } catch (JSONException e) {
                        Logger.e("mentionObj create ex0", e.getMessage() + " ");
                    }
                }
            }
        });

        btnPOST = findViewById(R.id.btn_post_comment);
        RelativeLayout cmtHolder = findViewById(R.id.lay_comment);
        cmtListView = cmtHolder.findViewById(R.id.list_recycler);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        eventId = intent.getStringExtra("eventid");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CommentActivity.this,
                LinearLayoutManager.VERTICAL, false);
//        linearLayoutManager.setStackFromEnd(true);
        cmtListView.setLayoutManager(linearLayoutManager);
        cmtListView.setItemAnimator(null);

        adapter = new CommentAdapter(CommentActivity.this, cmtList);
        adapter.setLoadEarlierListener(new CommentAdapter.LoadEarlierMessages() {
            @Override
            public void onLoadEarlierMessages() {
//                if (offset >= limit && cmtList.size() >= limit) {
                userCommentListingTask(true, cmtList.size());
//                } else {
//                    adapter.setIsLoadMoreEnable(false);
//                    adapter.notifyItemChanged(0);
//                }
            }
        });
        cmtListView.setAdapter(adapter);
        progressBar = cmtHolder.findViewById(R.id.progress);

        errorText = cmtHolder.findViewById(R.id.tv_error_message);
        cmtListView.setVisibility(View.VISIBLE);

        if (NetworkUtils.isInNetwork(this)) {
            userCommentListingTask(false, cmtList.size());
        } else {
            errorText.setText(R.string.no_internet);
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            cmtListView.setVisibility(View.GONE);
        }

        btnPOST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(edtComment.getText().toString().trim())) {
                    if (NetworkUtils.isInNetwork(CommentActivity.this)) {
                        String mentionJsonArray = "[]";
                        try {
                            mentionJsonArray = getMentionObjects();
                        } catch (JSONException e) {
                            Logger.e("mentionArray final ex", e.getMessage() + " ");
                        }
                        userCommentTask(mentionJsonArray);
                        edtComment.setText("");
                        edtComment.clearFocus();
                        errorText.setVisibility(View.GONE);

                        //clear prev mentions
                        mentionArray = new JSONArray();
                    } else {
                        errorText.setText(R.string.no_internet);
                        errorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        cmtListView.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void userCommentListingTask(final boolean isLoadMore, final int listSizePrev) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("event_id", eventId);
        postMap.put("parent_id", "0");
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().feedsCommentListingUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray commentLists = res.getJSONArray("comment");
                                if (offset == 0) {
                                    cmtList.clear();
                                }
                                if (commentLists.length() == 0) {
                                    adapter.setIsLoadMoreEnable(false);
                                    adapter.notifyItemChanged(0);
                                    return;
                                }
                                for (int i = 0; i < commentLists.length(); i++) {
                                    CommentModel commentModel = new CommentModel();
                                    JSONObject eachObj = commentLists.getJSONObject(i);
                                    commentModel.setCmntId(eachObj.getString("comment_id"));
                                    commentModel.setCmntTime(eachObj.getString("comment_time"));
                                    commentModel.setUserName(eachObj.getString("name"));
                                    commentModel.setCmntBody(eachObj.getString("body"));
                                    commentModel.setReplyCount(Integer.parseInt(eachObj.getString("reply_count")));
                                    commentModel.setUserId(eachObj.getString("comment_by"));
                                    String profileImage = eachObj.getString("profile_image");
                                    if (!TextUtils.isEmpty(profileImage)) {
                                        commentModel.setImgUrl(res.getString("profileimage_default_url") + profileImage);
                                    } else {
                                        commentModel.setImgUrl("");
                                    }
                                    //mentions
                                    commentModel.setMentionJsonData(eachObj.getString("mention_users"));

                                    cmtList.add(0, commentModel);
                                }
                                cmtListView.setVisibility(View.VISIBLE);
                                if (cmtList.size() >= limit) {
                                    adapter.setIsLoadMoreEnable(true);
                                } else {
                                    adapter.setIsLoadMoreEnable(false);
                                }
                                if (listSizePrev == 0) {
                                    adapter.notifyDataSetChanged();
                                    Logger.e("scroll", "level 1");
                                } else {
                                    Logger.e("scroll", "level 2");
                                    adapter.notifyItemRangeInserted(1, limit);
                                    adapter.notifyItemChanged(0);
                                }

                                if (!isLoadMore || listSizePrev == 0) {
                                    cmtListView.smoothScrollToPosition(cmtList.size());
                                    Logger.e("scroll", "level 3");
                                } else {
                                    try {
                                        cmtListView.smoothScrollToPosition(offset - listSizePrev);
                                        Logger.e("scroll", "level 4");
                                    } catch (Exception e) {
                                        Logger.e("scroll to pos", e.getMessage() + " ");
                                    }
                                }

                                offset = cmtList.size();
                            } else {
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    cmtListView.setVisibility(View.GONE);
                                } else {
                                    cmtListView.setVisibility(View.VISIBLE);
//                                    AlertUtils.showSnack(CommentActivity.this, errorText, " No More Comments");
                                    adapter.setIsLoadMoreEnable(false);
                                    adapter.notifyItemChanged(0);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("userCommentListingTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userCommentListingTask error ex", e.getMessage() + "");
                        }
                        if (offset == 0) {
                            errorText.setText(errMsg);
                            errorText.setVisibility(View.VISIBLE);
                            cmtListView.setVisibility(View.GONE);
                        } else {
                            AlertUtils.showToast(CommentActivity.this, errMsg);
                        }
                    }
                }, "userCommentListingTask");
    }

    private void userCommentTask(String mentionJsonArray) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("user_id", helper.getUserId());
        postMap.put("event_id", eventId);
        postMap.put("parent_id", "0");

        Logger.e("chatFrag emoji", edtComment.getText().toString().trim() + "   --");
        String toServerUnicodeEncoded = StringEscapeUtils.escapeJava(edtComment.getText().toString().trim());
        Logger.e("chatFrag escapedString ", toServerUnicodeEncoded + "  --");
//        String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(toServerUnicodeEncoded);
//        Logger.e("chatFrag unescapedString", fromServerUnicodeDecoded + "  --");

        postMap.put("body", toServerUnicodeEncoded);
        if (cmtList.size() > 0) {
            postMap.put("last_comment_id", cmtList.get(cmtList.size() - 1).getCmntId());
        } else {
            postMap.put("last_comment_id", "0");
        }

        //get mentions if any
        postMap.put("mention_users", mentionJsonArray);

        vHelper.addVolleyRequestListeners(Api.getInstance().userCommentUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.showToast(CommentActivity.this, res.getString("message"));

                                JSONArray commentLists = res.getJSONArray("latest_comment");

                                if (commentLists.length() == 0) {
                                    adapter.setIsLoadMoreEnable(false);
                                    adapter.notifyItemChanged(0);
                                    return;
                                }
                                for (int i = 0; i < commentLists.length(); i++) {
                                    CommentModel commentModel = new CommentModel();
                                    JSONObject eachObj = commentLists.getJSONObject(i);
                                    commentModel.setCmntId(eachObj.getString("comment_id"));
                                    commentModel.setCmntTime(eachObj.getString("comment_time"));
                                    commentModel.setUserName(eachObj.getString("name"));
                                    commentModel.setCmntBody(eachObj.getString("body"));
                                    commentModel.setReplyCount(Integer.parseInt(eachObj.getString("reply_count")));
                                    commentModel.setUserId(eachObj.getString("comment_by"));
                                    String profileImage = eachObj.getString("profile_image");
                                    if (!TextUtils.isEmpty(profileImage)) {
                                        commentModel.setImgUrl(res.getString("profileimage_default_url") + profileImage);
                                    } else {
                                        commentModel.setImgUrl("");
                                    }
                                    //mentions
                                    commentModel.setMentionJsonData(eachObj.getString("mention_users"));

                                    cmtList.add(commentModel);
                                }

                                if (cmtList.size() >= limit) {
                                    adapter.setIsLoadMoreEnable(true);
                                } else {
                                    adapter.setIsLoadMoreEnable(false);
                                }
                                if (cmtList.size() > 0) {
                                    adapter.notifyDataSetChanged();
                                    cmtListView.setVisibility(View.VISIBLE);
                                } else {
                                    cmtListView.setVisibility(View.GONE);
                                }
                                try {
                                    cmtListView.smoothScrollToPosition(cmtList.size());
                                } catch (Exception e) {
                                }
                                offset = cmtList.size();
                            } else {
                                AlertUtils.showAlertMessage(CommentActivity.this, "Error !!!", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userCommentTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userCommentTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(CommentActivity.this, "Error !!!", errMsg);
                    }
                }, "userCommentTask");
    }

    public String getMentionObjects() throws JSONException {
        JSONArray finalMentionArray = new JSONArray();
        List<String> allMentions = edtComment.getMentions();
        for (String mention : allMentions) {
            Logger.e("mentions in input", mention + " *");
            for (int i = 0; i < mentionArray.length(); i++) {
                JSONObject eachMention = mentionArray.getJSONObject(i);
                if (mention.equals(eachMention.getString("username").replace(" ", MentionPersonAdapter.MENTION_CONCAT_TEXT))) {
                    finalMentionArray.put(eachMention);
                }
            }
        }
        return finalMentionArray.toString();
    }

    private void userFriendsListingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("profile_id", helper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserFriendlists, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        isGetFriendListInProgress = false;
                        mentionList.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray friendsArray = res.getJSONArray("friend_list");
                                for (int i = 0; i < friendsArray.length(); i++) {
                                    JSONObject jsonObject = friendsArray.getJSONObject(i);
                                    if (jsonObject.getString("friend_status").equalsIgnoreCase("isfriend")) {
                                        FriendsModel friendsModel = new FriendsModel();
                                        friendsModel.setFriends(true);
                                        friendsModel.setUserName(jsonObject.getString("name"));
                                        friendsModel.setUserId(jsonObject.getString("friend_id"));
                                        friendsModel.setImgUrl(res.getString("default_profile_img_Url") + jsonObject.getString("profile_image"));
                                        String threadId = jsonObject.getString("thread_id");
                                        if (threadId.equals("") || threadId.equals("null") || threadId.equals("0")) {
                                            friendsModel.setThreadId("0");
                                        } else {
                                            friendsModel.setThreadId(threadId);
                                        }
                                        mentionList.add(friendsModel);
                                    }
                                }
                                setMentionAutoCompleteAdapter(mentionList);
                            }
                        } catch (JSONException e) {
                            Logger.e("userFriendsListingTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        isGetFriendListInProgress = false;
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userFriendsListingTask error ex", e.getMessage() + "");
                        }
                    }
                }, "userFriendsListingTask");
        isGetFriendListInProgress = true;
    }

    private void setMentionAutoCompleteAdapter(ArrayList<FriendsModel> mentionList) {
        isGetFriendList = true;
        if (mentionList.size() > 0) {
            try {
                mentionAdapter.addAll(mentionList);
                mentionAdapter.notifyDataSetChanged();
                mentionAdapter.getFilter().filter(edtComment.getText(), null);
            } catch (Exception e) {
                Logger.e("setMention autoComplete adapter", e.getMessage() + " ");
            }
        }
    }

    private void userHashTagTask(String hashKey) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("hash_key", hashKey);

        vHelper.addVolleyRequestListeners(Api.getInstance().hashTagUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray hashArray = res.getJSONArray("hash_keys");
                        hashtagAdapter.clear();

                        ArrayList<Hashtag> allHashTag = new ArrayList<>();
                        for (int i = 0; i < hashArray.length(); i++) {
                            JSONObject eachHashTag = hashArray.getJSONObject(i);
                            allHashTag.add(new Hashtag(eachHashTag.getString("hash_key"), Integer.parseInt(eachHashTag.getString("count"))));
                        }
                        hashtagAdapter.addAll(allHashTag);
//                        edtComment.setHashtagAdapter(hashtagAdapter);
                        hashtagAdapter.notifyDataSetChanged();
                        hashtagAdapter.getFilter().filter(edtComment.getText(), null);

                    } else {

                    }
                } catch (JSONException e) {
                    Logger.e("userNotificationsTask error ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userNotificationsTask error ex", e.getMessage() + "");
                }
            }
        }, "userHashTagTask");
    }


    @Override
    public void onBackPressed() {
        AlertUtils.hideInputMethod(CommentActivity.this, errorText);
        super.onBackPressed();
    }
}
