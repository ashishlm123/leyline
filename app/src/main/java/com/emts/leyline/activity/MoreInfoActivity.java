package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MoreInfoActivity extends AppCompatActivity {
    Intent intent;
    PreferenceHelper helper;
    TextView textView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_more_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        textView = (TextView) findViewById(R.id.app_info);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        intent = getIntent();

        if (intent == null) {
            return;
        }

        if (intent.getBooleanExtra("isPrivacy", false)) {
            toolbar.setTitle("Privacy Policy");
            if (NetworkUtils.isInNetwork(this)) {
                userPrivacyPolicyTask();
            } else {
                textView.setText(R.string.no_internet);
                textView.setVisibility(View.VISIBLE);
            }
        } else if (intent.getBooleanExtra("isTerms", false)) {
            toolbar.setTitle("Terms Of Services");
            if (NetworkUtils.isInNetwork(this)) {
                userTermsOfServicesTask();
            } else {
                textView.setText(R.string.no_internet);
                textView.setVisibility(View.VISIBLE);
            }
        } else if (intent.getBooleanExtra("isLicensces", false)) {
            toolbar.setTitle("Licensces");
            if (NetworkUtils.isInNetwork(this)) {
                userLicenscesTask();
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText(R.string.no_internet);
            }
        }

    }

    private void userPrivacyPolicyTask() {
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().privacyPolicy, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONObject privacyPolicy = res.getJSONObject("privacy");
                        textView.setText(Html.fromHtml(privacyPolicy.getString("content")));
                        textView.setVisibility(View.VISIBLE);

                    } else {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(res.getString("message"));
                        textView.setTextColor(getResources().getColor(R.color.c_red));
                    }
                } catch (JSONException e) {
                    Logger.e("userPrivacyPolicyTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userPrivacyPolicyTask error ex", e.getMessage() + "");
                }
                textView.setText(errMsg);
                textView.setVisibility(View.VISIBLE);
            }
        }, "userPrivacyPolicyTask");
    }

    private void userTermsOfServicesTask() {
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().termsOfServices, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONObject termsOfServices = res.getJSONObject("terms");
                        textView.setText(Html.fromHtml(termsOfServices.getString("content")));
                        textView.setVisibility(View.VISIBLE);
                    } else {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(res.getString("message"));
                        textView.setTextColor(getResources().getColor(R.color.c_red));
                    }
                } catch (JSONException e) {
                    Logger.e("userTermsOfServicesTask error ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userTermsOfServicesTask error ex", e.getMessage() + "");
                }
                textView.setText(errMsg);
                textView.setVisibility(View.VISIBLE);
            }
        }, "userTermsOfServicesTask");
    }

    private void userLicenscesTask() {
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().licensces, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONObject licensces = res.getJSONObject("licencing");
                        textView.setText(Html.fromHtml(licensces.getString("content")));
                        textView.setVisibility(View.VISIBLE);
                    } else {
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(res.getString("message"));
                        textView.setTextColor(getResources().getColor(R.color.c_red));
                    }
                } catch (JSONException e) {
                    Logger.e("userLicenscesTask error ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userLicenscesTask error ex", e.getMessage() + "");
                }
                textView.setText(errMsg);
                textView.setVisibility(View.VISIBLE);
            }
        }, "userLicenscesTask");
    }
}

