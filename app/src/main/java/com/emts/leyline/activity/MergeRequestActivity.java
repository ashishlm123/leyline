package com.emts.leyline.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.DateUtils;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.MyTextUtils;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.NotificationModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MergeRequestActivity extends AppCompatActivity {
    TextView userName, eventName, eventDate, hostName, eventDescription, eventTime, eventLocation;
    ImageView eventImage;
    Button acceptMerge, declineMerge, mergeStatus;
    EventModel eventModel;
    NotificationModel notificationModel;
    int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_request);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        toolbar.setTitle("Event Merge Request");

        userName = findViewById(R.id.user_name);
        eventName = findViewById(R.id.event_name);
        eventDate = findViewById(R.id.event_date);
        hostName = findViewById(R.id.hosted_by);
        eventDescription = findViewById(R.id.event_description);
        eventTime = findViewById(R.id.event_time);
        eventLocation = findViewById(R.id.event_location);

        eventImage = findViewById(R.id.event_icon);

        acceptMerge = findViewById(R.id.accept_merge_request);
        declineMerge = findViewById(R.id.decline_merge_request);
        mergeStatus = findViewById(R.id.status);

        Intent intent = getIntent();
        notificationModel = (NotificationModel) intent.getSerializableExtra("notificationmodel");
        eventModel = (EventModel) intent.getSerializableExtra("eventmodel");
        position = intent.getIntExtra("position", -1);

        userName.setText(notificationModel.getNotiUser().getUserName());
        eventName.setText(eventModel.getEventName());
        eventDate.setText(DateUtils.convertDate(eventModel.getEventDate()));
        hostName.setText(MyTextUtils.checkNull(eventModel.getHostedBy()[0].getUserName()));
        eventDescription.setText(eventModel.getEventDesc());
        eventTime.setText(eventModel.getEventTime());
        eventLocation.setText(eventModel.getEventLocation());

        if (!TextUtils.isEmpty(eventModel.getEventImage())) {
            Picasso.with(MergeRequestActivity.this).load(eventModel.getEventImage()).into(eventImage);
        }

        if (notificationModel.getMergeReqStat().equals(NotificationModel.MERGE_REJECTED)) {
            mergeStatus.setText("Merge Rejected");
            mergeStatus.setBackgroundResource(R.color.merge_decline_color);
            mergeStatus.setVisibility(View.VISIBLE);
            acceptMerge.setVisibility(View.GONE);
            declineMerge.setVisibility(View.GONE);

        } else if (notificationModel.getMergeReqStat().equals(NotificationModel.MERGE_ACCEPTED)) {
            mergeStatus.setText("Merge Accepted");
            mergeStatus.setBackgroundResource(R.color.progress_green);
            mergeStatus.setVisibility(View.VISIBLE);
            acceptMerge.setVisibility(View.GONE);
            declineMerge.setVisibility(View.GONE);
        } else {
            mergeStatus.setVisibility(View.GONE);
        }

        acceptMerge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(MergeRequestActivity.this)) {
                    userAcceptDeclineMergeTask("accepted", " accepted");
                } else {
                    AlertUtils.showToast(MergeRequestActivity.this, getString(R.string.no_internet));
                }

            }
        });

        declineMerge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(MergeRequestActivity.this)) {
                    userAcceptDeclineMergeTask("rejected", " declined");
                } else {
                    AlertUtils.showToast(MergeRequestActivity.this, getString(R.string.no_internet));
                }
            }
        });

    }

    private void userAcceptDeclineMergeTask(final String requestStat, final String type) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(MergeRequestActivity.this, "Please wait..");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("merger_id", notificationModel.getNotiUser().getUserId());
        postMap.put("notification_id", notificationModel.getNotId());
        postMap.put("event_id", eventModel.getEventId());
        postMap.put("request_stat", requestStat);

        vHelper.addVolleyRequestListeners(Api.getInstance().acceptRejectMergeUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        Intent intent = new Intent();
                        notificationModel.setMergeReqStat(requestStat);
                        intent.putExtra("notification", notificationModel);
                        intent.putExtra("position", position);
                        setResult(RESULT_OK, intent);

                        AlertUtils.showErrorAlert(MergeRequestActivity.this, "Merge request has been" + type, new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                MergeRequestActivity.this.finish();
                            }
                        });
                    }
                } catch (JSONException e) {
                    Logger.e("userAcceptDeclineMergeTask error ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userAcceptDeclineMergeTask error ex", e.getMessage() + "");
                }
                AlertUtils.showAlertMessage(MergeRequestActivity.this, "Error !!!", errMsg);
            }
        }, "userAcceptDeclineMergeTask");
    }
}
