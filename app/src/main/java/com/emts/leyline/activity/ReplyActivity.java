package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.leyline.R;
import com.emts.leyline.adapter.CommentAdapter;
import com.emts.leyline.adapter.MentionPersonAdapter;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.CommentModel;
import com.emts.leyline.models.FriendsModel;
import com.hendraanggrian.socialview.Hashtag;
import com.hendraanggrian.socialview.SocialView;
import com.hendraanggrian.widget.HashtagAdapter;
import com.hendraanggrian.widget.SocialAutoCompleteTextView;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class ReplyActivity extends AppCompatActivity {
    ProgressBar progressBar;//, infiniteProgressBar;
    //    EndlessRecyclerViewScrollListener infiniteScrollListner;
    RecyclerView replyListView;
    TextView errorText;
    CommentModel commentModel;
    ImageView profilePic;
    TextView userName, cmntBody, cmntTime, cmntReplies;
    Button btnReply;
    ArrayList<CommentModel> repList = new ArrayList<>();
    PreferenceHelper helper = PreferenceHelper.getInstance(this);
    String eventid;
    int limit = 4;
    int offset = 0;
    CommentAdapter adapter;

    SocialAutoCompleteTextView repComment;
    ArrayList<FriendsModel> mentionList = new ArrayList<>();
    ArrayAdapter<Hashtag> hashtagAdapter;
    MentionPersonAdapter mentionAdapter;
    boolean isGetFriendList = false;
    boolean isGetFriendListInProgress = false;

    JSONArray mentionArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Replies");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        commentModel = (CommentModel) intent.getSerializableExtra("comment");
        eventid = intent.getStringExtra("eventid");

        cmntReplies = findViewById(R.id.cmnt_replies);
        cmntReplies.setVisibility(View.GONE);

        profilePic = findViewById(R.id.friends_profile_pic);
        userName = findViewById(R.id.friend_name);
        cmntBody = findViewById(R.id.comment_body);
        cmntTime = findViewById(R.id.cmt_time);

        Picasso.with(getApplicationContext()).load((commentModel.getImgUrl())).into(profilePic);
        userName.setText(commentModel.getUserName());
        cmntBody.setText(StringEscapeUtils.unescapeJava(commentModel.getCmntBody()));
        cmntTime.setText(commentModel.getCmntTime());

        RelativeLayout replyHolder = findViewById(R.id.lay_comment_replies);
        replyListView = replyHolder.findViewById(R.id.list_recycler);
        progressBar = replyHolder.findViewById(R.id.progress);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReplyActivity.this);
        replyListView.setLayoutManager(linearLayoutManager);
        replyListView.setItemAnimator(null);

        adapter = new CommentAdapter(ReplyActivity.this, repList);
        adapter.isReplied(true);
        adapter.setLoadEarlierListener(new CommentAdapter.LoadEarlierMessages() {
            @Override
            public void onLoadEarlierMessages() {
//                if (repList.size() >= limit) {
                userReplyListingTask(true, repList.size());
//                } else {
//                    adapter.setIsLoadMoreEnable(false);
//                    adapter.notifyItemChanged(0);
//                }
            }
        });
        replyListView.setAdapter(adapter);

        if (NetworkUtils.isInNetwork(this)) {
            userReplyListingTask(false, repList.size());
        } else {
            errorText.setText(R.string.no_internet);
            errorText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            replyListView.setVisibility(View.GONE);
        }

        repComment = findViewById(R.id.rep_comment);
        errorText = replyHolder.findViewById(R.id.tv_error_message);
        repComment.setThreshold(1);
        repComment.setMentionEnabled(true);
        repComment.setHashtagEnabled(true);

        hashtagAdapter = new HashtagAdapter(this);
        repComment.setHashtagAdapter(hashtagAdapter);

        mentionAdapter = new MentionPersonAdapter(this);
        repComment.setMentionAdapter(mentionAdapter);

        repComment.setHashtagTextChangedListener(new Function2<SocialView, CharSequence, Unit>() {
            @Override
            public Unit invoke(SocialView socialView, CharSequence charSequence) {
                Logger.e("Hash Tag text change listener", charSequence + " " + socialView.getText().toString());
                userHashTagTask(charSequence.toString());
                return null;
            }
        });
        repComment.setMentionTextChangedListener(new Function2<SocialView, CharSequence, Unit>() {
            @Override
            public Unit invoke(SocialView socialView, CharSequence charSequence) {
                Logger.e("Mention text change listener", charSequence + " " + socialView.getText().toString());
                if (!isGetFriendList && !isGetFriendListInProgress) {
                    userFriendsListingTask();
                }
                return null;
            }
        });
        repComment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getAdapter() instanceof MentionPersonAdapter) {
                    Logger.e("selected item ", "POSITION:" + i + " ID:" + l);

                    FriendsModel frnMdl = (FriendsModel) view.findViewById(R.id.user_name).getTag();
                    if (frnMdl == null) {
                        return;
                    }
                    Logger.e("my tag", frnMdl.getUserName() + " ");

                    try {
                        //my json here for the mentions
                        JSONObject mentionObj = new JSONObject();
                        mentionObj.put("username", frnMdl.getUserName());
                        mentionObj.put("user_id", frnMdl.getUserId());
                        mentionObj.put("image", frnMdl.getImgUrl());

                        mentionArray.put(mentionObj);

                        Logger.e("myMentionObj =", mentionArray.toString() + " ");
                    } catch (JSONException e) {
                        Logger.e("mentionObj create ex0", e.getMessage() + " ");
                    }
                }
            }
        });


        btnReply = findViewById(R.id.btn_reply_comment);
        btnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(repComment.getText().toString().trim())) {
                    if (NetworkUtils.isInNetwork(ReplyActivity.this)) {

                        String mentionJsonArray = "[]";
                        try {
                            mentionJsonArray = getMentionObjects();
                        } catch (JSONException e) {
                            Logger.e("mentionArray final ex", e.getMessage() + " ");
                        }
                        userReplyTask(mentionJsonArray);
                        repComment.setText("");
                        repComment.clearFocus();
                        errorText.setVisibility(View.GONE);

                        //clear prev mention
                        mentionArray = new JSONArray();
                    } else {
                        errorText.setText(R.string.no_internet);
                        errorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        replyListView.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    public String getMentionObjects() throws JSONException {
        JSONArray finalMentionArray = new JSONArray();
        List<String> allMentions = repComment.getMentions();
        for (String mention : allMentions) {
            Logger.e("mentions in input", mention + " *");
            for (int i = 0; i < mentionArray.length(); i++) {
                JSONObject eachMention = mentionArray.getJSONObject(i);
                if (mention.equals(eachMention.getString("username").replace(" ", MentionPersonAdapter.MENTION_CONCAT_TEXT))) {
                    finalMentionArray.put(eachMention);
                }
            }
        }
        return finalMentionArray.toString();
    }

    private void userReplyTask(String mentionJsonArray) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        Logger.e("chatFrag emoji", repComment.getText().toString().trim() + "   --");
        String toServerUnicodeEncoded = StringEscapeUtils.escapeJava(repComment.getText().toString().trim());
        Logger.e("chatFrag escapedString ", toServerUnicodeEncoded + "  --");
        String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(toServerUnicodeEncoded);
        Logger.e("chatFrag unescapedString", fromServerUnicodeDecoded + "  --");

        postMap.put("user_id", helper.getUserId());
        postMap.put("body", toServerUnicodeEncoded);
        postMap.put("parent_id", commentModel.getCmntId());
        postMap.put("event_id", eventid);
        if (repList.size() > 0) {
            postMap.put("last_comment_id", repList.get(repList.size() - 1).getCmntId());
        } else {
            postMap.put("last_comment_id", "0");
        }

        //mentions
        postMap.put("mention_users", mentionJsonArray);

        vHelper.addVolleyRequestListeners(Api.getInstance().userCommentUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                                AlertUtils.showToast(ReplyActivity.this, res.getString("message"));
                                JSONArray commentLists = res.getJSONArray("latest_comment");

                                if (commentLists.length() == 0) {
                                    adapter.setIsLoadMoreEnable(false);
                                    adapter.notifyItemChanged(0);
                                    return;
                                }
                                for (int i = 0; i < commentLists.length(); i++) {
                                    CommentModel commentModel = new CommentModel();
                                    JSONObject eachObj = commentLists.getJSONObject(i);
                                    commentModel.setCmntId(eachObj.getString("comment_id"));
                                    commentModel.setCmntTime(eachObj.getString("comment_time"));
                                    commentModel.setUserName(eachObj.getString("name"));
                                    commentModel.setCmntBody(eachObj.getString("body"));
                                    commentModel.setReplyCount(Integer.parseInt(eachObj.getString("reply_count")));
                                    commentModel.setUserId(eachObj.getString("comment_by"));
                                    String profileImage = eachObj.getString("profile_image");
                                    if (!TextUtils.isEmpty(profileImage)) {
                                        commentModel.setImgUrl(res.getString("profileimage_default_url") + profileImage);
                                    } else {
                                        commentModel.setImgUrl("");
                                    }
                                    //mentions
                                    commentModel.setMentionJsonData(eachObj.getString("mention_users"));

                                    repList.add(commentModel);
                                }

                                if (repList.size() >= limit) {
                                    adapter.setIsLoadMoreEnable(true);
                                } else {
                                    adapter.setIsLoadMoreEnable(false);
                                }
                                if (repList.size() > 0) {
                                    adapter.notifyDataSetChanged();
                                    replyListView.setVisibility(View.VISIBLE);
                                } else {
                                    replyListView.setVisibility(View.GONE);
                                }
                                try {
                                    replyListView.smoothScrollToPosition(repList.size());
                                } catch (Exception e) {
                                }
                                offset = repList.size();
                            } else {
                                AlertUtils.showToast(ReplyActivity.this, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userReplyTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userReplyTask error ex", e.getMessage() + "");
                        }
                        AlertUtils.showAlertMessage(ReplyActivity.this, "Error !!!", errMsg);
                    }
                }, "userReplyTask");
    }

    private void userReplyListingTask(final boolean isLoadMore, final int listSizePrev) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("parent_id", commentModel.getCmntId());
        postMap.put("event_id", eventid);
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().replyCommentUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray commentLists = res.getJSONArray("comment");
                                if (offset == 0) {
                                    repList.clear();
                                }
                                if (commentLists.length() == 0) {
                                    adapter.setIsLoadMoreEnable(false);
                                    adapter.notifyItemChanged(0);
                                    return;
                                }
                                for (int i = 0; i < commentLists.length(); i++) {
                                    CommentModel commentModel = new CommentModel();
                                    JSONObject eachObj = commentLists.getJSONObject(i);
                                    commentModel.setCmntId(eachObj.getString("comment_id"));
                                    commentModel.setCmntTime(eachObj.getString("comment_time"));
                                    commentModel.setUserName(eachObj.getString("name"));
                                    commentModel.setCmntBody(eachObj.getString("body"));
                                    commentModel.setUserId(eachObj.getString("comment_by"));
                                    String profileImage = eachObj.getString("profile_image");
                                    if (!TextUtils.isEmpty(profileImage)) {
                                        commentModel.setImgUrl(res.getString("profileimage_default_url") + profileImage);
                                    } else {
                                        commentModel.setImgUrl("");
                                    }
                                    //mentions
                                    commentModel.setMentionJsonData(eachObj.getString("mention_users"));

                                    repList.add(0, commentModel);
                                }
                                replyListView.setVisibility(View.VISIBLE);
                                if (repList.size() >= limit) {
                                    adapter.setIsLoadMoreEnable(true);
                                } else {
                                    adapter.setIsLoadMoreEnable(false);
                                }
                                if (listSizePrev == 0) {
                                    adapter.notifyDataSetChanged();
                                    Logger.e("scroll", "level 1");
                                } else {
                                    Logger.e("scroll", "level 2");
                                    adapter.notifyItemRangeInserted(1, limit);
                                    adapter.notifyItemChanged(0);
                                }

                                if (!isLoadMore || listSizePrev == 0) {
                                    replyListView.smoothScrollToPosition(repList.size());
                                    Logger.e("scroll", "level 3");
                                } else {
                                    try {
                                        replyListView.smoothScrollToPosition(offset - listSizePrev);
                                        Logger.e("scroll", "level 4");
                                    } catch (Exception e) {
                                        Logger.e("scroll to pos", e.getMessage() + " ");
                                    }
                                }

                                offset = repList.size();
                            } else {
                                if (offset == 0) {
                                    errorText.setText(res.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                    replyListView.setVisibility(View.GONE);
                                } else {
                                    replyListView.setVisibility(View.VISIBLE);
//                                    AlertUtils.showSnack(ReplyActivity.this, errorText, " No more replies");
                                    adapter.setIsLoadMoreEnable(false);
                                    adapter.notifyItemChanged(0);
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("userReplyListingTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userReplyListing error ex", e.getMessage() + "");
                        }
                        if (offset == 0) {
                            errorText.setText(errMsg);
                            errorText.setVisibility(View.VISIBLE);
                            replyListView.setVisibility(View.GONE);
                        } else {
                            AlertUtils.showToast(ReplyActivity.this, errMsg);
                        }
                    }
                }, "userReplyListingTask");
    }

    private void userFriendsListingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("profile_id", helper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserFriendlists, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        isGetFriendListInProgress = false;
                        mentionList.clear();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray friendsArray = res.getJSONArray("friend_list");
                                for (int i = 0; i < friendsArray.length(); i++) {
                                    JSONObject jsonObject = friendsArray.getJSONObject(i);
                                    if (jsonObject.getString("friend_status").equalsIgnoreCase("isfriend")) {
                                        FriendsModel friendsModel = new FriendsModel();
                                        friendsModel.setFriends(true);
                                        friendsModel.setUserName(jsonObject.getString("name"));
                                        friendsModel.setUserId(jsonObject.getString("friend_id"));
                                        friendsModel.setImgUrl(res.getString("default_profile_img_Url") + jsonObject.getString("profile_image"));
                                        String threadId = jsonObject.getString("thread_id");
                                        if (threadId.equals("") || threadId.equals("null") || threadId.equals("0")) {
                                            friendsModel.setThreadId("0");
                                        } else {
                                            friendsModel.setThreadId(threadId);
                                        }
                                        mentionList.add(friendsModel);
                                    }
                                }
                                setMentionAutoCompleteAdapter(mentionList);
                            }
                        } catch (JSONException e) {
                            Logger.e("userFriendsListingTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        isGetFriendListInProgress = false;
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            Logger.e("userFriendsListingTask error ex", e.getMessage() + "");
                        }
                    }
                }, "userFriendsListingTask");
        isGetFriendListInProgress = true;
    }

    private void setMentionAutoCompleteAdapter(ArrayList<FriendsModel> mentionList) {
        isGetFriendList = true;
        if (mentionList.size() > 0) {
            try {
                mentionAdapter.addAll(mentionList);
                mentionAdapter.notifyDataSetChanged();
                mentionAdapter.getFilter().filter(repComment.getText(), null);
            } catch (Exception e) {
                Logger.e("setMention autoComplete adapter", e.getMessage() + " ");
            }
        }
    }

    private void userHashTagTask(String hashKey) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("hash_key", hashKey);

        vHelper.addVolleyRequestListeners(Api.getInstance().hashTagUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray hashArray = res.getJSONArray("hash_keys");
                        hashtagAdapter.clear();

                        ArrayList<Hashtag> allHashTag = new ArrayList<>();
                        for (int i = 0; i < hashArray.length(); i++) {
                            JSONObject eachHashTag = hashArray.getJSONObject(i);
                            allHashTag.add(new Hashtag(eachHashTag.getString("hash_key"), Integer.parseInt(eachHashTag.getString("count"))));
                        }
                        hashtagAdapter.addAll(allHashTag);
//                        edtComment.setHashtagAdapter(hashtagAdapter);
                        hashtagAdapter.notifyDataSetChanged();
                        hashtagAdapter.getFilter().filter(repComment.getText(), null);

                    } else {

                    }
                } catch (JSONException e) {
                    Logger.e("userNotificationsTask error ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errMsg = "Unexpected error !!! Please try again with internet access.";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("userNotificationsTask error ex", e.getMessage() + "");
                }
            }
        }, "userHashTagTask");
    }
}

