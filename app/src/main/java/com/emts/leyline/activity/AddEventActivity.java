package com.emts.leyline.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.emts.leyline.R;
import com.emts.leyline.adapter.EventAdapter;
import com.emts.leyline.helper.AlertUtils;
import com.emts.leyline.helper.Api;
import com.emts.leyline.helper.Config;
import com.emts.leyline.helper.ImageHelper;
import com.emts.leyline.helper.Logger;
import com.emts.leyline.helper.NetworkUtils;
import com.emts.leyline.helper.PreferenceHelper;
import com.emts.leyline.helper.Utils;
import com.emts.leyline.helper.VolleyHelper;
import com.emts.leyline.models.EventCategory;
import com.emts.leyline.models.EventModel;
import com.emts.leyline.models.UserModel;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyleduo.switchbutton.SwitchButton;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.emts.leyline.activity.EventDetailsActivity.createDrawableFromView;

public class AddEventActivity extends AppCompatActivity {
    int PLACE_PICKER_REQUEST = 1;
    String latitude;
    String longitude;
    private GoogleMap mMap;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(40.785091, -73.968285), new LatLng(40.785091, -73.968285));
    Toolbar toolbar;
    ImageView eventPicture, uploadEventPicture, staticMap;
    EditText eventName, eventHost, eventDescription, eventDate, eventTime;
    TextView eventAddress, errorEventName, errorEventHost, errorEventDescription, errorEventDate,
            errorEventTime, errorEventType, errorPhoto, errorAddress;
    Button postEvent, availableMerge;
    int position;
    Dialog dialog;

    private int day, month, year, hour, minute;
    LinearLayout setLocation;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    SwitchButton eventSwitch;
    Spinner spinEventType;
    String location = null;
    PreferenceHelper helper;
    EventAdapter eventAdapter;

    String selectedImagePath;
    public static Uri fileUri = null;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int RESULT_LOAD_VIDEO = 124;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;

    EventModel eventModel;
    ArrayList<EventModel> alertEventList = new ArrayList<>();
    boolean isEditEvent = false;
    boolean isDatePicked = false;
    boolean isTimePicked = false;
    String currentDate = "", currentTime = "";

    ArrayList<EventCategory> eventTypes = new ArrayList<>();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        helper = PreferenceHelper.getInstance(this);

        setContentView(R.layout.activity_add_event);

        eventTypes = Config.getAllEventTypes(this);

        if (getIntent() != null) {
            eventModel = (EventModel) getIntent().getSerializableExtra("eventModel");
            position = getIntent().getIntExtra("position", -1);
        }

        if (eventModel != null) {
            isEditEvent = true;
            latitude = String.valueOf(eventModel.getLatitude());
            longitude = String.valueOf(eventModel.getLongitude());
            location = eventModel.getEventLocation();
        } else {
            isEditEvent = false;
        }

        toolbar = findViewById(R.id.toolbar);
        if (isEditEvent) {
            toolbar.setTitle("Edit Show");
        } else {
            toolbar.setTitle("Add Show");
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        init();


        uploadEventPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto();
            }
        });

        eventPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertForPhoto();
            }
        });

        eventDate.setInputType(InputType.TYPE_NULL);
        eventDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    datePicker();
                }
                return true;
            }
        });

        eventTime.setInputType(InputType.TYPE_NULL);
        eventTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP) {
                    timePicker();
                }
                return true;
            }
        });

        postEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertUtils.hideInputMethod(AddEventActivity.this, eventName);
                if (validation()) {
                    if (NetworkUtils.isInNetwork(AddEventActivity.this)) {
                        if (isEditEvent) {
                            userEditEventTask();
                        } else {
                            userPostEventTask();
                            isDatePicked = false;
                            isTimePicked = false;
                        }
                    } else {
                        AlertUtils.showSnack(AddEventActivity.this, view, "No internet connection...");
                    }
                } else {
                    AlertUtils.showToast(AddEventActivity.this, "Please validate the form...");
                }
            }
        });

        setLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Logger.e("K ayo ta value", String.valueOf(isDatePicked));

                if (isDatePicked == true && isTimePicked == true) {

                    try {
                        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                        startActivityForResult(intentBuilder.build(AddEventActivity.this), PLACE_PICKER_REQUEST);

                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                        Toast.makeText(AddEventActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(AddEventActivity.this, "Please Pick Date And Time first", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void init() {
        eventPicture = findViewById(R.id.event_picture);
        uploadEventPicture = findViewById(R.id.upload_event_picture);
        eventName = findViewById(R.id.edt_event_name);
        eventHost = findViewById(R.id.edt_event_host);
        eventAddress = findViewById(R.id.edt_event_address);
        eventDescription = findViewById(R.id.edt_event_description);
        eventDate = findViewById(R.id.edt_event_date);
        eventTime = findViewById(R.id.edt_event_time);
        postEvent = findViewById(R.id.btn_post_event);
        availableMerge = findViewById(R.id.btn_available_merge);
        staticMap = findViewById(R.id.static_map);
        setLocation = findViewById(R.id.set_location);
        eventSwitch = findViewById(R.id.event_switch);
        spinEventType = findViewById(R.id.spin_event_type);
        errorEventName = findViewById(R.id.error_eventname);
        errorEventDate = findViewById(R.id.error_event_date);
        errorEventHost = findViewById(R.id.error_host_name);
        errorEventDescription = findViewById(R.id.error_eventdesc);
        errorEventTime = findViewById(R.id.error_event_time);
        errorEventType = findViewById(R.id.error_event_type);
        errorPhoto = findViewById(R.id.error_event_photo);
        errorAddress = findViewById(R.id.error_event_add);

        eventHost.setText(helper.getUserName());

        ArrayAdapter<EventCategory> spinnerArrayAdapter = new ArrayAdapter<>(this, R.layout.item_spinner,
                eventTypes);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinEventType.setAdapter(spinnerArrayAdapter);

        Calendar cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        hour = cal.get(Calendar.HOUR_OF_DAY);
        minute = cal.get(Calendar.MINUTE);

        //set value if edit
        if (isEditEvent) {
            Logger.e("picture", eventModel.getEventImage() + " ");
            if (!TextUtils.isEmpty(eventModel.getEventImage())) {
                Picasso.with(AddEventActivity.this).load(eventModel.getEventImage()).into(eventPicture);
            }
            eventName.setText(eventModel.getEventName());
            eventHost.setText(helper.getUserName());
            eventAddress.setText(eventModel.getEventLocation());
            eventDescription.setText(eventModel.getEventDesc());
            eventDate.setText(eventModel.getEventDate());
            eventTime.setText(eventModel.getEventTime());
            postEvent.setText("Save");
            postEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validation()) {
                        if (NetworkUtils.isInNetwork(AddEventActivity.this)) {
                            userEditEventTask();
                        } else {
                            AlertUtils.showToast(AddEventActivity.this, "No internet connection");
                        }
                    } else {
                        AlertUtils.showToast(AddEventActivity.this, "Please validate the form...");
                    }
                }
            });

            availableMerge.setVisibility(View.VISIBLE);
            availableMerge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(AddEventActivity.this)) {
                        alertEventList.clear();
                        userGetSameLocationEvents(String.valueOf(eventModel.getLatitude()),
                                String.valueOf(eventModel.getLongitude()), true);
                    } else {
                        AlertUtils.showToast(AddEventActivity.this, "No internet connection");
                    }
                }
            });
            if (eventModel.getStatus().equals(EventModel.CLOSE_EVENT)) {
                eventSwitch.setChecked(true);
            } else {
                eventSwitch.setChecked(false);
            }
            for (int i = 0; i < eventTypes.size(); i++) {
                if (eventTypes.get(i).getEventCatId().equals(eventModel.getEventCatId())) {
                    spinEventType.setSelection(i);
                    break;
                }
            }
        }
    }

    private void userEditEventTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(AddEventActivity.this, "Please Wait...");
        dialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(AddEventActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("event_id", eventModel.getEventId());
        postMap.put("created_by", helper.getUserId());
        postMap.put("event_name", eventName.getText().toString().trim());
        postMap.put("event_date", eventDate.getText().toString().trim());
        postMap.put("event_time", eventTime.getText().toString().trim());

        postMap.put("event_category_id", eventTypes.get(spinEventType.getSelectedItemPosition()).getEventCatId());

        postMap.put("description", eventDescription.getText().toString().trim());
        postMap.put("host_name", eventHost.getText().toString().trim());
        postMap.put("location", eventAddress.getText().toString().trim());
        if (eventSwitch.isChecked()) {
            postMap.put("status", EventModel.CLOSE_EVENT);
        } else {
            postMap.put("status", EventModel.OPEN_EVENT);
        }

        postMap.put("latitude", latitude);
        postMap.put("longitude", longitude);

        if (!TextUtils.isEmpty(selectedImagePath)) {
            String encodedImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                    (int) Utils.pxFromDp(AddEventActivity.this, 100f),
                    (int) Utils.pxFromDp(AddEventActivity.this, 100f)));

            if (!selectedImagePath.contains("http")) {
                postMap.put("image", encodedImage);
            }
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().editEvent, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            final JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray jsonArray = res.getJSONArray("event_details");
                                JSONArray eventDetails = jsonArray.getJSONArray(0);
                                for (int i = 0; i < eventDetails.length(); i++) {
                                    JSONObject eachObject = eventDetails.getJSONObject(i);

                                    JSONObject creatorObj = eachObject.getJSONObject("created_by");
                                    UserModel[] hostUsers = new UserModel[1];//provide size here
                                    UserModel createdBy = new UserModel();
                                    createdBy.setUserId(creatorObj.getString("id"));
                                    createdBy.setUserName(creatorObj.getString("name"));
                                    String hostImage = creatorObj.getString("image");
                                    if (!TextUtils.isEmpty(hostImage) && !hostImage.equalsIgnoreCase("null")) {
                                        createdBy.setImgUrl(res.getString("profileimage_default_url") + hostImage);
                                    } else {
                                        createdBy.setImgUrl("");
                                    }
                                    hostUsers[0] = createdBy;
                                    eventModel.setHostedBy(hostUsers);

                                    eventModel.setEventId(eachObject.getString("event_id"));
                                    eventModel.setEventName(eachObject.getString("event_name"));
                                    eventModel.setEventImage(res.getString("event_image_default_url") + eachObject.getString("event_image"));
                                    eventModel.setEventAttendStatus(eachObject.getString("attendance"));

                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eachObject.getString("rate")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }
                                    String lat = eachObject.getString("latitude");
                                    try {
                                        eventModel.setLatitude(Double.parseDouble(lat));
                                    } catch (Exception e) {
                                        eventModel.setLatitude(0);
                                    }
                                    String lng = eachObject.getString("longitude");
                                    try {
                                        eventModel.setLongitude(Double.parseDouble(lng));
                                    } catch (Exception e) {
                                        eventModel.setLongitude(0);
                                    }

                                    eventModel.setEventDesc(eachObject.getString("description"));
                                    eventModel.setEventDate(eachObject.getString("event_date"));
                                    eventModel.setEventLocation(eachObject.getString("location"));
                                    eventModel.setEventId(eachObject.getString("event_id"));
                                    eventModel.setStatus(eachObject.getString("status"));
                                    eventModel.setEventCatId(eachObject.getString("event_category_id"));

                                    JSONObject likedBy = eachObject.getJSONObject("like");
                                    eventModel.setLiked(likedBy.getBoolean("your_like_status"));
                                    eventModel.setEventTime(eachObject.getString("event_time"));
                                }


                                AlertDialog.Builder builder = new AlertDialog.Builder(AddEventActivity.this);
                                builder.setMessage(res.getString("message"));
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialog.dismiss();
                                        Intent intent = new Intent();
                                        intent.putExtra("eventmodel", eventModel);
                                        intent.putExtra("position", position);
                                        setResult(RESULT_OK, intent);
                                        onBackPressed();
                                    }
                                });
                                builder.show();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AddEventActivity.this);
                                builder.setMessage(res.getString("message"));
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialog.dismiss();
                                    }
                                });
                                builder.show();
                            }
                        } catch (JSONException e) {
                            Logger.e("userEditEventTask ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(AddEventActivity.this, eventDate, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                AlertUtils.showToast(getApplicationContext(), "No Internet Connection...");
                            } else if (error instanceof ServerError) {
                                AlertUtils.showToast(getApplicationContext(), "Server Error...");
                            } else if (error instanceof AuthFailureError) {
                                AlertUtils.showToast(getApplicationContext(), "Authentication Error...");
                            } else if (error instanceof ParseError) {
                                AlertUtils.showToast(getApplicationContext(), "Parse Error...");
                            } else if (error instanceof TimeoutError) {
                                AlertUtils.showToast(getApplicationContext(), "Timed Out...");
                            }
                        }
                    }
                }, "userEditEventTask");
    }

    private void userPostEventTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(AddEventActivity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(AddEventActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("created_by", helper.getUserId());
        postMap.put("event_name", eventName.getText().toString().trim());
        postMap.put("event_date", eventDate.getText().toString().trim());
        postMap.put("event_time", eventTime.getText().toString().trim());

        postMap.put("event_category_id", eventTypes.get(spinEventType.getSelectedItemPosition()).getEventCatId());

        postMap.put("description", eventDescription.getText().toString().trim());
        postMap.put("host_name", eventHost.getText().toString().trim());
        postMap.put("location", eventAddress.getText().toString().trim());
        if (eventSwitch.isChecked()) {
            postMap.put("status", EventModel.CLOSE_EVENT);
        } else {
            postMap.put("status", EventModel.OPEN_EVENT);
        }

        postMap.put("latitude", latitude);
        postMap.put("longitude", longitude);
        Logger.e("userPostEventTask postParams formPart", postMap.toString());

        String encodedImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                (int) Utils.pxFromDp(AddEventActivity.this, 100f),
                (int) Utils.pxFromDp(AddEventActivity.this, 100f)));

        postMap.put("image", encodedImage);

        vHelper.addVolleyRequestListeners(Api.getInstance().addEvent, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Intent intent = new Intent();
                                intent.putExtra("isAdd", true);
                                setResult(RESULT_OK, intent);
                                AlertDialog.Builder builder = new AlertDialog.Builder(AddEventActivity.this);
                                builder.setMessage(res.getString("message"));
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialog.dismiss();
                                        onBackPressed();
                                    }
                                });
                                builder.show();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AddEventActivity.this);
                                builder.setMessage(res.getString("message"));
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialog.dismiss();
                                    }
                                });
                                builder.show();
                            }
                        } catch (JSONException e) {
                            Logger.e("userPostEventTask ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                            AlertUtils.showToast(AddEventActivity.this, errMsg);
                        } catch (Exception e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                            AlertUtils.showToast(AddEventActivity.this, errMsg);
                        }
                    }
                }, "userPostEventTask");
    }

    public void datePicker() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                eventDate.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                Integer monthOfYearTmp = (monthOfYear + 1);
                Integer dayOfMonthTmp = dayOfMonth;
                String monthOfYearTmp1 = String.valueOf(monthOfYearTmp);
                String dayOfMonthTmp1 = String.valueOf(dayOfMonthTmp);
                if (monthOfYearTmp <= 9) {
                    monthOfYearTmp1 = ("0" + monthOfYearTmp1);
                }
                if (dayOfMonthTmp <= 9) {
                    dayOfMonthTmp1 = ("0" + dayOfMonthTmp1);
                }
                currentDate = (year + "-" + (monthOfYearTmp1) + "-" + dayOfMonthTmp1);
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();
        isDatePicked = true;

    }


    public void timePicker() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                int hourOfDayTemp = hourOfDay;
                int minuteTemp = minute;

                if (minute < 9) {
                    eventTime.setText(hourOfDay + ":" + "0" + minute);
                } else {
                    eventTime.setText(hourOfDay + ":" + minute);
                }

                String hourOfDayTemp1 = String.valueOf(hourOfDayTemp);
                String minuteTemp1 = String.valueOf(minuteTemp);

                if (hourOfDayTemp <= 9) {
                    hourOfDayTemp1 = ("0" + hourOfDayTemp1);
                }
                if (minuteTemp <= 9) {
                    minuteTemp1 = ("0" + minuteTemp1);
                }

                currentTime = (hourOfDayTemp1 + ":" + minuteTemp1 + ":" + "00");

            }
        }, hour, minute, false);
        timePickerDialog.show();
        isTimePicked = true;
    }

    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AddEventActivity.this);
        builder.setTitle("Choose Media");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(AddEventActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddEventActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }
                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(AddEventActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(AddEventActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_gtp");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_gtp" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {


            /*if (requestCode == RESULT_LOAD_IMAGE) {

                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                assert selectedImageUri != null;
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);

                Bitmap bm = ImageHelper.decodeImageFileWithCompression(selImgPath,
                        (int) Utils.pxFromDp(getApplicationContext(), 85f), (int) Utils.pxFromDp(getApplicationContext(), 85f));
                if (requestCode == RESULT_LOAD_IMAGE) {
                    eventPicture.setImageBitmap(bm);
                    selectedImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(selectedImagePath, bm);
                }
            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                    Glide.with(AddEventActivity.this).load(fileUri).into(eventPicture);
                    ImageHelper.saveBitMapToFile(fileUri.getPath(), ImageHelper.bitmapFromFile(fileUri.getPath()));
                    selectedImagePath = fileUri.getPath();
                }
            }*/


            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);

                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(16, 9)
                        .start(AddEventActivity.this);
            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (fileUri != null) {


                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(16, 12)
                            .start(AddEventActivity.this);
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                selectedImagePath = resultUri.getPath();
                if (resultUri.getPath() != null) {
                    Picasso.with(AddEventActivity.this).load(resultUri).into(eventPicture);
                }
            } else if (requestCode == PLACE_PICKER_REQUEST) {
                final Place place = PlacePicker.getPlace(this, data);
                String toastMsg = String.valueOf(place.getAddress());
                location = toastMsg;

                Double lat = place.getLatLng().latitude;
                Double longs = place.getLatLng().longitude;


                latitude = lat.toString().trim();
                longitude = longs.toString().trim();


                final SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
                        mMap.clear();
                        LatLng nyCity = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nyCity, 15));
                        View marker = ((LayoutInflater) AddEventActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                                inflate(R.layout.custom_layout_marker, null);

                        TextView markerEventName = marker.findViewById(R.id.event_name);
                        ImageView eventIcon = marker.findViewById(R.id.event_icon);
                        markerEventName.setText("Your Event Location");

                        Marker customMarker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude))
                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(AddEventActivity.this, marker))));

                        customMarker.setTag(eventModel);
                    }
                });


                if (NetworkUtils.isInNetwork(this)) {
                    //alertEventList.clear();
                    userGetSameLocationEvents(latitude, longitude, false);
                }
                eventAddress.setText(toastMsg);
                //setMapImage(latitude, longitude);


            } else if (requestCode == EventAdapter.REQUEST_MERGE) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                AddEventActivity.this.finish();
            }
        }
    }

    ProgressDialog pDialog = null;

    private void userGetSameLocationEvents(String latitude, String longitude, final boolean isUserChecking) {
        if (isUserChecking) {
            pDialog = AlertUtils.showProgressDialog(AddEventActivity.this, "Searching ...");
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        postMap.put("latitude", latitude);
        postMap.put("longitude", longitude);
        if (isEditEvent) {
            postMap.put("event_id", eventModel.getEventId());
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().eventInSameLocationUrl, Request.Method.POST,
                postMap, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        alertEventList.clear();
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray eventArray = res.getJSONArray("event_similar");
                                for (int i = 0; i < eventArray.length(); i++) {
                                    JSONObject eachEvent = eventArray.getJSONObject(i);
                                    EventModel eventModel = new EventModel();
                                    JSONObject eventDetail = eachEvent.getJSONObject("event_detail");
                                    eventModel.setEventName(eventDetail.getString("event_name"));
                                    eventModel.setEventId(eventDetail.getString("event_id"));
                                    eventModel.setEventTime(eventDetail.getString("event_time"));
                                    eventModel.setEventDate(eventDetail.getString("event_date"));
                                    eventModel.setStatus(eventDetail.getString("status"));
                                    eventModel.setEventDesc(eventDetail.getString("description"));
                                    eventModel.setEventLocation(eventDetail.getString("location"));
                                    eventModel.setLiked(eventDetail.getBoolean("like_status"));

                                    if (!TextUtils.isEmpty(eventDetail.optString("attendance"))) {
                                        eventModel.setEventAttendStatus(eventDetail.optString("attendance"));
                                    } else {
                                        eventModel.setEventAttendStatus(EventModel.EVENT_NONE);
                                    }

                                    if (!eventDetail.getString("event_image").equals("")) {
                                        eventModel.setEventImage(res.getString("event_image_default_url")
                                                + eventDetail.getString("event_image"));
                                    } else {
                                        eventModel.setEventImage("");
                                    }

                                    try {
                                        eventModel.setEventRating(Float.parseFloat(eventDetail.getString("rate")));
                                    } catch (Exception e) {
                                        eventModel.setEventRating(0);
                                    }

                                    JSONArray hostArray = eachEvent.getJSONArray("host_name");
                                    UserModel[] hostUsers = new UserModel[hostArray.length()];
                                    for (int j = 0; j < hostArray.length(); j++) {
                                        JSONObject eachHostInfo = hostArray.getJSONObject(j);
                                        UserModel hostedBy = new UserModel();
                                        hostedBy.setUserId(eachHostInfo.getString("host_id"));
                                        hostedBy.setUserName(eachHostInfo.getString("host_name"));
                                        if (!eachHostInfo.getString("host_image").equals("")) {
                                            hostedBy.setImgUrl(res.getString("profileimage_default_url")
                                                    + eachHostInfo.getString("host_image"));
                                        } else {
                                            hostedBy.setImgUrl("");
                                        }
                                        hostUsers[j] = hostedBy;
                                    }
                                    eventModel.setHostedBy(hostUsers);
                                    eventModel.setEventCatId(eventDetail.getString("event_category_id"));


                                    if (currentDate.equals(eventModel.getEventDate()) && currentTime.equals(eventModel.getEventTime())) {

                                        alertEventList.add(eventModel);

                                    }
                                }
                                if (alertEventList.size() > 0) {
                                    alertEventList();
                                }
                            } else {
                                if (isUserChecking) {
                                    AlertUtils.showAlertMessage(AddEventActivity.this, "Error !!!", res.getString("message"));
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                        }

                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        String errMsg = "Unexpected error !!! Please try again with internet access.";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errMsg = errorObj.getString("message");
                            AlertUtils.showToast(AddEventActivity.this, errMsg);
                        } catch (Exception e) {
                            Logger.e("userGetSameLocationEvents error ex", e.getMessage() + "");
                            AlertUtils.showToast(AddEventActivity.this, errMsg);
                        }
                    }
                }, "userGetSameLocationEvents");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RESULT_LOAD_IMAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do
                    chooseFrmGallery(false);
                }
                break;

            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhoto();
                }

        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public void chooseFrmGallery(boolean video) {
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }
    }

    /*private void setMapImage(Double latitude, Double longitude) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=600x300&maptype=roadmap\n" +
                "&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318\n" +
                "                &markers=color:red%7Clabel:C%7C40.718217,-73.998284\n" +
                "                &key=AIzaSyBqVwPJPCnOh9C6HUKZOp6eL7Be6EyDHxQ";
        Picasso.with(AddEventActivity.this).load(url).into(staticMap);
        Logger.e("Set map image vitra pugis", "ah yaar");
    }*/

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(eventName.getText().toString().trim())) {
            isValid = false;
            errorEventName.setText(R.string.txt_req);
            errorEventName.setVisibility(View.VISIBLE);

        } else {
            errorEventName.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(eventHost.getText().toString().trim())) {
            isValid = false;
            errorEventHost.setText(R.string.txt_req);
            errorEventHost.setVisibility(View.VISIBLE);
        } else {
            errorEventHost.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(eventAddress.getText().toString().trim())) {
            isValid = false;
            errorAddress.setVisibility(View.VISIBLE);
            errorAddress.setText("Please select location");
        } else {
            errorAddress.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(eventDescription.getText().toString().trim())) {
            isValid = false;
            errorEventDescription.setVisibility(View.VISIBLE);
            errorEventDescription.setText(R.string.txt_req);
        } else {
            errorEventDescription.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(eventDate.getText().toString().trim())) {
            isValid = false;
            errorEventDate.setVisibility(View.VISIBLE);
            errorEventDate.setText(R.string.txt_req);
        } else {
            errorEventDate.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(eventTime.getText().toString().trim())) {
            isValid = false;
            errorEventTime.setText(R.string.txt_req);
            errorEventTime.setVisibility(View.VISIBLE);
        } else {
            errorEventTime.setVisibility(View.GONE);
        }
        if (spinEventType.getSelectedItemPosition() == 0) {
            isValid = false;
            errorEventType.setText(R.string.txt_req);
            errorEventType.setVisibility(View.VISIBLE);
        } else {
            errorEventType.setVisibility(View.GONE);
        }
        if (isEditEvent) {
            if (TextUtils.isEmpty(eventModel.getEventImage())) {
                isValid = false;
                errorPhoto.setText(R.string.txt_req);
                errorPhoto.setVisibility(View.VISIBLE);
            } else {
                errorPhoto.setVisibility(View.VISIBLE);
            }
        } else {
            if (TextUtils.isEmpty(selectedImagePath)) {
                isValid = false;
                errorPhoto.setText(R.string.txt_req);
                errorPhoto.setVisibility(View.VISIBLE);
            } else {
                errorPhoto.setVisibility(View.VISIBLE);
            }
        }

        return isValid;
    }

    public void alertEventList() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(AddEventActivity.this);
        View view = LayoutInflater.from(AddEventActivity.this).inflate(R.layout.alert_event_lisiting, null, false);
        dialog.setView(view);
        RecyclerView eventLisiting = view.findViewById(R.id.alert_event_listing);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AddEventActivity.this);
        eventLisiting.setLayoutManager(linearLayoutManager);
        eventAdapter = new EventAdapter(alertEventList, AddEventActivity.this);
        if (eventModel != null) {
            if (!TextUtils.isEmpty(eventModel.getEventId())) {
                eventAdapter.mergeRequesterEvent(eventModel.getEventId());
            }
        }
        eventLisiting.setAdapter(eventAdapter);

        AddEventActivity.this.dialog = dialog.show();
    }
}
