package com.emts.leyline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.emts.leyline.R;
import com.emts.leyline.helper.Logger;

public class WebsiteActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("User Page");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        String website = intent.getStringExtra("website");
        webView = findViewById(R.id.web_view);
        webView.setWebViewClient(new WebViewClient());
        Logger.e("profile url", website + " ");
        if (website.startsWith("http://") || website.startsWith("https://"))
            webView.loadUrl(website);
        else
            webView.loadUrl("http://" + website);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack())
            webView.goBack();
        else
            super.onBackPressed();
    }
}
